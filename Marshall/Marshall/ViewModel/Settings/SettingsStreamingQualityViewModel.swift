//
//  SettingsStreamingQualityViewModel.swift
//  UrbanEars
//
//  Created by Robert Sandru on 24/11/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class SettingsStreamingQualityViewModel {
    
    // MARK: Properties
    
    var provider: SpeakerProvider
    var audioSystem: AudioSystem
    
    var disposeBag: DisposeBag = DisposeBag()
    var streamingQuality: Variable<StreamingQuality> = Variable(.high)
    // MARK: Init methods
    
    init(provider: SpeakerProvider, audioSystem: AudioSystem) {
        self.provider = provider
        self.audioSystem = audioSystem

        var speakersWithNetworkOptimization: Int = 0
        var speakersWithoutNetworkOptimization: Int = 0
        for speaker in audioSystem.speakers.value {
            if let mrInfo = self.audioSystem.multiroomInfo.value[speaker] {
                if let networkOptimization = mrInfo.networkOptimization {
                    if networkOptimization {
                        speakersWithNetworkOptimization += 1
                    } else {
                        speakersWithoutNetworkOptimization += 1
                    }
                }
            }
        }
        if speakersWithNetworkOptimization >= speakersWithoutNetworkOptimization {
            streamingQuality.value = .low
        } else {
            streamingQuality.value = .high
        }
    }
    
    
}
