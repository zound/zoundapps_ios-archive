//
//  HomePullToRefresh.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 03/03/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

class HomePullToRefresh: PullToRefresh {
    
    convenience init() {
        let refreshView = Bundle(for: type(of: self)).loadNibNamed("RefreshView", owner: nil, options: nil)!.first as! RefreshView
        refreshView.translatesAutoresizingMaskIntoConstraints = true
        refreshView.autoresizingMask = [.flexibleWidth]
        let animator = RefreshAnimator(refreshView: refreshView)
        self.init(refreshView: refreshView, animator: animator)
    }
}
