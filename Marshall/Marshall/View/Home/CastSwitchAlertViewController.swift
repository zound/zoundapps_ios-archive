//
//  VolumeViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 08/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Zound

protocol CastSwitchAlertViewControllerDelegate: class {
    
    func castSwitchAlertDidRequestContinue(_ alertViewController: CastSwitchAlertViewController)
    func castSwitchAlertDidRequestCancel(_ alertViewController: CastSwitchAlertViewController)
}


class CastSwitchAlertViewController: UIViewController {
    
    var dontShow: Bool = false
    weak var delegate: CastSwitchAlertViewControllerDelegate?
    var speakerViewModel: HomeSpeakerViewModel?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var learnMoreButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.text = Localizations.CastSwitch.Title
        titleLabel.font = Fonts.UrbanEars.Bold(23)
        
        contentLabel.text = Localizations.CastSwitch.Content
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)

        continueButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.CastSwitch.Buttons._Continue), for: .normal)
        learnMoreButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.CastSwitch.Buttons.Cancel), for: .normal)
        
        continueButton.layoutIfNeeded()
        learnMoreButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onClose(_ sender: AnyObject) {
        
        self.delegate?.castSwitchAlertDidRequestCancel(self)
    }
    @IBAction func onContinue(_ sender: AnyObject) {
        
        self.delegate?.castSwitchAlertDidRequestContinue(self)
    }
    @IBAction func onLearnMore(_ sender: AnyObject) {
        
        self.delegate?.castSwitchAlertDidRequestCancel(self)
    }
}
