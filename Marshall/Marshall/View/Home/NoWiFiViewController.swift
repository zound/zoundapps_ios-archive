//
//  NoWiFiViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol NoWiFiViewControllerDelegate: class {
    
    func noWifiViewControllerDidRequestMenu(_ noWifiViewController: UIViewController)
}

class NoWiFiViewController: UIViewController {
    
    weak var delegate: NoWiFiViewControllerDelegate?
    
    @IBOutlet weak var messageTitleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var goToSettingsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageTitleLabel.attributedText = Fonts.UrbanEars.Bold(23).AttributedTextWithString(Localizations.NoWifi.Title, color: UIColor.white, letterSpacing: 1.12)
        
        messageLabel.text = Localizations.NoWifi.Description
        messageLabel.font = Fonts.MainContentFont
        
        goToSettingsButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.NoWifi.Buttons.GoToSettings), for: .normal)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onMenu(_ sender: AnyObject) {
        
        self.delegate?.noWifiViewControllerDidRequestMenu(self)
    }
    @IBAction func onGotoSettings(_ sender: Any) {
        
        if #available(iOS 10.0, *) {
            if let url = URL(string: "App-Prefs:root=WIFI") {
                UIApplication.shared.open(url, completionHandler: .none)
            }
        } else {
            if let url = URL(string: "prefs:root=WIFI") {
                UIApplication.shared.openURL(url)
            }
        }

    }
}
