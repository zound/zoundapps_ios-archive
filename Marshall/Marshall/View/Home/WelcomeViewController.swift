//
//  WelcomeViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import AVKit
import AVFoundation

protocol WelcomeViewControllerDelegate:class {
    
    func welcomeViewControllerDidAccept(_ welcomeViewController: WelcomeViewController)
}

class WelcomeViewController:UIViewController {
    
    weak var delegate: WelcomeViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var placeholderImage: UIImageView!
    var playerLayer: AVPlayerLayer?
    var player: AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        titleLabel.attributedText = Fonts.UrbanEars.Bold(45).AttributedTextWithString(Localizations.Welcome.Title, color: titleLabel.textColor, letterSpacing: 1)
        
        subtitleLabel.attributedText = Fonts.UrbanEars.Regular(17).AttributedTextWithString(Localizations.Welcome.Subtitle, color: titleLabel.textColor, letterSpacing: 0.25)
        
        UIView.setAnimationsEnabled(false)
        
        acceptButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Welcome.Buttons.Accept), for: .normal)
        acceptButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        
        titleLabel.adjustsFontSizeToFitWidth = true
        
        
        initVideoPlayer()
        
        let didEnterBackground = NotificationCenter.default.rx.notification(NSNotification.Name.UIApplicationDidEnterBackground)
        let willEnterForeground = NotificationCenter.default.rx.notification(NSNotification.Name.UIApplicationWillEnterForeground)
        
        didEnterBackground.subscribe(onNext: { [weak self] _ in
            self?.didEnterBackground()
        }).disposed(by: rx_disposeBag)
        
        willEnterForeground.subscribe(onNext: { [weak self] _ in
            self?.willEnterForeground()
        }).disposed(by: rx_disposeBag)
        
        animateControls()
    }

    
    func willEnterForeground() -> Void {
        
//        if self.navigationController?.viewControllers.last == self {
//            loopVideo()
//        }
        
    }
    
    func didEnterBackground() -> Void {
        
//        player?.pause()
//        self.placeholderImage.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        player?.play()
    }
    
    override func viewDidLayoutSubviews() {
        
        playerLayer?.frame = view.frame
    }

    
    func animateControls() {
        
        titleLabel.alpha = 0.0
        subtitleLabel.alpha = 0.0
        acceptButton.alpha = 0.0
        
        UIView.animate(withDuration: 3.0, delay: 0.0, options: [.beginFromCurrentState, .allowUserInteraction], animations: { [weak self] in
        
            self?.titleLabel.alpha = 1.0
            self?.subtitleLabel.alpha = 1.0
            self?.acceptButton.alpha = 1.0
        }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
     
        player?.pause()
    }
    
    func initVideoPlayer() {
        
        if let videoURL = Bundle.main.url(forResource: "intro", withExtension: "mp4") {
            
            
            _ = try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
            
            player = AVPlayer(url: videoURL)
            player?.actionAtItemEnd = .none
            player?.isMuted = false
            
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            playerLayer?.zPosition = -1
            
            playerLayer?.frame = view.frame
            
            view.layer.addSublayer(playerLayer!)
            
            
            NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
            
            
            if let player = player {
                player.rx.observe(AVPlayerStatus.self, "status").filter({ $0 != nil && $0! == .readyToPlay}).subscribe( onNext: { [weak self] status in
                    if let status = status {
                        self?.didChangePlayerStatusTo(status: status)
                    }
                }).disposed(by: rx_disposeBag)
            }
        }
        
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func didChangePlayerStatusTo(status: AVPlayerStatus) {
        
        if case AVPlayerStatus.readyToPlay = status {
            self.placeholderImage.isHidden = true
        }
    }
    @objc func loopVideo() {
        
        let t1 = CMTimeMake(5, 100);
        player?.seek(to: t1)
        player?.play()
        self.placeholderImage.isHidden = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onAccept(_ sender: AnyObject) {
        
        self.delegate?.welcomeViewControllerDidAccept(self)
    }
    
}
