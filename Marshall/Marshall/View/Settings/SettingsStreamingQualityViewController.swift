//
//  SettingsStreamingQualityViewController.swift
//  UrbanEars
//
//  Created by Robert Sandru on 24/11/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import MinuetSDK

enum StreamingQuality {
    case high
    case low
}

enum StreamingQualitySettingType {
    case radio
}

enum StreamingQualitySetting {
    
    case high
    case low
    
    var quality: StreamingQuality? {
        
        switch self {
        case .high: return StreamingQuality.high
        case .low: return StreamingQuality.low
        }
    }
    
    var type: StreamingQualitySettingType {
        
        switch self {
        case .high: return .radio
        case .low: return .radio
        }
    }
    
    var localizedDisplayName: String {
        
        switch self {
        case .high: return Localizations.Settings.StreamingQuality.High
        case .low: return Localizations.Settings.StreamingQuality.Normal
        }
    }
}

protocol SettingsStreamingQualityViewControllerDelegate: class {
    
    func settingsStreamingQualityControllerDidRequestBack(_ viewController: SettingsStreamingQualityViewController)
    func settingsStreamingQualityControllerDidRequestSetting(_ speakers: [Speaker], streamingQualitySetting: StreamingQualitySetting, settingStreamingQualityCell: SettingsStreamingQualitySettingCell, viewController: SettingsStreamingQualityViewController)
}

class SettingsStreamingQualityViewController: UIViewController {
    
    weak var delegate: SettingsStreamingQualityViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!
    @IBOutlet weak var topSeparator: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var viewModel: SettingsStreamingQualityViewModel?
    
    let menuItems: [StreamingQualitySetting]
    required init?(coder aDecoder: NSCoder) {
        self.menuItems = [.low, .high]
        super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.StreamingQuality.Title)
        self.tableTopSeparatorHeight.constant = 0.5
        
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.delegate = self
        
        descriptionLabel.attributedText = Fonts.UrbanEars.Regular(15).AttributedTextWithString(Localizations.Settings.StreamingQuality.Description, color: UIColor.white)
        
        updateSeparatorViewVisibility()
        
        if let vm = viewModel {
            if vm.streamingQuality.value == .high {
                self.tableView.selectRow(at: IndexPath(row: 1, section: 0), animated: false, scrollPosition: .none)
            } else {
                self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
            }
        }
    }
    
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.settingsStreamingQualityControllerDidRequestBack(self)
    }
}

extension SettingsStreamingQualityViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let vm = viewModel {
            let menuItem = menuItems[(indexPath as NSIndexPath).row]
            switch menuItem.type {
            case .radio:
                if let quality = menuItem.quality {
                    switch quality {
                    case .high:
                        if let cell = tableView.dequeueReusableCell(withIdentifier: "settingsSettingHigh", for: indexPath) as? SettingsStreamingQualitySettingCell {
                            
                            cell.setting = menuItem
                            cell.cellStreamingQuality = StreamingQuality.high
                            if vm.streamingQuality.value == .high {
                                cell.viewModel.isSelected.value = true
                            } else {
                                cell.viewModel.isSelected.value = false
                            }
                            return cell
                        }
                        break;
                    case .low:
                        if let cell = tableView.dequeueReusableCell(withIdentifier: "settingsSettingLow", for: indexPath) as? SettingsStreamingQualitySettingCell {
                            
                            cell.setting = menuItem
                            cell.cellStreamingQuality = StreamingQuality.low
                            if vm.streamingQuality.value == .low {
                                cell.viewModel.isSelected.value = true
                            } else {
                                cell.viewModel.isSelected.value = false
                            }
                            return cell
                        }
                        break;
                    }
                }
        }
        }
        return UITableViewCell()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    func updateSeparatorViewVisibility() {
        
        topSeparator.alpha = max(0,min(1.0,tableView.contentOffset.y/22.0))
    }
    
}

extension SettingsStreamingQualityViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let setting = menuItems[(indexPath as NSIndexPath).row]
        let cell = tableView.cellForRow(at: indexPath) as! SettingsStreamingQualitySettingCell
        
        for cell in self.tableView.visibleCells {
            if let cell = cell as? SettingsStreamingQualitySettingCell {
                cell.viewModel.isSelected.value = false
            }
        }
        cell.viewModel.isSelected.value = true
        
        if let vm = viewModel {
            if let settingQuality = setting.quality {
                vm.streamingQuality.value = settingQuality
            }
            self.delegate?.settingsStreamingQualityControllerDidRequestSetting(vm.audioSystem.speakers.value, streamingQualitySetting: setting, settingStreamingQualityCell: cell, viewController: self)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        updateSeparatorViewVisibility()
    }
}
