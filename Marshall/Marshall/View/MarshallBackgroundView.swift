//
//  MarshallBackgroundView.swift
//  Marshall Background View
//
//  Created by Sam Soffes on 10/27/09.
//  Copyright (c) 2009-2014 Sam Soffes. All rights reserved.
//

import UIKit

/// Simple view for drawing gradients and borders.
open class MarshallBackgroundView: UIImageView {
    
    
    public required init?(coder aDecoder: NSCoder) {
        
        
        super.init(coder: aDecoder)
        self.image = UIImage(named: "marshall_background")
        self.contentMode = .scaleToFill
    }
}
