//
//  RootViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 11/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import Cartography
import Zound

protocol RootViewControllerDelegate: class {
    
    func rootViewControllerdidShowMenuViewController(_ rootViewController: RootViewController)
    func rootViewControllerdidHideMenuViewController(_ rootViewController: RootViewController)
}

class RootViewController: UIViewController, UIGestureRecognizerDelegate, MenuViewInteractiveDelegate {
    
    weak var delegate: RootViewControllerDelegate?
    var displayedViewController:UIViewController?
    let menuTransitionDelegate: MenuTransitioningDelegate = MenuTransitioningDelegate()
    override func viewDidLoad() {
        super.viewDidLoad()
        menuTransitionDelegate.dismissInteractor.delegate = self
        menuTransitionDelegate.presentInteractor.delegate = self
    }
    
    var forcedHiddenStatusBar: Bool = false {
        
        didSet {
            UIView.animate(withDuration: 0.15, animations: { [weak self] in
                self?.setNeedsStatusBarAppearanceUpdate()
            })
        }
    }
    
    func showChildViewController(_ viewController: UIViewController?, animated:Bool) {
    
        if let vc = self.displayedViewController {
            vc.willMove(toParentViewController: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
        }
        if let newVC = viewController {
            self.addChildViewController(newVC)
            newVC.view.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(newVC.view)
            constrain(newVC.view) { view in
                view.edges == view.superview!.edges
            }
            newVC.didMove(toParentViewController: self)
            self.displayedViewController = newVC
            self.setNeedsStatusBarAppearanceUpdate()
            
        }
    }
    
    override var childViewControllerForStatusBarHidden: UIViewController? {
        
        if forcedHiddenStatusBar {
            return nil
        } else {
            
            return self.childViewControllers.first
        }
    }
    
    override var childViewControllerForStatusBarStyle: UIViewController? {
        
        return self.childViewControllers.first
    }
    
    override var prefersStatusBarHidden: Bool {
        
        return true
    }
    

    var menuVisible = false
    let menuAnimationTime = 0.35
    var currentMenuViewController: UIViewController?
    var currentMenuConstraints:ConstraintGroup = ConstraintGroup()
    
    var menuViewController: UIViewController? {
        
        didSet {
            if let menuViewController = menuViewController {
                menuViewController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                menuViewController.transitioningDelegate = menuTransitionDelegate
                
                menuTransitionDelegate.attachDismissToViewController(menuViewController, withView: menuViewController.view, presentViewController: nil)
                //menuTransitionDelegate.attachPresentToViewController(self, withView: self.view, presentViewController: menuViewController)
            }
        }
    }
    
    func showMenuViewController(withCompletion completion:@escaping () -> Void) {
        
        
        if let menuViewController = menuViewController {
            menuTransitionDelegate.disableInteractivePlayerTransitioning = true
            menuVisible = true
            self.present(menuViewController, animated: true, completion: { [weak self] in
                
                self?.menuTransitionDelegate.disableInteractivePlayerTransitioning = false
                self?.delegate?.rootViewControllerdidShowMenuViewController(self!)
                completion()
            })
        }
    }
    
    func hideMenuViewController(withCompletion completion:@escaping () -> Void) {
        
        menuTransitionDelegate.disableInteractivePlayerTransitioning = true
        self.dismiss(animated: true, completion: {[weak self] in
            
            self?.menuVisible = false
            self?.menuTransitionDelegate.disableInteractivePlayerTransitioning = false
            self?.delegate?.rootViewControllerdidHideMenuViewController(self!)
            completion()
        })
        
        
    }
    
    func menuViewInteractiveDidInteractivelyDismiss() {
        
        self.menuVisible = false
    }
    
    func menuViewInteractiveDidInteractivelyPresent() {
     
        self.menuVisible = true
    }
    
}
