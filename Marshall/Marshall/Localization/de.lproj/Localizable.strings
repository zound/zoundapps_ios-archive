/* Localizable.strings
  UrbanEars

  Created by Raul Andrisan on 11/07/16.
  Copyright © 2016 Zound Industries. All rights reserved. */

"about.eula.buttons.go_to_website" = "ZUR WEBSITE";

"about.eula.content" = "Die vollständige Fassung des Endnutzer-Lizenzvertrags findest du unter www.marshallheadphones.com.";

"about.eula.title" = "Endbenutzer-Lizenzvertrag";

"about.foss.buttons.go_to_website" = "ZUR WEBSITE";

"about.foss.content" = "Die vollständige Version der kostenlosen und Open-Source-Software findest du unter www.marshallheadphones.com.";

"about.foss.title" = "FOSS";

"about.menu_item.eula" = "Endbenutzer-Lizenzvertrag";

"about.menu_item.foss" = "Kostenlose und Open-Source-Software";

"about.title" = "ÜBER";

"about.version" = "Marshall Multi-Room\nVersion %@ (%@)";

"airplay_source.content" = "Mit AirPlay kannst du über das Kontrollzentrum deines iOS-Geräts eine Musikquelle direkt auf deinen Lautsprecher streamen.";

"browse_radio.search_cancel" = "ABBRECHEN";

"browse_radio.search_placeholder" = "Radiosender suchen";

"browse_radio.title" = "RADIO DURCHSUCHEN";

"cast_info.buttons.get_google_cast" = "GOOGLE HOME-APP ERHALTEN";

"cast_info.buttons.learn_more" = "MEHR ERFAHREN";

"cast_info.content" = "Dieser Lautsprecher spielt über das integrierte Chromecast.\n\nVerwende die App Google Home, um Gruppen für Cast-Multiroom einzurichten.\n\nDeine Multiroom-Gruppen werden unter der Cast-Schaltfläche in deiner Musik-App angezeigt.";

"cast_info.title" = "HINWEIS";

"cast_switch.title" = "HINWEIS";

"CFBundleDisplayName" = "Multi-Room";

"CFBundleName" = "Multi-Room";

"google_cast_source.content" = "Dein Lautsprecher ist über das Cast-Symbol auf mehreren Apps verfügbar.";

"google_cast_source.get_cast_apps" = "CAST-AKTIVIERTE APPS SUCHEN";

"google_cast_source.note" = "HINWEIS\nUm Musik auf mehreren Lautsprechern gleichzeitig über integriertes Chromecast abzuspielen, muss mit der Google Home-App eine Multi-Gruppe eingerichtet werden.";

"group_full.buttons.back" = "ZURÜCK";

"group_full.content" = "Du versuchst, im Multi-Mode einen sechsten Lautsprecher hinzuzufügen. Wir freuen uns, dass du so viele Lautsprecher gekauft hast, aber zurzeit unterstützt das Marshall Multi-Room System den Multi-Mode nur für fünf Lautsprecher in einem Wi-Fi-Netz.";

"group_full.title" = "HINWEIS";

"help.contact.button.go_to_website" = "ZUR WEBSITE";

"help.contact.button.send_email" = "SUPPORT KONTAKTIEREN";

"help.contact.content" = "Du kannst unsere Website unter www.marshallheadphones.com besuchen \noder unseren Support kontaktieren.";

"help.contact.title" = "KONTAKT";

"help.menu_item.contact" = "Kontakt";

"help.menu_item.online_manual" = "Online-Handbuch";

"help.menu_item.quick_guide" = "Kurzanleitung";

"help.online_manual_content" = "Auf www.marshallheadphones.com sind Benutzerhandbücher für Marshall Speakers in mehreren Sprachen verfügbar.";

"help.online_manual.button.go_to_website" = "ZUR WEBSITE";

"help.online_manual.title" = "ONLINE-HANDBUCH";

"help.quick_guide.menu_item.presets" = "Voreinstellungen";

"help.quick_guide.menu_item.solo_multi" = "Single-Mode/Multi-Mode";

"help.quick_guide.menu_item.speaker_knobs" = "Lautsprecherregler";

"help.quick_guide.title" = "KURZANLEITUNG";

"help.title" = "HILFE";

"home.speaker.playing_source" = "%@";

"home.speaker.switch_label.multi" = "MULTI";

"home.speaker.switch_label.solo" = "SINGLE";

"loading.loading_speakers" = "Nach verbundenen Lautsprechern suchen ...";

"menu.about" = "Über";

"menu.add_speaker" = "Setup";

"menu.help" = "Hilfe";

"menu.shop" = "Kaufen";

"menu.speaker_settings" = "Einstellungen";

"no_speakers.buttons.refresh" = "AKTUALISIEREN";

"no_speakers.buttons.set_up_speakers" = "SETUP";

"no_speakers.description" = "Es scheint, dass in diesem Wi-Fi-Netz keine Lautsprecher installiert sind.";

"no_wifi.buttons.go_to_settings" = "ZU DEN EINSTELLUNGEN";

"no_wifi.description" = "Es ist keine Wi-Fi-Verbindung auf deinem Gerät verfügbar. Überprüfe deine Verbindung und versuche es erneut.";

"no_wifi.title" = "SORRY, KEIN SIGNAL";

"player.aux.activate" = "AKTIVIEREN";

"player.aux.activated" = "AKTIVIERT";

"player.aux.audio_active" = "Audio aktiv";

"player.bluetooth.activate" = "AKTIVIEREN";

"player.bluetooth.activated" = "AKTIVIERT";

"player.bluetooth.connected" = "Bluetooth – verbunden";

"player.bluetooth.connected_to" = "Verbunden mit";

"player.bluetooth.enter_pairing" = "KOPPLUNGSMODUS";

"player.bluetooth.idle" = "Idle";

"player.bluetooth.waiting_to_pair" = "WARTEN AUF KOPPLUNG";

"player.cloud.airplay" = "AIRPLAY";

"player.cloud.google_cast" = "INTEGRIERTES CHROMECAST";

"player.cloud.or_play_mobile_app" = "Über Smartphone-App abspielen";

"player.cloud.play_internet_radio" = "INTERNETRADIO";

"player.cloud.spotify_connect" = "SPOTIFY";

"player.connecting.buttons.cancel" = "ABBRECHEN";

"player.connecting.reconnecting" = "Erneut verbinden …";

"player.now_playing.browse_stations" = "RADIO DURCHSUCHEN";

"player.now_playing.save_preset" = "ZU VOREINSTELLUNGEN HINZUFÜGEN";

"player.preset.empty_placeholder" = "Voreinstellung leeren";

"player.preset.notification.add" = "Voreinstellung hinzugefügt";

"player.preset.notification.add_playlist" = "Playliste hinzugefügt";

"player.preset.notification.add_radio_station" = "Radiosender hinzugefügt";

"player.preset.notification.addfailed" = "Hinzufügen der Voreinstellung fehlgeschlagen";

"player.preset.notification.addfailedinternetradio" = "Hinzufügen des Radiosenders fehlgeschlagen";

"player.preset.notification.addfailedplaylist" = "Hinzufügen der Playliste fehlgeschlagen";

"player.preset.notification.delete" = "Voreinstellung gelöscht";

"player.preset.notification.delete_internet_radio" = "Radiosender gelöscht";

"player.preset.notification.delete_playlist" = "Playliste gelöscht";

"player.preset.notification.empty" = "Voreinstellung ist leer";

"player.preset.notification.play" = "Abspielen von Voreinstellung";

"player.preset.spotify_login_error.cancel_button" = "ABBRECHEN";

"player.preset.spotify_login_error.content" = "Dein Lautsprecher konnte nicht mit\ndeinem Spotify-Konto verbunden werden. \n\nBitte versuche es erneut oder gehe zurück.";

"player.preset.spotify_login_error.retry_button" = "ERNEUT VERSUCHEN";

"player.preset.spotify_login_error.title" = "ETWAS IST FEHLGESCHLAGEN";

"player.preset.spotify_premium_error.content" = "Du hast kein Premium-Konto. Um Spotify auf deinen Lautsprecher zu streamen, ist ein Premium-Konto erforderlich.";

"player.preset.spotify_premium_error.dismiss_button" = "OK";

"player.preset.spotify_premium_error.title" = "Spotify Fehlermeldung";

"player.session_lost.buttons.go_to_home" = "ZURÜCK";

"player.session_lost.buttons.reconnect" = "ERNEUT VERBINDEN";

"player.session_lost.generic_error_to_speaker" = "Ein unbekannter Fehler ist aufgetreten bei der Kommunikation mit %@. Versuche, erneut eine Verbindung herzustellen, indem du auf \„Erneut verbinden\“ drückst.";

"player.session_lost.network_connection_lost_to_speaker" = "%@ hat die Wi-Fi-Verbindung verloren. Drücke auf \„Erneut verbinden\“, um weiterzugrooven.";

"player.session_lost.speaker_not_connected" = "Lautsprecher ist noch nicht verbunden";

"player.session_lost.timeout" = "Verbindung zu %@ ist abgelaufen. Drücke auf \„Erneut verbinden\“, um es noch einmal zu versuchen.";

"player.session_lost.user_is_controlling_speaker" = "%@ wird von einem anderen Benutzer gesteuert. Bist du bereit, die Regie zu übernehmen? \nDrücke auf \„Erneut verbinden\“, um den Lautsprecher zu steuern.";

"presets_disabled.buttons.continue" = "FERTIG";

"presets_disabled.buttons.read_more" = "MEHR ERFAHREN";

"presets_disabled.content" = "Um eine Voreinstellung hinzuzufügen, spiele entweder:\n\n– Musik über Spotify Connect\n– Internetradio (zu finden unter [cloud_image])\n\nWird eine dieser Quellen abgespielt, ist die „Hinzufügen“-Taste ( [plus_image] ) aktiviert. Durch Antippen fügst du die gerade laufende Playliste oder den Radiosender hinzu.";

"presets_disabled.title" = "SO FÜGST DU EINE VOREINSTELLUNG HINZU";

"save_preset.title" = "ZUR VOREINSTELLUNG HINZUFÜGEN";

"settings.about.cast_version" = "Version für integriertes Chromecast:";

"settings.about.ip" = "IP:";

"settings.about.mac" = "MAC:";

"settings.about.model" = "Modell:";

"settings.about.title" = "ÜBER DIESEN LAUTSPRECHER";

"settings.about.wi_fi_network" = "Wi-Fi-Netz:";

"settings.list.title" = "SINGLE-LAUTSPRECHER";

"settings.speaker.about" = "Über diesen Lautsprecher";

"settings.speaker.rename" = "Lautsprecher umbenennen";

"settings.speaker.streaming_quality" = "Multi-Streaming-Qualität";

"settings.streaming_quality.cellLabel" = "Multi-Streaming-Qualität";

"settings.streaming_quality.description" = "„Low Quality“ kann im Multi-Mode nur auf dem Serverlautsprecher aktiviert werden. \n\nDie Option „Low Quality“ stellt die Audio-Qualität niedriger ein, um die Netzwerkverbindung zu optimieren. Wenn es keine Probleme mit der Netzwerkverbindung gibt, empfehlen wir die Option „High Quality“ auszuwählen.";

"settings.streaming_quality.high" = "High (empfohlen)";

"settings.streaming_quality.low" = "Low";

"settings.streaming_quality.title" = "MULTI-STREAMING-QUALITÄT";

"setup.connect_spotify.buttons.connect_to_spotify" = "MIT SPOTIFY VERBINDEN";

"setup.connect_spotify.paragraph1.content" = "Es ist leicht, Spotify auf deinem Lautsprecher \nabzuspielen.\n\nSobald du verbunden bist, öffne die Spotify-App, um kabellos Musik abzuspielen und zu steuern.";

"setup.connect_spotify.paragraph1.title" = "Mit Spotify verbinden";

"setup.done.buttons.done" = "FERTIG";

"setup.done.content" = "Jetzt kannst du deine Musik mit dem Marshall Wireless Multi-Room System genießen.";

"setup.done.title" = "Alles erledigt";

"setup.failed_boot.buttons.cancel" = "ABBRECHEN";

"setup.failed_boot.buttons.next" = "FERTIG";

"setup.failed_boot.message" = "Wenn beide LED-Lichter beständig leuchten, \nfährt der Lautsprecher hoch. Das dauert etwa 20 Sekunden.";

"setup.failed_connect.buttons.cancel" = "ABBRECHEN";

"setup.failed_connect.buttons.next" = "FERTIG";

"setup.failed_connect.message" = "Stelle sicher, dass dein Lautsprecher an das Stromnetz angeschlossen ist.";

"setup.failed_reset.buttons.next" = "FERTIG";

"setup.failed_reset.message" = "Immer noch Probleme? Sieh dir die Kurzanleitung in unserem Hilfebereich an, um mehr über das Setup zu erfahren.";

"setup.failed_setup.buttons.no" = "NEIN";

"setup.failed_setup.buttons.yes" = "JA";

"setup.failed_setup.message" = "Wenn alle LED-Lichter auf dem \nSource-Regler blinken, befindet sich \nder Lautsprecher im Setup-Modus.\n\nBlinken alle LED-Lichter?";

"setup.list.found_speakers_with_count" = "Okay. %d Lautsprecher wurde(n) gefunden. \nWähle einen aus, um fortzufahren.";

"setup.loading.looking_for_speakers" = "Nach verbundenen Lautsprechern suchen ...";

"setup.pick_presets.buttons.pick_them_now" = "WEITER";

"setup.pick_presets.buttons.skip" = "ÜBERSPRINGEN";

"setup.pick_presets.content" = "Verbinde dich mit Spotify und Internetradio, um deine Voreinstellungen anzufüllen.";

"setup.pick_presets.internet_radio_description" = "Mehr als 30.000 Radiosender weltweit";

"setup.pick_presets.internet_radio_title" = "Internetradio";

"setup.pick_presets.spotify_description" = "Playlisten, Alben, Interpreten, Musikrichtungen & Podcasts";

"setup.pick_presets.title" = "Hinzufügen von Voreinstellungen";

"setup.presets_fail.content" = "Beim Hinzufügen der Voreinstellungen auf deinem Lautsprecher scheint es ein Problem zu geben. Bitte versuche es erneut oder gehe zurück. Du kannst die Voreinstellungen auch später hinzufügen.";

"setup.presets_list.buttons.move_on" = "FERTIG";

"setup.presets_list.title" = "Lerne deine Voreinstellungen kennen";

"setup.presets_loading.adding_preset_title" = "Hinzufügen von %@";

"setup.presets_loading.done" = "Fertig";

"setup.presets_loading.title" = "Hinzufügen von Voreinstellungen";

"setup.presets_tutorial.buttons.continue" = "FERTIG";

"setup.presets_tutorial.content" = "Um eine Voreinstellung abzuspielen, dreh den Knopf auf eine Zahl und drücke darauf.\n\nUm eine Voreinstellung zu speichern, dreh den Knopf auf eine Zahl und halte ihn länger gedrückt beim Hören der Musik über Spotify oder Internetradio.";

"setup.presets_tutorial.title" = "Voreinstellungen verwenden";

"setup.spotify_success.buttons.continue" = "FERTIG";

"setup.spotify_success.title" = "Alles erledigt";

"setup.spotify_success.your_spotify_account" = "Du bist jetzt bei Spotify angemeldet";

"setup.tutorial.cast.content_top" = "Mit integriertem Chromecast kannst du Musik, Internetradio oder Podcasts deiner Lieblings-Apps auf deine Lautsprecher streamen.";

"setup.tutorial.cast.title" = "Integriertes Chromecast";

"setup.tutorial.cloud.airplay" = "AirPlay";

"setup.tutorial.cloud.content" = "Halte bei deiner Lieblings-Musik-App \nnach einem dieser Symbole Ausschau. \nTippe das Symbol an, wenn es erscheint, \nund wähle deinen Lautsprecher aus. So kannst du \nsofort losrocken.";

"setup.tutorial.cloud.google_cast" = "Integriertes Chromecast";

"setup.tutorial.cloud.spotify_connect" = "Spotify-Verbindung";

"setup.tutorial.cloud.title" = "Wi-Fi";

"setup.tutorial.internet_radio.content" = "Durchsuche und spiele mehr als 30.000 Internet-Radiosender über diese App ab.\nInternet-Radiosender findest du in\nder Wi-Fi-Kategorie dieser App.";

"setup.tutorial.internet_radio.title" = "Internetradio";

"setup.tutorial.multi.content" = "Du kannst Gruppen mit mehreren Multi-Room Lautsprechern für ein synchronisiertes Playdate bilden. Setze einen Lautsprecher auf Multi-Mode über diese App oder durch Drücken des Single/Multi-Knopfes auf dem oberen Panel des Lautsprechers.";

"setup.tutorial.next" = "WEITER";

"setup.tutorial.skip" = "ÜBERSPRINGEN";

"spotify_source.buttons.get_spotify" = "SPOTIFY ERHALTEN";

"spotify_source.buttons.open_spotify" = "SPOTIFY ÖFFNEN";

"spotify_source.instructions" = "1. \tÖffne Spotify\n2. \tSpiele einen Song ab\n3.\tTippe auf \„Verfügbare Geräte\“\n4. \tWähle deinen Lautsprecher aus";

"spotify_source.intro" = "Spiele Spotify von deinem Lautsprecher, wobei die Spotify-App als Fernbedienung dient.";

"volume.headers.master_volume_label" = "Multi-Lautstärke";

"volume.title" = "LAUTSTÄRKE";

"welcome.title" = "WILLKOMMEN";

"about.eula.website" = "https://www.marshallheadphones.com/multi-room-speaker-eula-de";

"welcome.buttons.accept" = "START";

"setup.presets_fail.title" = "VOREINSTELLUNG NICHT GESPEICHERT";

"setup.presets_fail.buttons.try_again" = "ERNEUT VERSUCHEN";

"setup.presets_fail.buttons.skip" = "ZURÜCK";

"help.contact.website" = "https://www.marshallheadphones.com/multi-room-speaker-contact";

"help.online_manual.website" = "https://www.marshallheadphones.com/multi-room-speaker-support";

"about.foss.website" = "https://www.marshallheadphones.com/multi-room-speaker-foss";

"volume.role.multi" = "MULTI";

"volume.role.solo" = "SINGLE";

"player.aux.mode_name" = "AUX";

"player.bluetooth.connect_device_advice" = "Stelle sicher, dass Bluetooth auf deinem %@ aktiviert ist.";

"cast_switch.buttons.cancel" = "ABBRECHEN";

"setup.presets_loading.preparing_presets" = "Abrufen von Voreinstellungen";

"player.preset.login_spotify_advice" = "Hier tippen, um eine Verbindung mit Spotify herzustellen. Das voreingestellte Werk wird aktualisiert.";

"player.cloud.internet_radio_mode_name" = "Internetradio";

"player.cloud.google_cast_mode_name" = "Integriertes Chromecast";

"player.preset.spotify_playlist" = "Spotify Playlist";

"player.carousel.playable_source.aux" = "AUX";

"player.carousel.playable_source.preset" = "Voreinstellung";

"player.carousel.playable_source.bluetooth" = "Bluetooth";

"player.carousel.playable_source.cloud" = "Wi-Fi";

"welcome.subtitle" = "Dein Marshall Wireless System basiert auf über 50 Jahren Erfahrung und bringt dir das Live-Gefühl direkt nach Hause.";

"help.contact.support_website" = "https://www.marshallheadphones.com/multi-room-speaker-contact";

"no_speakers.title" = "KEINE LAUTSPRECHER GEFUNDEN";

"player.rca.mode_name" = "RCA";

"player.carousel.playable_source.rca" = "RCA";

"player.rca.activate" = "AKTIVIEREN";

"player.rca.activated" = "AKTIVIERT";

"player.rca.audio_active" = "Audio aktiv";

"volume.volume_slider_label" = "Lautstärke";

"volume.bass_slider_label" = "Bass";

"volume.treble_slider_label" = "Höhen";

"volume.eq_button_show_text" = "EQ";

"volume.eq_button_hide_text" = "VERBERGEN";

"settings.speaker.led_intensity" = "Licht";

"setup.failed.title" = "SETUP-ANLEITUNG";

"setup.pick_presets.content_bottom" = "Lass uns Voreinstellungen zu deinem Lautsprecher hinzufügen, \ndamit du gleich starten kannst. Du kannst sie später ändern.";

"settings.led_adjuster.adjust_instruction" = "LED-Lichtintensität am Lautsprecher anpassen";

"settings.sounds.title" = "Sound";

"settings.sounds.bluetooth_connect" = "Bluetooth-Verbindung";

"settings.sounds.network_connect" = "Netzwerkverbindung";

"settings.sounds.preset_store" = "Voreinstellung Store";

"settings.sounds.preset_fail" = "Voreinstellung fehlgeschlagen";

"reconnecting.reconnecting_to" = "Erneut verbinden mit %@";

"settings.about.build_version" = "Version der System-Firmware:";

"setup.list.multi_room" = "Multi-Room";

"setup.list.connected" = "Voreinstellungen hinzufügen";

"setup.list.connect_to_wifi" = "Mit Wi-Fi verbinden";

"setup.presets_initial.title" = "VOREINSTELLUNGEN";

"setup.presets_initial.content" = "Sieben Voreinstellungen bieten dir unmittelbaren Zugang zu deinen Internet-Radiosendern, Lieblingskünstlern und Playlisten auf Spotify.";

"setup.presets_initial.buttons.next" = "VOREINSTELLUNGEN WERDEN HINZUGEFÜGT";

"setup.presets_initial.buttons.skip" = "ÜBERSPRINGEN";

"settings.root_menu.title" = "EINSTELLUNGEN";

"settings.root_menu.solo_speakers" = "Single-Lautsprecher";

"settings.root_menu.multi_speakers" = "Multi-Lautsprecher";

"settings.multi_menu.stereo_pairing" = "Stereo-Koppelung";

"settings.multi_menu.multi_streaming_quality" = "Multi-Streaming-Qualität";

"player.session_lost.user_is_controlling_speaker_title" = "NUTZERÜBERNAHME";

"player.session_lost.network_connection_lost_to_speaker_title" = "KEINE WI-FI-VERBINDUNG";

"presets_disabled.buttons.back" = "ZURÜCK";

"airplay_source.back" = "ZURÜCK";

"google_cast_source.back" = "ABBRECHEN";

"spotify_source.back" = "ABBRECHEN";

"save_preset.cancel" = "ABBRECHEN";

"cast_switch.buttons.continue" = "WEITER";

"cast_switch.content" = "Durch Wechseln des Lautsprechers in den Multi-Mode wird diese integrierte Chromecast-Sitzung beendet.\n\nVerwende die App Google Home, um Gruppen für Cast-Multiroom einzurichten.";

"unlocked_module.title" = "Hinweis";

"unlocked_module.content"  = "%@ is a prototype unit and will therefore stop working by the end of 2017. \n\nPlease get in touch with your point of contact at Zound Industries.";

"appwide.back" = "ZURÜCK";

"appwide.cancel" = "ABBRECHEN";

"airplay_source.open_airplay" = "ÖFFNE APPLE MUSIC";

"setup.tutorial.multi.title" = "MULTI-MODE";

"volume.buttons.mute_all" = "TON AUS";

"volume.buttons.unmute_all" = "TON AN";


