//
//  SettingsStreamingQualitySettingCell.swift
//  UrbanEars
//
//  Created by Robert Sandru on 24/11/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class SettingsStreamingQualitySettingCell: UITableViewCell {
    
    let settingVariable: Variable<StreamingQualitySetting?> = Variable(nil)
    let viewModel = SettingsStreamingQualitySettingCellViewModel()
    var cellStreamingQuality: StreamingQuality? = nil
    
    var setting: StreamingQualitySetting? {
        
        get {
            return settingVariable.value
        }
        set {
            settingVariable.value = newValue
        }
    }
    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var radioButtonCircle: UIImageView!
    @IBOutlet weak var radioButtonInside: UIImageView!
    
    
    override func awakeFromNib() {
        
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        settingLabel.font = Fonts.ListItemFont
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1.0, alpha: 0.3);
        self.selectedBackgroundView =  customColorView;
        
        settingVariable.asObservable().map{ $0 != nil ?  $0!.localizedDisplayName : "" }.bind(to:settingLabel.rx.text).disposed(by: rx_disposeBag)
        
        viewModel.isSelected.asObservable().subscribe(onNext: self.setRadioButtonState).disposed(by: rx_disposeBag)
    }
    
    func setRadioButtonState(selected: Bool) {
        radioButtonInside.isHidden = !selected
    }
}
