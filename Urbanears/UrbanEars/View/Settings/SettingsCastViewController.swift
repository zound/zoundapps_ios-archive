//
//  SettingsCastViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 19/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

enum CastSettingType {
    case link
    case `switch`
}

enum CastSetting {
    
    case shareUsageData
    case learnHowToCast
    case googleCastEnabledApps
    case googleCastPrivacy
    case googleTermsOfService
    case openSourceLicenses
    
    
    var type: CastSettingType {
        
        switch self {
        case .shareUsageData: return .switch
        default: return .link
        }
    }
    
    var link: URL? {
        
        switch self {
        case .learnHowToCast: return URL(string:"http://www.google.com/cast/learn/audio/")
        case .googleCastEnabledApps: return URL(string:"http://www.google.com/cast/apps")
        case .googleCastPrivacy: return URL(string:"http://www.google.com/policies/privacy/")
        case .googleTermsOfService: return URL(string:"http://www.google.com/policies/terms/")
        case .openSourceLicenses: return URL(string:"https://support.google.com/googlecast/answer/6121012")
        case .shareUsageData: return nil
        }
        
    }
    
    var localizedDisplayName: String {
        
        switch self {
        case .shareUsageData: return Localizations.Settings.GoogleCast.ShareUsageData
        case .learnHowToCast: return Localizations.Settings.GoogleCast.LearnHowToCast
        case .googleCastEnabledApps: return Localizations.Settings.GoogleCast.GoogleCastEnabledApps
        case .googleCastPrivacy: return Localizations.Settings.GoogleCast.GoogleCastPrivacy
        case .googleTermsOfService: return Localizations.Settings.GoogleCast.GoogleTerms
        case .openSourceLicenses: return Localizations.Settings.GoogleCast.OpenSource
        }
    }
}

protocol SettingsCastViewControllerDelegate: class {
    
    func settingsCastViewControllerDidRequestBack(_ viewController: SettingsCastViewController)
    func settingsCastViewControllerDidRequestSetting(_ castSetting: CastSetting, viewController: SettingsCastViewController)
}

class SettingsCastViewController: UIViewController {

    weak var delegate: SettingsCastViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!
    @IBOutlet weak var topSeparator: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: SettingsCastViewModel?
    
    let menuItems: [CastSetting]
    required init?(coder aDecoder: NSCoder) {
        self.menuItems = [.shareUsageData, .learnHowToCast, .googleCastEnabledApps, .googleCastPrivacy, .googleTermsOfService, .openSourceLicenses]
        super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.GoogleCast.Title)
        updateTopSeparatorAlpha()
        self.tableTopSeparatorHeight.constant = 0.5
        
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 66.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    

    func updateTopSeparatorAlpha() {
        
        topSeparator.alpha = max(0,min(1.0,tableView.contentOffset.y/22.0))
    }
    
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.settingsCastViewControllerDidRequestBack(self)
    }
}

extension SettingsCastViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let menuItem = menuItems[(indexPath as NSIndexPath).row]
        switch menuItem.type {
        case .link:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "settingsSetting", for: indexPath) as? SettingsCastSettingCell {
                cell.setting = menuItem
                return cell
            }
        case .switch:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "settingsSwitch", for: indexPath) as? SettingsSwitchCell {
                cell.setting = menuItem
                if let vm = self.viewModel {
                    cell.castViewModel = vm
                }
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
}

extension SettingsCastViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let setting = menuItems[(indexPath as NSIndexPath).row]
        self.delegate?.settingsCastViewControllerDidRequestSetting(setting, viewController: self)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        updateTopSeparatorAlpha()
    }
}
