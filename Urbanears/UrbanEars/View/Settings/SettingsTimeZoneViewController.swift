//
//  SettingsSpeakerList.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import Cartography
import MinuetSDK
import Zound

protocol SettingsTimeZoneViewControllerDelegate: class {
    
    func settingsTimeZoneViewControllerDidRequestBack(_ viewController: SettingsTimeZoneViewController)
}

class SettingsTimeZoneViewController: UIViewController {
    
    weak var delegate: SettingsTimeZoneViewControllerDelegate?
    var viewModel: SettingsTimeZoneViewModel?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!
    @IBOutlet weak var topSeparator: UIView!
    
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBarToTitleConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBarRightConstraint: NSLayoutConstraint!
    @IBOutlet var searchPlaceholderCenterConstraint: NSLayoutConstraint!
    @IBOutlet var tableViewBottomConstraint: NSLayoutConstraint!
    var searchPlaceholderLeftConstraint: NSLayoutConstraint!
    var searchPlaceholderRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var searchPlaceholderLabel: UILabel!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.Timezone.Title)
        cancelButton.setAttributedTitle(Fonts.UrbanEars.Regular(13).AttributedSecondaryButtonWithString(Localizations.Settings.Timezone.CancelButton), for: .normal)
        searchPlaceholderLabel.text = Localizations.Settings.Timezone.SearchHint
        
        tableView.tableFooterView = UIView()
        
        self.tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 22, height: 20))
        searchTextField.leftView = paddingView;
        searchTextField.leftViewMode = UITextFieldViewMode.always
        
        self.tableTopSeparatorHeight.constant = 0.5
        
        Keyboard.keyboardFrameChangeAnimationInfoObservable().subscribe(onNext: {[weak self] (animationInfo:KeyboardAnimationTuple) in
            self?.animateContentUsingKeyboardFrameAnimationInfo(animationInfo)
        })
        .disposed(by: rx_disposeBag)
        
        if let vm = viewModel {
            
            vm.isSearching.asObservable().distinctUntilChanged()
                .subscribe(weak: self, onNext: SettingsTimeZoneViewController.updateForIsSearching)
                .disposed(by: rx_disposeBag)
            
            self.searchTextField.rx.text.subscribe(onNext:{ [weak self] _ in self?.updateSearchPlaceholder() }).disposed(by: rx_disposeBag)
            
            vm.filteredTimeZones.asObservable().subscribe(weak: self, onNext: SettingsTimeZoneViewController.updateWithFilteredTimeZones).disposed(by: rx_disposeBag)
        }
    }
    
    func updateWithFilteredTimeZones(_ timeZones: [MinuetSDK.TimeZone]) {
        
        tableView.reloadData()
    }
    
    func scrollToCurrentTimeZone() {
        
        if let vm = viewModel {
            if let selectedIndex = vm.filteredTimeZones.value.index(where: { $0.name == vm.currentTimeZone.value }) {
                let indexPath = IndexPath(row: selectedIndex, section:0)
                
                DispatchQueue.main.async { [weak self] in
                    self?.tableView.scrollToRow(at: indexPath , at: .middle, animated: false)
                    self?.tableView.flashScrollIndicators()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        scrollToCurrentTimeZone()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.settingsTimeZoneViewControllerDidRequestBack(self)
    }
    
    
    func updateSearchPlaceholder() {
        
        searchPlaceholderLabel.isHidden = (self.searchTextField.text != nil && self.searchTextField.text != "")
    }
    func updateForIsSearching(_ searching: Bool) {
        
        titleTopConstraint.constant = searching ? -21 : 40
        searchBarToTitleConstraint.constant = searching ? 40 : 20
        searchBarRightConstraint.constant = searching ? cancelButton.frame.size.width + 15 : 20
        if searching {
            
            
            if self.searchPlaceholderCenterConstraint != nil {
                self.view.removeConstraint(self.searchPlaceholderCenterConstraint)
                self.searchPlaceholderCenterConstraint = nil
            }
            
            constrain(self.searchPlaceholderLabel, self.searchTextField) { placeholder, textField  in
                self.searchPlaceholderLeftConstraint = (textField.left == placeholder.left - 22)
                self.searchPlaceholderRightConstraint = (textField.right == placeholder.right + 10)
            }
            
            if !self.searchTextField.isFirstResponder {
                self.searchTextField.becomeFirstResponder()
            }
            
        } else {
            
            if self.searchPlaceholderLeftConstraint != nil {
                self.view.removeConstraint(self.searchPlaceholderLeftConstraint)
                self.view.removeConstraint(self.searchPlaceholderRightConstraint)
                self.searchPlaceholderLeftConstraint = nil;
                self.searchPlaceholderRightConstraint = nil;
            }
            constrain(self.searchPlaceholderLabel, self.searchTextField) { placeholder, textField  in
                self.searchPlaceholderCenterConstraint = (textField.centerX == placeholder.centerX)
            }
        }
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
            
            if !searching {
                self?.searchTextField.textAlignment = NSTextAlignment.center
                self?.searchTextField.resignFirstResponder()
            } else {
                self?.searchTextField.textAlignment = NSTextAlignment.left
            }
            self?.cancelButton.alpha = searching ? 1.0 : 0.0
            self?.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    @IBAction func onCancelSearch(_ sender: AnyObject) {
        
        searchTextField.text = ""
        self.updateSearchPlaceholder()
        if let vm = viewModel {
            vm.searchText.value = nil
            vm.isSearching.value = false
        }
    }
    
    
    func animateContentUsingKeyboardFrameAnimationInfo(_ animationInfo: KeyboardAnimationTuple) {
        
        if let view = self.view {
            let keyboardFrame  = view.convert(animationInfo.frame, from: self.view.window)
            self.tableViewBottomConstraint.constant =  (view.bounds.height)-(keyboardFrame.origin.y)
            UIView.animate(withDuration: animationInfo.animationDuration,
                           delay: 0.0,
                           options: UIViewAnimationOptions(rawValue: UInt(animationInfo.animationCurve << 16)),
                           animations: { [weak view] in
                            view?.layoutIfNeeded()
                },
                           completion: nil)
        }
    }
    
}

extension SettingsTimeZoneViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for cell in tableView.visibleCells {
            if let tzCell = cell as? SettingsTimeZoneCell {
                tzCell.tickVisible = false
            }
        }
        if let selectedCell = tableView.cellForRow(at: indexPath) as? SettingsTimeZoneCell {
            selectedCell.tickVisible = true
        }
        
        if let vm = viewModel {
            let timeZone = vm.filteredTimeZones.value[indexPath.row]
            vm.selectTimeZone(timeZone, done: { [weak self] in
                self?.delegate?.settingsTimeZoneViewControllerDidRequestBack(self!)
            })
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt editingStyleForRowAtIndexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.none
    }

}

extension SettingsTimeZoneViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if let vm = viewModel {
            return vm.filteredTimeZones.value.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let vm = viewModel {
            let timeZone = vm.filteredTimeZones.value[indexPath.row]
            if let cell = tableView.dequeueReusableCell(withIdentifier: "settingsTimeZone", for: indexPath) as? SettingsTimeZoneCell {
                cell.timeZone = timeZone
                cell.tickVisible = (timeZone.name == vm.currentTimeZone.value)
                return cell
            }
            
        }
        return UITableViewCell()
    }
    
}

extension SettingsTimeZoneViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let vm = viewModel {
            if let text = textField.text {
             
                if text == "" {
                    vm.isSearching.value = false
                }
            }
        }
        textField.resignFirstResponder()
        return false
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentString: NSString = textField.text as NSString? ?? ""
        let newString: String = currentString.replacingCharacters(in: range, with: string)
        if let vm = viewModel {
            vm.searchText.value = (newString != "") ? newString : nil
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.selectedTextRange = textField.textRange(from: textField.beginningOfDocument, to: textField.endOfDocument)
        if let vm = viewModel {
            vm.isSearching.value = true
        }
    }
}
