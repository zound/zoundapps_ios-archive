//
//  SettingsRename.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift
import RxSwift
import MinuetSDK
import Zound

protocol SettingsRenameViewControllerDelegate: class {
    
    func settingsRenameViewControllerDidRequestBack(_ viewController: SettingsRenameViewController)
    func settingsRenameViewControllerDidRenameSpeaker(_ speaker: Speaker, viewController: SettingsRenameViewController)
}

class SettingsRenameViewController: UIViewController {
    
    weak var delegate: SettingsRenameViewControllerDelegate?
    var viewModel: SettingsRenameViewModel?
    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var speakerNameTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldContainer: UIView!
    @IBOutlet weak var spinnerImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.Rename.Title)
        speakerNameTextField.font = Fonts.UrbanEars.Regular(17)
        
        speakerNameTextField.delegate = self;
        spinnerImageView.isHidden = true
        
        if let vm = viewModel {
            
            vm.delegate = self
            if let speakerName = ExternalConfig.sharedInstance.speakerImageForColor(vm.speaker.color)?.heroImageName {
                speakerImageView.image = UIImage(named: speakerName)
            } else {
                speakerImageView.image = UIImage(named: "hero_placeholder")
            }
            speakerNameTextField.text = vm.speaker.friendlyName
            vm.loading.asObservable().distinctUntilChanged().skip(1).subscribe(weak: self, onNext: SettingsRenameViewController.updateForLoading).disposed(by: rx_disposeBag)
        }
        
        
        keyboardFrameChangeAnimationInfoObservable().subscribe(onNext:{[weak self] (animationInfo:KeyboardAnimationTuple) in
            self?.animateContentUsingKeyboardFrameAnimationInfo(animationInfo)
        })
            .disposed(by: rx_disposeBag)
    }
    
    func animateContentUsingKeyboardFrameAnimationInfo(_ animationInfo: KeyboardAnimationTuple) {
        
        if let view = self.view {
            let keyboardFrame  = view.convert(animationInfo.frame, from: self.view.window)
            self.contentBottomConstraint.constant =  max(0,view.bounds.height - keyboardFrame.origin.y)
            UIView.animate(withDuration: animationInfo.animationDuration,
                           delay: 0.0,
                           options: UIViewAnimationOptions(rawValue: UInt(animationInfo.animationCurve << 16)),
                           animations: { [weak view] in
                            view?.layoutIfNeeded()
                },
                           completion: nil)
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    func keyboardFrameChangeAnimationInfoObservable() -> Observable<KeyboardAnimationTuple> {
        
        let keyboardWillShowUserInfo = NotificationCenter.default.rx.notification(NSNotification.Name.UIKeyboardWillShow).map{ $0.userInfo}
        let keyboardWillHideUserInfo = NotificationCenter.default.rx.notification(NSNotification.Name.UIKeyboardWillHide).map{ $0.userInfo}
        let keyboardFrameChangedAnimationInfo: Observable<KeyboardAnimationTuple> = Observable.of(keyboardWillShowUserInfo, keyboardWillHideUserInfo)
            
            .merge().map{ userInfo in
                
                let frame = (userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
                let animationDuration = (userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
                let animateCurve = (userInfo![UIKeyboardAnimationCurveUserInfoKey]! as AnyObject).integerValue!
                
                return (frame: frame, animationDuration:animationDuration, animationCurve: animateCurve)
        }
        return keyboardFrameChangedAnimationInfo
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        speakerNameTextField.becomeFirstResponder()
        DispatchQueue.main.async(execute: {
            let textField = self.speakerNameTextField
            textField?.selectedTextRange = textField?.textRange(from: (textField?.beginningOfDocument)!, to: (textField?.endOfDocument)!)
        })
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.settingsRenameViewControllerDidRequestBack(self)
    }
    
    
    func onRename() {
        
        if let vm = viewModel {
            if let newSpeakerName = speakerNameTextField.text {
                
                //start renaming progress indicator
                speakerNameTextField.resignFirstResponder()
                vm.renameSpeakerWithName(newSpeakerName)
            }
        }
    }
    
    func updateForLoading(_ loading: Bool) {
        
        if loading {
            spinnerImageView.isHidden = false
            spinnerImageView.rotate()
            UIView.setAlphaOfView(spinnerImageView, visible: true, animated: true)
            UIView.setAlphaOfView(textFieldContainer, visible: false, animated: true)
        } else {
            spinnerImageView.stopRotation()
            UIView.setAlphaOfView(spinnerImageView, visible: false, animated: true)
            UIView.setAlphaOfView(textFieldContainer, visible: true, animated: true)
        }
    }
    
    
    func showToastErrorWithText(_ text: String) {
        
        var style = ToastStyle()
        style.messageAlignment = .center
        ToastManager.shared.tapToDismissEnabled = true
        ToastManager.shared.queueEnabled = false
        self.view.makeToast(text, duration: 2.0, position: .top, style: style)
    }
}

extension SettingsRenameViewController: SettingsRenameViewModelDelegate {
    
    //stop progress indicator
    func settingsRenameViewModelDidFinishRenamingSpeaker(_ speaker: Speaker) {
        
        self.delegate?.settingsRenameViewControllerDidRenameSpeaker(speaker, viewController: self)
    }
    func settingsRenameViewModelDidFailRenamingSpeaker(_ speaker: Speaker) {
        
        //rename error
    }
}

extension SettingsRenameViewController: UITextFieldDelegate {
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let text = textField.text ?? ""
        let friendlyName = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if friendlyName.characters.count > 0 {
            
            onRename()
            
        } else {
            
            showToastErrorWithText(Localizations.Setup.Rename.Error.EmptyName)
        }
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allCharactersValid = string.allCharactersValidAsSpeakerName
        
        let maxLength = 24
        let currentString: NSString = textField.text as NSString? ?? ""
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        let correctLength = (newString.length <= maxLength)
        
        if !allCharactersValid {
            showToastErrorWithText(Localizations.Setup.Rename.Error.SpecialCharacters)
        }
        else if !correctLength {
            showToastErrorWithText(Localizations.Setup.Rename.Error.MaximumLength(maxLength))
        }
        
        return (allCharactersValid && correctLength);
    }
}
