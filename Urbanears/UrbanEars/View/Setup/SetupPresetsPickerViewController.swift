//
//  SetupPresetsPickerViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit


protocol SetupPresetsPickerViewControllerDelegate: class {
    
    func setupPresetsPickerDidRequestBack(_ viewController:SetupPresetsPickerViewController)
    func setupPresetsPickerDidRequestSpotify(_ viewController:SetupPresetsPickerViewController)
    func setupPresetsPickerDidRequestRadio(_ viewController:SetupPresetsPickerViewController)
    func setupPresetsPickerDidRequestSpotifyAndRadio(_ viewController:SetupPresetsPickerViewController)
    func setupPresetsPickerDidRequestSkip(_ viewController:SetupPresetsPickerViewController)
}

enum PresetsPickerState {
    
    case noPresets
    case spotifyOnly
    case radioOnly
    case radioAndSpotify
}

class SetupPresetsPickerViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    
    
    @IBOutlet weak var spotifyDescriptionLabel: UILabel!
    @IBOutlet weak var irOptionTitle: UILabel!
    @IBOutlet weak var irOptionDescription: UILabel!
    
    @IBOutlet weak var radioCheck: UIImageView!
    @IBOutlet weak var spotifyCheck: UIImageView!
    @IBOutlet weak var scrollSize: NSLayoutConstraint!
    
    
    var viewModel: SetupPresetsViewModel?
    weak var delegate: SetupPresetsPickerViewControllerDelegate?
    var radio: Bool = true
    var spotify: Bool = true

    @IBAction func onSkip(_ sender: AnyObject) {
     
        self.delegate?.setupPresetsPickerDidRequestSkip(self)
    }
    @IBAction func onRadio(_ sender: AnyObject) {
        
        radio = !radio
        updateForCurrentPickerSelectionAnimated(true)
    }
    @IBAction func onSpotify(_ sender: AnyObject) {
        
        spotify = !spotify
        updateForCurrentPickerSelectionAnimated(true)
    }
    
    
    @IBAction func onNext(_ sender: AnyObject) {
        
        if let vm = viewModel {
            
            if vm.presetSelection.hasSpotify && vm.presetSelection.hasIR {
                
                self.delegate?.setupPresetsPickerDidRequestSpotifyAndRadio(self)
            } else if vm.presetSelection.hasSpotify {
                
                self.delegate?.setupPresetsPickerDidRequestSpotify(self)
            } else if vm.presetSelection.hasIR {
                
                self.delegate?.setupPresetsPickerDidRequestRadio(self)
            } else if vm.presetSelection.isEmpty {
                
                self.delegate?.setupPresetsPickerDidRequestSkip(self)
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Setup.PickPresets.Title
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        contentLabel.text = Localizations.Setup.PickPresets.Content
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.PickPresets.Buttons.PickThemNow), for: .normal)
        skipButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Setup.PickPresets.Buttons.Skip), for: .normal)
        
        nextButton.layoutIfNeeded()
        skipButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        spotifyDescriptionLabel.text = Localizations.Setup.PickPresets.SpotifyDescription
        spotifyDescriptionLabel.font = Fonts.UrbanEars.Regular(13)
            
        irOptionTitle.text = Localizations.Setup.PickPresets.InternetRadioTitle
        irOptionTitle.font = Fonts.UrbanEars.Regular(16)
        irOptionDescription.text = Localizations.Setup.PickPresets.InternetRadioDescription
        irOptionDescription.font = Fonts.UrbanEars.Regular(13)
        
        updateForCurrentPickerSelectionAnimated(false)
    }
    
    func updateForCurrentPickerSelectionAnimated(_ animated: Bool) {
        
        if let vm = viewModel {
            
            if radio && spotify {
                vm.presetSelection = [PresetTypesSelection.Spotify, PresetTypesSelection.IR]
            } else if radio {
                vm.presetSelection = [PresetTypesSelection.IR]
            } else if spotify {
                vm.presetSelection = [PresetTypesSelection.Spotify]
            } else {
                vm.presetSelection = []
            }
            if vm.presetSelection.isEmpty {
                nextButton.alpha = 0.3
                nextButton.isUserInteractionEnabled = false
            } else {
                nextButton.alpha = 1.0
                nextButton.isUserInteractionEnabled = true
            }
            
            UIView.setAlphaOfView(spotifyCheck, visible: vm.presetSelection.hasSpotify , animated: animated)
            UIView.setAlphaOfView(radioCheck, visible: vm.presetSelection.hasIR , animated: animated)
        }
    }
}
