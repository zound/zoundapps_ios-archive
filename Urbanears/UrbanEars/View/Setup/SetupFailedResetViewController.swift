//
//  SetupFailedResetViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol SetupFailedResetViewControllerDelegate: class {
    
    func setupFailedResetDidRequestNext(_ viewController: SetupFailedResetViewController)
    func setupFailedResetDidRequestCancel(_ viewController: SetupFailedResetViewController)
}

class SetupFailedResetViewController: UIViewController {
    
    weak var delegate: SetupFailedResetViewControllerDelegate?
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        messageLabel.text = Localizations.Setup.FailedReset.Message
        messageLabel.font = Fonts.MainContentFont
        
        
        UIView.setAnimationsEnabled(false)
        
        nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.FailedReset.Buttons.Next), for: .normal)
        cancelButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Setup.FailedReset.Buttons.Cancel), for: .normal)
        
        nextButton.layoutIfNeeded()
        cancelButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        self.view.backgroundColor = UIColor.clear
    }
 
    @IBAction func onNext(_ sender: AnyObject) {
        
        self.delegate?.setupFailedResetDidRequestNext(self)
    }
    @IBAction func onCancel(_ sender: AnyObject) {
        
        self.delegate?.setupFailedResetDidRequestCancel(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}
