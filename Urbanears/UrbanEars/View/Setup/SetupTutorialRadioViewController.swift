//
//  SetupTutorialCloudViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 18/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

import UIKit 

class SetupTutorialRadioViewController: UIViewController {
    
    @IBOutlet weak var speakerImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
        titleLabel.text = Localizations.Setup.Tutorial.InternetRadio.Title
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        contentLabel.text = Localizations.Setup.Tutorial.InternetRadio.Content
        contentLabel.font = Fonts.MainContentFont
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        UIView.setAlphaOfView(speakerImage, visible: true, animated: animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        UIView.setAlphaOfView(speakerImage, visible: false, animated: animated)
    }
}
