//
//  SetupFailedBootViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

import UIKit

protocol SetupFailedBootViewControllerDelegate: class {
    
    func setupFailedBootDidRequestNext(_ viewController: SetupFailedBootViewController)
    func setupFailedBootDidRequestCancel(_ viewController: SetupFailedBootViewController)
}

class SetupFailedBootViewController: UIViewController {
 
    weak var delegate: SetupFailedBootViewControllerDelegate?
    @IBOutlet weak var dialImage: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
        messageLabel.text = Localizations.Setup.FailedBoot.Message
        messageLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.FailedBoot.Buttons.Next), for: .normal)
        cancelButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Setup.FailedBoot.Buttons.Cancel), for: .normal)

        nextButton.layoutIfNeeded()
        cancelButton.layoutIfNeeded()
        
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onNext(_ sender: AnyObject) {
        
        self.delegate?.setupFailedBootDidRequestNext(self)
    }
    @IBAction func onCancel(_ sender: AnyObject) {
        
        self.delegate?.setupFailedBootDidRequestCancel(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}
