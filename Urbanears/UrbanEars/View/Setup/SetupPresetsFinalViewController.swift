//
//  SetupPresetsFinalViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import AlamofireImage

protocol SetupPresetsFinalViewControllerDelegate: class {
    
    func setupPresetsFinalDidFinish(_ viewcontroller:SetupPresetsFinalViewController)
}

class SetupPresetsFinalViewController: UIViewController,UITableViewDelegate {

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var bottomSeparatorViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var separatorViewHeightConstraint: NSLayoutConstraint!
    
    var viewModel: SetupPresetsViewModel?
    weak var delegate: SetupPresetsFinalViewControllerDelegate?
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.text = Localizations.Setup.PresetsList.Title
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        UIView.setAnimationsEnabled(false)
        self.nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.PresetsList.Buttons.MoveOn), for: .normal)
        nextButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        self.separatorViewHeightConstraint.constant = 0.5
        self.bottomSeparatorViewHeightConstraint.constant = 0.5
        
        tableView.rx.setDelegate(self).disposed(by: rx_disposeBag)
        
        if let vm = viewModel {
            vm.presets.asObservable().bind(to:tableView.rx.items(cellIdentifier:"presetItem", cellType: SetupPresetCell.self)) { (row, element, cell) in
                cell.menuItemLabel?.text = element.name
                cell.menuItemDescriptionLabe?.text = element.type?.displayName()
                cell.menuItemNumberLabel.text = String(element.number)
                if let imageURL  = element.imageURL {
                    cell.menuItemImageView?.af_setImage(withURL:imageURL,
                        placeholderImage: UIImage(),
                        filter: nil,
                        progress: nil,
                        progressQueue: DispatchQueue.main,
//                        imageTransition: UIImageView.ImageTransition.crossDissolve(0.15),
                        runImageTransitionIfCached: false,
                        completion: nil)
                } else {
                    cell.menuItemImageView.image = UIImage(color: UIColor(white: 1.0, alpha: 0.05))
                }
                }.disposed(by: rx_disposeBag)
                
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt editingStyleForRowAtIndexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.none
    }
    @IBAction func onOK(_ sender: AnyObject) {
        self.delegate?.setupPresetsFinalDidFinish(self)
    }
}
