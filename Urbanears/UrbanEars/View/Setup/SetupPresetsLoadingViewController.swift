//
//  SetupPresetsLoadingViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import AlamofireImage

protocol SetupPresetsLoadingViewControllerDelegate: class {
    
    func setupPresetsLoadingDidFinishLoading(_ viewcontroller:SetupPresetsLoadingViewController)
    func setupPresetsLoadingDidSkipOnError(_ viewcontroller:SetupPresetsLoadingViewController)
}

class SetupPresetsLoadingViewController: UIViewController {
    
    var viewModel: SetupPresetsViewModel?
    weak var delegate: SetupPresetsLoadingViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    var currentLabel: UILabel?
    var otherLabel: UILabel? {
        return currentLabel != nil ? (currentLabel == statusLabel1 ? statusLabel2 : statusLabel1) : nil
    }
    var constraintForLabel:[UILabel:NSLayoutConstraint]?
    var numberLabelForLabel:[UILabel:UILabel]?
    
    var disposeBag = DisposeBag()
    
    @IBOutlet weak var ledOnImageView: UIImageView!
    @IBOutlet weak var statusLabelsContainer: UIView!
    @IBOutlet weak var statusLabelNumber1: UILabel!
    @IBOutlet weak var statusLabel1: UILabel!
    @IBOutlet weak var statusLabelNumber2: UILabel!
    @IBOutlet weak var statusLabel2: UILabel!
    
    @IBOutlet weak var statusLabel1VerticalPositionConstraint: NSLayoutConstraint!
    @IBOutlet weak var statusLabel2VerticalPositionConstraint: NSLayoutConstraint!
        
    @IBOutlet weak var uploadingPresetsContainer: UIView!
    @IBOutlet weak var preparingPresetsContainer: UIView!
    @IBOutlet weak var errorPresetsContainer: UIView!
    
    @IBOutlet weak var loadingIndicator: UIImageView!
    @IBOutlet weak var preparingPresetsLabel: UILabel!
    
    @IBOutlet weak var errorContainerTitleLabel: UILabel!
    @IBOutlet weak var errorContainerContentLabel: UILabel!
    
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    
    var loadingAnimationDone = false
    var loadingPresetsDone = false

    override func viewDidLoad() {

        super.viewDidLoad()
        
        titleLabel.text = Localizations.Setup.PresetsLoading.Title
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        preparingPresetsLabel.text = Localizations.Setup.PresetsLoading.PreparingPresets
        preparingPresetsLabel.font = Fonts.UrbanEars.Regular(16)
        
        statusLabel1.font = Fonts.UrbanEars.Regular(16)
        statusLabel2.font = Fonts.UrbanEars.Regular(16)
        
        statusLabelNumber1.font = Fonts.UrbanEars.Regular(75)
        statusLabelNumber2.font = Fonts.UrbanEars.Regular(75)
        
        
        constraintForLabel = [statusLabel1 : statusLabel1VerticalPositionConstraint,
                              statusLabel2: statusLabel2VerticalPositionConstraint]
        
        numberLabelForLabel = [statusLabel1 : statusLabelNumber1,
                              statusLabel2: statusLabelNumber2]
        
        statusLabel1.alpha = 0.0
        statusLabel2.alpha = 0.0
        statusLabelNumber1.alpha = 0.0
        statusLabelNumber2.alpha = 0.0
        
        errorContainerTitleLabel.text = Localizations.Setup.PresetsFail.Title
        errorContainerTitleLabel.font = Fonts.UrbanEars.Medium(23)
        
        errorContainerContentLabel.text = Localizations.Setup.PresetsFail.Content
        errorContainerContentLabel.font = Fonts.UrbanEars.Regular(17)
        
        
        UIView.setAnimationsEnabled(false)
        
        tryAgainButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.PresetsFail.Buttons.TryAgain), for: .normal)
        skipButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Setup.PresetsFail.Buttons.Skip), for: .normal)
        
        tryAgainButton.layoutIfNeeded()
        skipButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        
        if let vm = viewModel {
            
            vm.presetUploadStateVariable.asObservable().subscribe(weak: self, onNext: SetupPresetsLoadingViewController.updateForUploadState).disposed(by: rx_disposeBag)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    fileprivate func updateForUploadState(_ state: PresetUploadState) {
    
        let preparing = state == PresetUploadState.downloadingPresets
        let error = (state == PresetUploadState.error)
        let uploading = (state == PresetUploadState.uploadingPresets || state == PresetUploadState.done)
        
        UIView.setAlphaOfView(preparingPresetsContainer, visible: preparing, animated: true)
        
        if preparing {
            loadingIndicator.rotate()
        } else {
            loadingIndicator.stopAnimating()
        }
        
        UIView.setAlphaOfView(errorPresetsContainer, visible: error, animated: true)
        UIView.setAlphaOfView(titleLabel, visible: (preparing || uploading), animated: true)
        
        if error {
            
        }
        
        UIView.setAlphaOfView(uploadingPresetsContainer, visible: uploading, animated: true)
        
        if (state == PresetUploadState.uploadingPresets) {
            runPresetsSettingAnimation()
        }
        
        if (state == .done) {
            
            loadingPresetsDone = true
            reportUploadingFinished()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let vm = viewModel {
            
            vm.loadPresets()
        }
    }
    
    
    
    func runPresetsSettingAnimation() {
        
        let ticks = Observable<Int>.interval(1.0, scheduler: MainScheduler.instance).take(7)
        
        loadingAnimationDone = false
        if let presets = viewModel?.presets.value {
            let presetNames = presets.map { $0.name }
            
            ticks.map{ currentTick in
            
                let statuses = presetNames
                let index = currentTick % statuses.count
                
                    return (index, Localizations.Setup.PresetsLoading.AddingPresetTitle(statuses[index]))
                
                }
                .subscribe(
                    onNext: { [weak self] (index: Int, status: String) in
                        self?.showStatus(index, status: status)
                        self?.selectLedWithIndex(index)
                    },
                    onCompleted: {
                        
                        [weak self] in
                        guard let `self` = self else {return}
                        
                        self.runAfterDelay(1.0, block: {
                            self.finishedLoadingAnimation()
                        })
                        
                    })
                
                .disposed(by: disposeBag)
        }
    }
    
    func reportUploadingFinished() {
        
        if loadingAnimationDone && loadingPresetsDone {
            self.showStatus(-1, status: Localizations.Setup.PresetsLoading.Done)
            self.selectLedWithIndex(-1)
            runAfterDelay(1.0, block: { [weak self] in
                self?.delegate?.setupPresetsLoadingDidFinishLoading(self!)
            })
            
        }
    }
    
    func finishedLoadingAnimation() {
        
        loadingAnimationDone = true
        reportUploadingFinished()
    }
    
    func selectLedWithIndex(_ index: Int) {
        
        let image = UIImage(named: "led_on_0\(index)")
        UIView.transition(
            with: ledOnImageView,
            duration: 0.4,
            options: [.transitionCrossDissolve,.beginFromCurrentState],
            animations: { [weak self] in
                self?.ledOnImageView.image = image
            },
            completion: nil)
    }
    
    
    func showStatus(_ index: Int, status: String) {
        
        let scrollSize = CGFloat(50.0)
        let animationDuration = 0.35
        if currentLabel != nil {
            
            let currentLabelConstraint = constraintForLabel![currentLabel!]!
            let otherLabelConstraint = constraintForLabel![otherLabel!]!
            
            let currentNumberLabel = numberLabelForLabel![currentLabel!]
            let otherNumberLabel = numberLabelForLabel![otherLabel!]
            
            currentLabelConstraint.constant = 0
            self.currentLabel!.alpha = 1.0
            currentNumberLabel?.alpha = 0.2
            
            otherLabelConstraint.constant = scrollSize
            self.otherLabel!.alpha = 0.0
            otherNumberLabel?.alpha = 0.0
            
            self.otherLabel!.text = status
            if index >= 0 {
                otherNumberLabel?.text = String(index+1)
            } else {
                otherNumberLabel?.text = ""
            }
            
            self.statusLabelsContainer.layoutIfNeeded()
            
            currentLabelConstraint.constant = -scrollSize
            otherLabelConstraint.constant = 0
            UIView.animate(withDuration: animationDuration, animations: { [weak self] in
            
                self?.statusLabelsContainer.layoutIfNeeded()
                
                self?.currentLabel?.alpha = 0.0
                currentNumberLabel?.alpha = 0.0
                
                self?.otherLabel?.alpha = 1.0
                otherNumberLabel?.alpha = 0.2
            })
            
            self.currentLabel = self.otherLabel
            
        } else {
            
            currentLabel = statusLabel1
            
            let currentLabelConstraint = constraintForLabel![currentLabel!]!
            let currentNumberLabel = numberLabelForLabel![currentLabel!]
            
            currentLabelConstraint.constant = scrollSize
            self.currentLabel!.alpha = 0.0
            currentNumberLabel?.alpha = 0.0
            
            self.currentLabel!.text = status
            if index >= 0 {
                currentNumberLabel?.text = String(index+1)
            } else {
                currentNumberLabel?.text = ""
            }
            
            self.statusLabelsContainer.layoutIfNeeded()
            
            currentLabelConstraint.constant = 0
            
            UIView.animate(withDuration: animationDuration, animations: { [weak self] in
                
                self?.statusLabelsContainer.layoutIfNeeded()
                self?.currentLabel?.alpha = 1.0
                currentNumberLabel?.alpha = 0.2
            })
        }
        
        
    }

    @IBAction func errorContainerOnTryAgain(_ sender: Any) {
        if let vm = self.viewModel {
            self.disposeBag = DisposeBag()
            self.resetLoader()
            vm.loadPresets()
        }
    }
    
    @IBAction func errorContainerOnSkip(_ sender: Any) {
        self.delegate?.setupPresetsLoadingDidSkipOnError(self)
    }
    
    
    func resetLoader() {
        statusLabel1.alpha = 0.0
        statusLabel2.alpha = 0.0
        statusLabelNumber1.alpha = 0.0
        statusLabelNumber2.alpha = 0.0
        selectLedWithIndex(0)
    }
}
