//
//  SetupCoordinator.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 11/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import STKWebKitViewController
import SafariServices
import MessageUI
import Crashlytics
import Fabric
import MinuetSDK

protocol SetupCoordinatorDelegate: class {
    
    func setupCoordinatorDidFinishSetup(_ coordinator: SetupCoordinator)
    func setupCoordinatorDidCancelSetup(_ coordinator: SetupCoordinator)
}

class SetupCoordinator: NSObject,Coordinator
{

    var displayViewController: UIViewController
    weak var delegate: SetupCoordinatorDelegate?
    var setupViewController: SetupViewController
    let discoveryService: DiscoveryServiceType
    
    let provider:SpeakerProvider
    var wacService: WACServiceType
    
    var spotifyProvider: SpotifyProvider!
    var setupNetworkViewModel: SetupNetworkViewModel?
    
    
    var disposeBag = DisposeBag()
    
    
    init(displayViewController: UIViewController, moyaProvider:SpeakerProvider, discoveryService: DiscoveryServiceType, wacService: WACServiceType) {
        
        self.displayViewController = displayViewController
        self.provider = moyaProvider
        self.wacService = wacService
        self.discoveryService = discoveryService
        setupViewController = UIStoryboard.setup.setupViewController
        
        super.init()
        
        
        setupViewController.delegate = self;
        
    }
    
    func start(_ speaker: Speaker?) {
        
        UIApplication.shared.isIdleTimerDisabled = true
        //SpotifyTokenStore.clearToken()
        //SpotifyArtworkCache.sharedInstace.clearData()
        if speaker != nil {

           showSetupUpdate(speaker!)
            //showRenameForSpeaker(speaker!)
            //let setupPresetsVieModel = SetupPresetsViewModel(speaker: speaker!, speakerProvider: provider)
            //setupPresetsVieModel.delegate = self
            //showPresetsPicker(setupPresetsVieModel)

        } else {
            self.setupNetworkViewModel = SetupNetworkViewModel(speakerProvider: provider, wacService: wacService, discoveryService: discoveryService)
            self.setupNetworkViewModel?.delegate = self
            self.setupNetworkViewModel?.searchForSpeakers()
        
//            self.setupNetworkViewModel = SetupNetworkViewModel(speakerProvider: provider, wacService: wacService, discoveryService: discoveryService)
//            self.setupNetworkViewModel?.delegate = self
//            let unconfiguredSpeaker = UnconfiguredSpeaker(model: "Baggen Plant Green", ssid: "Baggen_Plant_Green_1f:32", wacMac: "0:22:61:1f:1f:32")
//            setupNetworkViewModel?.setupNetworkState = .configured(speaker: unconfiguredSpeaker, configureError: SetupNetworkError.other)
//             showTroubleshooting(self.setupNetworkViewModel!)
//
        //showLostConnection(setupNetworkViewModel!)
        }
        
        self.setupViewController.modalPresentationStyle = .formSheet
        
        displayViewController.present(setupViewController, animated: true, completion: {})
        
    }
    
    func start() {
       
        start(nil)
    }
    
    func showRenameForSpeaker(_ speaker: Speaker) {
        
        let rename = UIStoryboard.setup.renameViewController
        rename.viewModel = SetupRenameViewModel(speaker: speaker, speakerProvider: provider)
        rename.delegate = self
        
        setupViewController.resetWithViewController(rename, animated: true)
    }
    
    func showSpeakerList(_ viewModel: SetupNetworkViewModel) {
        
        let setupList = UIStoryboard.setup.setupListViewController
        setupList.viewModel = viewModel
        setupList.delegate = self;
        
        setupViewController.resetWithViewController(setupList, animated: false)
    }
    
    func showSpeakerConfiguring(_ viewModel: SetupNetworkViewModel) {
        
        let setupConfiguring = UIStoryboard.setup.setupConfiguringViewController
        setupConfiguring.viewModel = viewModel
        setupConfiguring.delegate = self
        
        setupViewController.resetWithViewController(setupConfiguring, animated: false)
    }
    
    func showTroubleshooting(_ viewModel: SetupNetworkViewModel) {
        
        
        let setupTroubleshooting = UIStoryboard.setup.setupTroubleshootingViewController
        setupTroubleshooting.viewModel = viewModel
        setupTroubleshooting.delegate = self
        
        setupViewController.resetWithViewController(setupTroubleshooting, animated: false)
        
    }
    
    func showLostConnection(_ viewModel: SetupNetworkViewModel) {
        
        
        let setupLostConnection = UIStoryboard.setup.setupLostConnectionViewController
        setupLostConnection.viewModel = viewModel
        setupLostConnection.delegate = self
        
        setupViewController.resetWithViewController(setupLostConnection, animated: false)
        
    }
    
    func showWACFailure(_ viewModel: SetupNetworkViewModel) {
        
        
        let setupWACFailure = UIStoryboard.setup.setupWACFailureViewController
        setupWACFailure.viewModel = viewModel
        setupWACFailure.delegate = self
        
        setupViewController.resetWithViewController(setupWACFailure, animated: false)
        
    }
    
    func showTutorial(_ viewModel: SetupTutorialViewModel) {
        
        let setupTutorial = UIStoryboard.setup.setupTutorialViewController
        setupTutorial.viewModel = viewModel
        setupTutorial.delegate = self
        
        setupViewController.resetWithViewController(setupTutorial, animated: true)
    }
    
    func showPresetsPicker(_ viewModel: SetupPresetsViewModel) {
        
        let presetsPicker = UIStoryboard.setup.setupPresetsPickerViewController
        presetsPicker.viewModel = viewModel
        presetsPicker.delegate = self
        
        setupViewController.resetWithViewController(presetsPicker, animated: true)
    }
    
    func showPresetsLoading(_ viewModel: SetupPresetsViewModel) {
        
        let presetsLoading = UIStoryboard.setup.setupPresetsLoadingViewController
        presetsLoading.viewModel = viewModel
        presetsLoading.delegate = self
        
        setupViewController.resetWithViewController(presetsLoading, animated: true)
    }
    
    func showPresetsFinal(_ viewModel: SetupPresetsViewModel) {
        
        let presetsFinal = UIStoryboard.setup.setupPresetsFinalViewController
        presetsFinal.viewModel = viewModel
        presetsFinal.delegate = self
        
        setupViewController.resetWithViewController(presetsFinal, animated: true)
    }
    
    func showPresetsTutorial(_ viewModel: SetupPresetsViewModel) {
        
        let presetsTutorial = UIStoryboard.setup.setupPresetsTutorialViewController
        presetsTutorial.viewModel = viewModel
        presetsTutorial.delegate = self
        
        setupViewController.resetWithViewController(presetsTutorial, animated: true)
    }
    
    func showPresetsSpotify(_ viewModel: SetupPresetsViewModel) {
        
        let setupSpotify = UIStoryboard.setup.setupPresetsSpotifyViewController
        setupSpotify.viewModel = viewModel
        setupSpotify.delegate = self
        
        setupViewController.pushViewController(setupSpotify, animated: true)
    }
    
    func showPresetsSpotifySuccess(_ viewModel: SetupPresetsViewModel) {
        
        let setupSpotifySuccess = UIStoryboard.setup.setupPresetsSpotifySuccessViewController
        setupSpotifySuccess.viewModel = viewModel
        setupSpotifySuccess.delegate = self
        
        setupViewController.pushViewController(setupSpotifySuccess, animated: true)
    }

    func showSetupDone() {
        
        let setupDone = UIStoryboard.setup.setupDoneViewController
        setupDone.delegate = self
        
        setupViewController.resetWithViewController(setupDone, animated: true)
    }
    
    func showSetupUpdate(_ speaker: Speaker) {
        
        let setupUpdate = UIStoryboard.setup.setupUpdateViewController
        setupUpdate.viewModel = SetupUpdateViewModel(speaker: speaker, speakerProvider: provider, discoveryService: discoveryService)
        setupUpdate.delegate = self
        
        setupViewController.resetWithViewController(setupUpdate, animated: true)
    }
    
    func showSetupFailed(_ viewModel: SetupNetworkViewModel) {
        
        let setupFailed = UIStoryboard.setup.setupFailedViewController
        setupFailed.viewModel = viewModel
        setupFailed.delegate = self
        
        setupViewController.resetWithViewController(setupFailed, animated: false)
    }
    
    func showSetupLoading(_ viewModel: SetupNetworkViewModel) {
        
        let setupLoading = UIStoryboard.setup.setupLoadingViewController
        setupLoading.viewModel = viewModel
        setupLoading.delegate = self
        
        setupViewController.resetWithViewController(setupLoading, animated: false)
    }
    
    func closeSetup() {
        
        delegate?.setupCoordinatorDidCancelSetup(self);
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}

extension SetupCoordinator: SetupConfiguringViewControllerDelegate {
    
    func setupConfiguringDidRequestTroubleshooting(_ setupConfiguringViewController: SetupConfiguringViewController) {
        
        showTroubleshooting(setupConfiguringViewController.viewModel)
    }
    
    func setupConfiguringDidRequestCancel(_ setupListViewController: SetupListViewController) {
        
        //close setup
    }
}

extension SetupCoordinator: SetupTroubleshootingViewControllerDelegate {
    
    
    func setupTroubleshootingDidRequestBack(_ setupTroubleshootingViewController: SetupTroubleshootingViewController) {
     
        showLostConnection(setupTroubleshootingViewController.viewModel)
    }
    
    func setupTroubleshootingDidRequestContactSupport(_ setupTroubleshootingViewController: SetupTroubleshootingViewController) {
        
        showContact(animated: true)
    }
    
    func showContact(animated: Bool = true) {
        
        let contact = UIStoryboard.main.contactViewController
        contact.delegate = self
        self.setupViewController.present(contact, animated: true, completion: nil)
    }
}

extension SetupCoordinator: ContactViewControllerDelegate {
    
    func contactViewControllerDidRequestBack(_ contactViewController: ContactViewController) {
        
        self.setupViewController.dismiss(animated: true, completion: nil)
    }
    
    func contactViewControllerDidRequestGotoWebsite(_ website: String, contactViewController: ContactViewController) {
        
        contactViewController.dismiss(animated: true, completion: { [weak self] in
            if let url = URL(string: website) {
                self?.showBrowserForURL(url)
            }
        })
    }
    
    func contactViewControllerDidRequestGotoSupportWebsite(_ supportWebsite: String, contactViewController: ContactViewController) {
        
        contactViewController.dismiss(animated: true, completion: { [weak self] in
            if let url = URL(string: supportWebsite) {
                self?.showBrowserForURL(url)
            }
        })
    }
    
    func contactViewControllerDidRequestSendEmailTo(_ email: String, contactViewController: ContactViewController) {
        
        contactViewController.dismiss(animated: true, completion: { [weak self] in
            self?.showEmailComposerToAddress(email)
        })
        
    }
    
    func showEmailComposerToAddress(_ address: String) {
        
        
        if MFMailComposeViewController.canSendMail() {
            
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([address])
            
            self.setupViewController.present(mail, animated: true)
        } else {
            
            let alert = UIAlertController(title: Localizations.Help.Contact.CannotSendEmail.Title,
                                          message: Localizations.Help.Contact.CannotSendEmail.Content,
                                          preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: Localizations.Help.Contact.CannotSendEmail.OkButton,
                                          style: .cancel,
                                          handler: nil))
            
            alert.addAction(UIAlertAction(title: Localizations.Help.Contact.CannotSendEmail.CopyButton,
                                          style: .default,
                                          handler: { action in
                                            
                                            UIPasteboard.general.string = address
            }))
            
            self.setupViewController.present(alert, animated: true, completion: nil)
        }
    }
    
    func showBrowserForURL(_ url: URL) {
        
        let rootVC:UIViewController = self.setupViewController
        if #available(iOS 9, *) {
            let safari = SFSafariViewController(url: url)
            safari.modalPresentationStyle = .overFullScreen
            safari.modalPresentationCapturesStatusBarAppearance = true
            rootVC.present(safari, animated: true, completion: nil)
        } else {
            let webViewController = STKWebKitModalViewController(url: url)
            rootVC.present(webViewController!, animated: true, completion: nil)
        }
        
    }

}

extension SetupCoordinator: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Swift.Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension SetupCoordinator: SetupLostConnectionViewControllerDelegate {
    
    func setupLostConnectionDidRequestReconnect(_ setupLostConnectionViewController: SetupLostConnectionViewController) {
        
        showSpeakerConfiguring(setupLostConnectionViewController.viewModel)
    }
    
    func setupLostConnectionDidRequestClose(_ setupLostConnectionViewController: SetupLostConnectionViewController) {
        
        closeSetup()
    }
    
    func setupLostConnectionDidRequestTroubleshooting(_ setupLostConnectionViewController: SetupLostConnectionViewController) {
        
        showTroubleshooting(setupLostConnectionViewController.viewModel)
    }
}

extension SetupCoordinator: SetupWACFailureViewControllerDelegate {
    
    func setupWACFailureDidRequestCloseApp(_ setupWACFailureViewController: SetupWACFailureViewController) {
    
        UserDefaults.standard.set(true, forKey: "DidCrashIntentionally")
        UserDefaults.standard.synchronize()
        fatalError("troubleshooting crash")
    }
    
    func setupWACFailureDidRequestCancelSetup(_ setupWACFailureViewController: SetupWACFailureViewController) {
        
       closeSetup()
    }
}

extension SetupCoordinator: SetupViewControllerDelegate {
    
    func setupViewContontrollerDidCancel(_ setupViewController: SetupViewController) {
        delegate?.setupCoordinatorDidCancelSetup(self);
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}

extension SetupCoordinator: SetupTutorialViewControllerDelegate {
    
    func setupTutorialControllerDidRequestNext(_ setupTutorialViewController: SetupTutorialViewController) {
        
        if let tutorialViewModel = setupTutorialViewController.viewModel {
            let setupPresetsViewModel = SetupPresetsViewModel(speaker: tutorialViewModel.speaker, speakerProvider: provider)
            setupPresetsViewModel.delegate = self
            showPresetsPicker(setupPresetsViewModel)
        }
    }
    
    func setupTutorialControllerDidRequestCancel(_ setupTutorialViewController: SetupTutorialViewController) {
        
        delegate?.setupCoordinatorDidCancelSetup(self);
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}

extension SetupCoordinator: SetupFailedViewControllerDelegate {
    
    func setupFailedDidRequestCancelSetup(_ viewcontroller: SetupFailedViewController) {
        
        delegate?.setupCoordinatorDidCancelSetup(self);
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
    
    func setupFailedDidFinishTroubleshooting(_ viewcontroller: SetupFailedViewController) {
     
        if let setupNetworkViewModel = viewcontroller.viewModel {
        
            setupNetworkViewModel.searchForSpeakers()
        }
        
    }
}

extension SetupCoordinator: SetupLoadingViewControllerDelegate {
    
    func setupLoadingDidRequestCancelSetup(_ setupLoading: SetupLoadingViewController) {
        
        delegate?.setupCoordinatorDidCancelSetup(self)
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}

extension SetupCoordinator: SetupListViewControllerDelegate {
    
    func setupListViewControllerDidPickUnconfiguredSpeaker(_ speaker: UnconfiguredSpeaker, setupListViewController: SetupListViewController) {
        
        if let viewModel = setupListViewController.viewModel {
            viewModel.configureSpeaker(speaker,viewController: setupViewController)
        }
    }
    
    func setupListViewControllerRequestCancel(_ setupListViewController: SetupListViewController) {
        
        delegate?.setupCoordinatorDidCancelSetup(self);
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}

extension SetupCoordinator: SetupNetworkViewModelDelegate {
    
    
    func setupViewModelDidCancelSpeakerConfiguration(_ setupViewModel: SetupNetworkViewModel) {
        
        
    }
    
    func setupViewModelDidFinishSpeakerConfiguration(_ setupViewModel: SetupNetworkViewModel, speaker: Speaker) {
        
        self.setupNetworkViewModel = nil
        
        provider.setNode(ScalarNode.CastTOS, value: "1", forSpeaker: speaker)
            .subscribe(onNext: { didSetTOS in }).disposed(by: disposeBag)
        
        var secondsFromGMT: Int { return NSTimeZone.local.secondsFromGMT() }
        provider.setNode(ScalarNode.UtcOffset, value: String(secondsFromGMT), forSpeaker: speaker)
            .subscribe(onNext: { didSetUtcOffset in }).disposed(by: disposeBag)
        
        showSetupUpdate(speaker)
//      showRenameForSpeaker(speaker)
    }
    
    func  setupNetworkViewModelDidChangeConfigurationState(_ setupViewModel: SetupNetworkViewModel, state: SetupConfigurationState) {
        
        switch state {
        case .idle: break
        case .loading: showSetupLoading(setupViewModel)
        case .noSpeakersFound: showSetupFailed(setupViewModel)
        case .foundSpeakers: showSpeakerList(setupViewModel)
        case .configuring: showSpeakerConfiguring(setupViewModel)
        case .configured(_ , let error):
            if error == SetupNetworkError.wacCrash {
                showWACFailure(setupViewModel)
            }
        }
    }
    
    func setupNetworkViewModelDidChangeDiscoveryState(_ setupViewModel: SetupNetworkViewModel, state: SpeakerDiscoveryState) {
        
        if case SpeakerDiscoveryState.discoveryFailed(let count) = state {
            
            if count > 2 {
                showTroubleshooting(setupViewModel)
            } else {
                
                showLostConnection(setupViewModel)
            }
        }
    }
}

extension SetupCoordinator: SetupPresetsViewModelDelegate {
    

    func setupViewModelDidLoginSpotify(_ setupViewModel: SetupPresetsViewModel) {
        
        showPresetsSpotifySuccess(setupViewModel)
    }
    
    func showSpotifyLoginErrorAlert(_ setupViewModel: SetupPresetsViewModel) {
        
        let alertController = UIAlertController(title: Localizations.Player.Preset.SpotifyLoginError.Title,
                                                message: Localizations.Player.Preset.SpotifyLoginError.Content, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: Localizations.Player.Preset.SpotifyLoginError.CancelButton, style: .cancel, handler: { [weak self] action in
            
            guard let `self` = self else {return}
            //let setupPresetsVieModel = SetupPresetsViewModel(speaker: setupViewModel.speaker, speakerProvider: self.provider)
            self.setupViewController.popViewController(true)
        }))
        alertController.addAction(UIAlertAction(title: Localizations.Player.Preset.SpotifyLoginError.RetryButton, style: .default, handler: { action in
            
            setupViewModel.loginSpotify(forceWebLogin: true)
        }))
        setupViewController.present(alertController, animated: true, completion: nil)
    }
    
    func setupViewModelDidNotLoginSpotify(_ setupViewModel: SetupPresetsViewModel, error: Swift.Error) {
        
        if error is SpotifyLoginError {
            
            if let loginError = error as? SpotifyLoginError {
                switch loginError {
                case .cancelled: break
                case .authorizationFailed(let spotifyError):
                    if ["access_denied","user_canceled"].contains(spotifyError)  {
                        //just do nothing since the whole process stops
                    } else {
                        showSpotifyLoginErrorAlert(setupViewModel)
                    }
                default:
                    showSpotifyLoginErrorAlert(setupViewModel)
                }
            }
        } else if error is SpotifyProductError {
            
            let alertController = UIAlertController(title: Localizations.Player.Preset.SpotifyPremiumError.Title,
                                                    message: Localizations.Player.Preset.SpotifyPremiumError.Content, preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: Localizations.Player.Preset.SpotifyPremiumError.DismissButton, style: .cancel, handler: { [weak self] action in
                
                guard let `self` = self else {return}
                let setupPresetsViewModel = SetupPresetsViewModel(speaker: setupViewModel.speaker, speakerProvider: self.provider)
                setupPresetsViewModel.delegate = self
                self.showPresetsPicker(setupPresetsViewModel)
                }))
            setupViewController.present(alertController, animated: true, completion: nil)
            
        }
    }

}

extension SetupCoordinator: RenameViewControllerDelegate {
    
    func renameViewControllerDidRequestTutorial(_ viewController: RenameViewController) {
     
        if let renameViewModel = viewController.viewModel {
            let tutorialViewModel = SetupTutorialViewModel(speaker: renameViewModel.speaker)
            showTutorial(tutorialViewModel)
        }
    }
    
    func renameViewControllerDidRequestSkip(_ viewController: RenameViewController) {
        
        if let renameViewModel = viewController.viewModel {
            let setupPresetsViewModel = SetupPresetsViewModel(speaker: renameViewModel.speaker, speakerProvider: provider)
            setupPresetsViewModel.delegate = self
            showPresetsPicker(setupPresetsViewModel)
        }
    }
    
    func renameViewControllerDidRequestBack(_ viewController: RenameViewController) {
        
        setupViewController.popViewController(true)
    }
}

extension SetupCoordinator: SetupPresetsTutorialViewControllerDelegate {
    
    func setupPresetsTutorialDidFinish(_ viewController: SetupPresetsTutorialViewController) {
        
        showSetupDone()
    }
    func setupPresetsTutorialDidRequestBack(_ viewController: SetupPresetsTutorialViewController) {
        
        setupViewController.popViewController(true)
    }
}


extension SetupCoordinator: SetupPresetsPickerViewControllerDelegate {
    
    
    func setupPresetsPickerDidRequestRadio(_ viewController: SetupPresetsPickerViewController) {
        
        if let setupPresetsViewModel = viewController.viewModel {
            
            showPresetsLoading(setupPresetsViewModel)
        }
    }
    
    func setupPresetsPickerDidRequestSpotify(_ viewController: SetupPresetsPickerViewController) {
        
        if let setupPresetsViewModel = viewController.viewModel {

            showPresetsSpotify(setupPresetsViewModel)
        }
       
    }
    
    func setupPresetsPickerDidRequestSpotifyAndRadio(_ viewController: SetupPresetsPickerViewController) {
        
        if let setupPresetsViewModel = viewController.viewModel {
            
            showPresetsSpotify(setupPresetsViewModel)
        }
    }
    
    func setupPresetsPickerDidRequestSkip(_ viewController: SetupPresetsPickerViewController) {
        
        showSetupDone()
    }
    
    func setupPresetsPickerDidRequestBack(_ viewController: SetupPresetsPickerViewController) {
        
        setupViewController.popViewController(true)
    }
}

extension SetupCoordinator: SetupPresetsSpotifyViewControllerDelegate {
    
    func setupPresetsSpotifyDidRequestLogin(_ viewController: SetupPresetsSpotifyViewController) {
     
        if let setupPresetsViewModel = viewController.viewModel {
            
            setupPresetsViewModel.loginSpotify(clearExistingLogin: true)
        }
    }
    
    func setupPresetsSpotifyDidRequestBack(_ viewController: SetupPresetsSpotifyViewController) {
        
        setupViewController.popViewController(true)
    }
}

extension SetupCoordinator: SetupPresetsSpotifySuccessViewControllerDelegate {
    
    func setupPresetsSpotifyDidRequestNext(_ viewController: SetupPresetsSpotifySuccessViewController) {
        
        if let setupPresetsViewModel = viewController.viewModel {
            
            showPresetsLoading(setupPresetsViewModel)
        }
    }
}

extension SetupCoordinator: SetupPresetsLoadingViewControllerDelegate {
    
    func setupPresetsLoadingDidFinishLoading(_ viewController: SetupPresetsLoadingViewController) {
        
        if let setupPresetsViewModel = viewController.viewModel {
            showPresetsFinal(setupPresetsViewModel)
        }
        
    }
    
    func setupPresetsLoadingDidSkipOnError(_ viewcontroller: SetupPresetsLoadingViewController) {
        
        if viewcontroller.viewModel != nil {
            showSetupDone()
        }
    }
}

extension SetupCoordinator: SetupPresetsFinalViewControllerDelegate {
    
    func setupPresetsFinalDidFinish(_ viewController: SetupPresetsFinalViewController) {
        
        if let setupPresetsViewModel = viewController.viewModel {
            showPresetsTutorial(setupPresetsViewModel)
        }
        
    }
}

extension SetupCoordinator: SetupDoneViewControllerDelegate {
    
    func setupDidFinish(_ viewController: SetupDoneViewController) {
        
        self.delegate?.setupCoordinatorDidFinishSetup(self)
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}

extension SetupCoordinator: SetupUpdateViewControllerDelegate {
    
    func setupUpdateDidFinish(_ viewController: SetupUpdateViewController) {
        
        if let updateViewModel = viewController.viewModel {
            showRenameForSpeaker(updateViewModel.speaker)
        }
    }
}

extension SetupCoordinator: FinalizingViewControllerDelegate {
    
    func finalizingViewControllerDidRequestBack(_ viewController: FinalizingViewController) {
        
        setupViewController.popViewController(true)
    }
    func finalizingViewControllerFinishSetup(_ viewController: FinalizingViewController) {
        
        
    }
}


