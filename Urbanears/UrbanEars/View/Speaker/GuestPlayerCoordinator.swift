//
//  GuestPlayerCoordinator.swift
//  UrbanEars
//
//  Created by Claudiu Alin Luminosu on 13/09/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import UIKit

import Foundation

import Foundation
import UIKit
import RxSwift
import STKWebKitViewController
import SafariServices

import Toast_Swift
import Cartography
import MinuetSDK
import Zound

class GuestPlayerCoordinator: PlayerCoordinator
{
    
    let speakerViewModel : SpeakerViewModel
    var playerViewController : PlayerViewController?
    var disconnectedController : UIViewController?
    
    
    
    override init(connectionManager: SpeakerConnectionManager, navigationController: UINavigationController) {
        
        
        self.speakerViewModel = SpeakerViewModel(connectionManager: connectionManager, spotifyProvider: SpotifyProvider(isUrbanearsApp: false))
        self.playerViewController = nil
        self.disconnectedController = nil
        super.init(connectionManager: connectionManager, navigationController: navigationController)
        
        self.speakerViewModel.connectionStatus.asObservable().distinctUntilChanged().subscribe(weak: self, onNext: GuestPlayerCoordinator.updateForConnected).disposed(by: rx_disposeBag)
        NotificationCenter.default.addObserver(self, selector: #selector(GuestPlayerCoordinator.rotate), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)

    }
    
    
    override func start() {
        
        self.connectionManager.connect()
        
        let playerVC = UIStoryboard.player.nowPlayingViewController
        playerVC.delegate = self
        let playerViewModel = PlayerViewModel(connectionManager: connectionManager, isGuest: true)
        
        playerVC.viewModel = playerViewModel
        
        self.playerViewController = playerVC
        self.navController.present(playerVC, animated: true, completion: nil)
    }
    
    override func stop() {
        if let spViewController = self.playerViewController {
            
            if !isStopping {
                isStopping = true
                if spViewController.presentedViewController != nil {
                    
                    spViewController.presentingViewController?.dismiss(animated: true, completion: { [weak self] in
                        
                        self?.delegate?.playerCoordinatorDidFinishCoordinating(self!)
                    })
                } else if self.disconnectedController != nil {
                    
                    self.disconnectedController?.presentingViewController?.dismiss(animated: true, completion: { [weak self] in
                        self?.delegate?.playerCoordinatorDidFinishCoordinating(self!)
                    })
                } else {
                    
                    self.delegate?.playerCoordinatorDidFinishCoordinating(self)
                }
            }
        }
    }
    
    func updateForConnected(_ connected: SpeakerConnectionStatus) {
        
        
        switch connected {
            
        case .disconnected(let error):
            
            
            if let notifierError = error {
                
                switch  notifierError {
                case .networkConnectionLost:
                    print("zzzz 1")
                case .sessionLost:
                    print("zzz 2")
                    
                    self.showDisconnectedController()
                    
                case .unknownError(let error):
                    print("zzz 3 \(String(describing: error))")
                    
                    if error == nil {
                        
                        //  self.delegate?.playerCoordinatorDidFinishCoordinating(self)
                        
                        
                        self.showDisconnectedController()
                    }
                    
                case .canceledByUser:
                    print("zzzz 4")
                    
                case .timeout:
                    print("zzz 5")
                    
                case .paused:
                    print("zzz 6")
                    
                }
            }
            
            
            
        default:
            print("ok - guest")
            
        }
    }
    
    func showDisconnectedController() {
        
        let speakerDisconnectedViewController = UIStoryboard.player.speakerDisconnectedViewController
        speakerDisconnectedViewController.viewModel = self.speakerViewModel
        speakerDisconnectedViewController.delegate = self
        self.disconnectedController = speakerDisconnectedViewController
        self.playerViewController?.present(speakerDisconnectedViewController, animated: false, completion: nil)
        
    }
}

extension GuestPlayerCoordinator : PlayerViewControllerDelegate {
    
    func playerViewControllerDidRequestDismiss(_ playerViewController: PlayerViewController) {
        self.delegate?.playerCoordinatorDidFinishCoordinating(self)
    }
    
    func playerViewControllerDidRequestVolume(_ playerViewController: PlayerViewController, fromView: UIView) {
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.appCoordinator.showVolumesFromViewCotroller(playerViewController)
        }
    }
    
    func playerViewControllerDidRequestAddPreset(_ playerViewController: PlayerViewController, fromView: UIView) {
        
    }
    
    func playerViewControllerDidRequestBrowse(_ playerViewController: PlayerViewController, fromView:UIView) {
        
    }
    
    func playerViewControllerDidRequestOpenPlaylistInfo(_ playerViewController: PlayerViewController) {
        
    }
    
}

extension GuestPlayerCoordinator  : SpeakerDisconnectedViewControllerDelegate {
    
    func speakerDisconnectedDidRequestReconnect(_ viewController: SpeakerDisconnectedViewController) {
        self.connectionManager.connect()
        self.disconnectedController?.presentingViewController?.dismiss(animated: false, completion: nil)
        //    self.playerViewController?.dismiss(animated: true, completion: nil)
    }
    
    func speakerDisconnectedDidRequestDisconnect(_ viewController: SpeakerDisconnectedViewController) {
        self.disconnectedController?.presentingViewController?.dismiss(animated: false, completion: nil)
        //    self.playerViewController?.dismiss(animated: true, completion: nil)
        self.delegate?.playerCoordinatorDidFinishCoordinating(self)
    }
}

