//
//  MultiVolumeHeaderReusableView.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 08/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Zound

class MultiVolumeHeaderReusableView: UICollectionReusableView {
    
    @IBOutlet weak var masterThumbLabelVerticalCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var masterThumbLabelCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var masterVolumeSlider: AnimatedSlider!
    @IBOutlet weak var thumbLabel: UILabel!
    
    let groupViewModelObservable: Variable<GroupVolumeViewModel?> = Variable(nil)
    var groupViewModel: GroupVolumeViewModel? {
        
        get {
            return groupViewModelObservable.value
        }
        set {
            groupViewModelObservable.value = newValue
        }
    }
    
    override func awakeFromNib() {
        
        thumbLabel.attributedText = Fonts.UrbanEars.Regular(10).AttributedTextWithString(Localizations.Volume.Headers.MasterVolumeLabel, color: UIColor(white:0.0, alpha: 0.8), letterSpacing: 1.5)
        
        masterVolumeSlider.setThumbImage(UIImage(named: "master_slider_thumb"), for: .normal)
        masterVolumeSlider.setMinimumTrackImage(UIImage(named:"slider_track_min"), for: .normal)
        masterVolumeSlider.setMaximumTrackImage(UIImage(named:"slider_track_max"), for: .normal)
        
        
        masterVolumeSlider.addTarget(self, action: #selector(onVolumeTouchDown), for: .touchDown)
        masterVolumeSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpInside)
        masterVolumeSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpOutside)
        
        masterThumbLabelVerticalCenterConstraint.constant = -1.0

        
        let masterVolume = groupViewModelObservable.asObservable().flatMapLatest{ viewModel -> Observable<Float> in
            guard let `viewModel` = viewModel else { return Observable.just(0)}
            return viewModel.volume.asObservable()
                .filter({$0.masterVolume != nil})
                .map{ $0.masterVolume}
                .unwrapOptional()
                .map{ masterVolume in
                
                Float(Float(masterVolume)/32.0)
            }
        }
        
        groupViewModelObservable.asObservable().unwrapOptional()
            .flatMapLatest{ $0.volume.asObservable() }
            .filter({$0.masterVolume != nil})
            .map{ $0.isMute }
            .subscribe(onNext:{[weak self] mute in
                self?.masterVolumeSlider.alpha = mute ? 0.3 : 1.0
            }).disposed(by: rx_disposeBag)
        
        masterVolumeSlider.willSetValueFromUserInteraction = true
        masterVolume.subscribe(onNext:{ [weak self] masterVolumeNormalized in
            
            if self?.masterVolumeSlider.willSetValueFromUserInteraction == false {
                self?.masterVolumeSlider.setValue(masterVolumeNormalized, animated: true)
                DispatchQueue.main.async {
                    
                    self?.updateSliderLabel()
                }
            }
            
        })
            .disposed(by: rx_disposeBag)
        masterVolumeSlider.willSetValueFromUserInteraction = false
        
        groupViewModelObservable.asObservable().flatMapLatest{ viewModel -> Observable<Bool> in
            guard let `viewModel` = viewModel else { return Observable.just(false)}
            return viewModel.hasMaster.asObservable()
            }
        .subscribe(weak: self, onNext: MultiVolumeHeaderReusableView.updateForMultiSliderEnabled)
        .disposed(by: rx_disposeBag)
    }
    
    func updateForMultiSliderEnabled(_ isEnabled:Bool) {
        
        UIView.animate(withDuration: 0.25, delay: 0, options: [.beginFromCurrentState], animations: {[weak self] in
            self?.masterVolumeSlider.alpha = isEnabled ? 1.0 : 0.5
            self?.thumbLabel.alpha =  isEnabled ? 1.0 : 0.5
        }, completion: nil)
        
        self.masterVolumeSlider.isUserInteractionEnabled = isEnabled
        
    }
    
    func updateSliderLabel() {
        
        let trackRect = self.masterVolumeSlider.trackRect(forBounds: self.masterVolumeSlider.bounds)
        let thumbRect = self.masterVolumeSlider.thumbRect(forBounds: self.masterVolumeSlider.bounds, trackRect: trackRect, value: self.masterVolumeSlider.value)
        
        let thumbCenterSlidingWidth = trackRect.midX-trackRect.origin.x+2.5
        
        masterThumbLabelCenterConstraint.constant = -thumbCenterSlidingWidth+thumbRect.midX+1
        
        if self.masterVolumeSlider.willSetValueFromUserInteraction == false {
            UIView.animate(withDuration: 0.10, delay: 0.0, options: [.allowUserInteraction, .beginFromCurrentState], animations: { [weak self] in
                self?.thumbLabel.superview!.layoutSubviews()
                }, completion: nil)
        }
    }
    
    @IBAction func onVolumeTouchUp(_ sender: AnyObject) {
        
        self.masterVolumeSlider.willSetValueFromUserInteraction = false
        if let vm = groupViewModel {
            vm.stopSettingMasterVolume()
        }
    }
    
    @IBAction func onVolumeTouchDown(_ sender: AnyObject) {
        
        self.masterVolumeSlider.willSetValueFromUserInteraction = true
        if let vm = groupViewModel {
            vm.startSettingMasterVolume()
        }
    }
    
    @IBAction func onSliderChanged(_ sender: AnyObject) {
        
        updateSliderLabel()
        
        if let vm = groupViewModel {
            vm.setMasterVolume(self.masterVolumeSlider.value)
        }
        
    }
}
