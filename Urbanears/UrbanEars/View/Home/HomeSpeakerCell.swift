//
//  HomeSpeakerCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 04/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import BetterSegmentedControl
import MinuetSDK
import Zound

protocol HomeSpeakerCellDelegate: class {
    
    func homeSpeakerCell(_ cell: HomeSpeakerCell, didRequestSwitchMultiToMulti: Bool)
    func homeSpeakerCellDidRequestCastInfo(_ cell: HomeSpeakerCell)
    func homeSpeakerCellDidRequestSpeakerSettings(_ cell:HomeSpeakerCell)
}


enum MultiSwitchState: UInt {
    
    case solo = 0
    case multi
}

class HomeSpeakerCell: UITableViewCell {
    
    @IBOutlet weak var speakerImage: UIImageView!
    @IBOutlet weak var showSettingsButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var soloMultiSwitch: BetterSegmentedControl!
    @IBOutlet weak var spinnerImageView: UIImageView!
    @IBOutlet weak var multiLoadingIndicator: UIImageView!
    @IBOutlet weak var playIndicatorView: PlayIndicatorView!
    @IBOutlet weak var statusLabelBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var speakerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var speakerToNameConstaint: NSLayoutConstraint!
    @IBOutlet weak var contentRightConstaint: NSLayoutConstraint!
    
    @IBOutlet weak var speakerBottomConstraint: NSLayoutConstraint!
    
    static var cellHeight: CGFloat {
        
        if UIScreen.main.bounds.size.width < 375 {
            return 116.0
        }
        return 116.0
    }
    
    static var verticalMargin: CGFloat {
        
        if UIScreen.main.bounds.size.width < 375 {
            return 5.0
        }
        return 13.0
    }

    let viewModel: Variable<HomeSpeakerViewModel?> = Variable(nil)
    var updatedWithNewInfo = false
    weak var delegate: HomeSpeakerCellDelegate?
    
    
    let isSwitchingMulti = Variable(false)
    
    var multiSwitchTimeout: Disposable?
    
    override func prepareForReuse() {
        
        stopMultiSwitchTimeout()
        updatedWithNewInfo = false
        UIView.setAlphaOfView(multiLoadingIndicator, visible: false, animated: false)
        isSwitchingMulti.value = false
        playIndicatorView.startAnimation()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        let backgroundColor = soloMultiSwitch.backgroundColor
        super.setSelected(selected, animated: animated)
        if selected {
            self.soloMultiSwitch.backgroundColor = backgroundColor
            self.playIndicatorView.barViews.forEach{ $0.backgroundColor = UIColor.white }
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
        let backgroundColor = soloMultiSwitch.backgroundColor
        super.setHighlighted(highlighted, animated: animated)
        if highlighted {
            self.soloMultiSwitch.backgroundColor = backgroundColor
            self.playIndicatorView.barViews.forEach{ $0.backgroundColor = UIColor.white }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        self.speakerBottomConstraint.constant = HomeSpeakerCell.verticalMargin
        self.speakerTopConstraint.constant = HomeSpeakerCell.verticalMargin
        
        self.soloMultiSwitch.titles = [Localizations.Home.Speaker.SwitchLabel.Solo, Localizations.Home.Speaker.SwitchLabel.Multi]
        self.soloMultiSwitch.titleFont = Fonts.UrbanEars.Regular(11)
        self.soloMultiSwitch.selectedTitleFont = self.soloMultiSwitch.titleFont
        self.soloMultiSwitch.indicatorViewInset = 0
        self.soloMultiSwitch.cornerRadius = 3
        
        self.playIndicatorView.startAnimation()
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1.0, alpha: 0.1);
        self.selectedBackgroundView =  customColorView;
        
        self.showSettingsButton.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        self.showSettingsButton.imageView?.contentMode = .scaleAspectFit
        
        statusLabel.font = Fonts.UrbanEars.Regular(12)
        
        if UIScreen.main.bounds.size.width < 375 {
            speakerToNameConstaint.constant = -15.0
            nameLabel.font = Fonts.UrbanEars.Regular(15)
            contentRightConstaint.constant = 24.0
        } else {
            speakerToNameConstaint.constant = -15.0
            nameLabel.font = Fonts.UrbanEars.Regular(17)
            contentRightConstaint.constant = 30.0
        }
        
        
        viewModel.asObservable()
            .flatMapLatest{ $0 != nil ? $0!.nowPlayingDescription.asObservable() : Observable.just(nil) }
            .subscribe(weak: self, onNext: HomeSpeakerCell.updateDescriptionWithText).disposed(by: rx_disposeBag)
        
        viewModel.asObservable().map{ $0?.speaker.friendlyNameWithIndex ?? "" }.bind(to:nameLabel.rx.text).disposed(by: rx_disposeBag)
        
        let speakerImageObservable = viewModel.asObservable()
            .map{ ExternalConfig.sharedInstance.speakerImageForColor($0?.speaker.color)?.listItemImageName }
            .map{ ($0 != nil ? UIImage(named:$0!) : UIImage(named: "list_item_placeholder")) }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let audioSystem = appDelegate.appCoordinator.audioSystem
        let isUnlocked = viewModel.asObservable().map { $0 != nil ? $0!.speaker : nil }.map { (speaker : Speaker?) -> Bool in
            if let currentSpeaker = speaker {
               
                if audioSystem.isSpeakerUnlocked(currentSpeaker) {
                    NSLog("speaker %@ is unlocked", currentSpeaker.friendlyName)
                } else {
                    NSLog("speaker %@ is not unlocked", currentSpeaker.friendlyName)
                }
                
                return audioSystem.isSpeakerUnlocked(currentSpeaker)
            }
            return false
        }

        
        Observable.combineLatest(speakerImageObservable, isUnlocked) { (speakerImageObservable, isUnlocked) -> (speakerImageObservable: UIImage, isUnlocked: Bool) in
            
            return (speakerImageObservable: speakerImageObservable!, isUnlocked: isUnlocked)
            }.subscribe(onNext:{ [weak self] (speakerImageObservable: UIImage, isUnlocked: Bool) in
                
                if isUnlocked == true {
                    self?.updateSpeakerImage(speakerImg: UIImage(named: "list_item_prototype")!)
                } else {
                    self?.updateSpeakerImage(speakerImg: speakerImageObservable)
               }
                
            }).disposed(by: rx_disposeBag)
        
        viewModel.asObservable()
            .flatMapLatest{ $0 != nil ? $0!.multi.asObservable() : Observable.just(HomeSpeakerMultiState.unknown) }
            .subscribe(weak: self, onNext: HomeSpeakerCell.updateForMultiState).disposed(by: rx_disposeBag)
        
        self.isSwitchingMulti.asObservable().subscribe(weak: self, onNext: HomeSpeakerCell.updateMultiIndicatorVisible).disposed(by: rx_disposeBag)
        
        viewModel.asObservable()
            .flatMapLatest{ $0 != nil ? $0!.isPlaying.asObservable() : Observable.just(false) }
            .subscribeNext(weak: self, HomeSpeakerCell.updatePlayingIndicatorWithVisibility).disposed(by: rx_disposeBag)
        
        let isEnabled = viewModel.asObservable().flatMapLatest{ $0 != nil ? $0!.isEnabled.asObservable() : Observable.just(true) }
        let isLoading = viewModel.asObservable().flatMapLatest{ $0 != nil ? $0!.loading.asObservable() : Observable.just(false) }
        let isConnectable = viewModel.asObservable().map{ ExternalConfig.sharedInstance.speakerImageForColor($0?.speaker.color) }.map { $0?.connectable }.map{ $0 != nil ? $0! : false }
        
        
        isLoading.subscribe(weak: self, onNext: HomeSpeakerCell.setActivityIndicatorVisiblity).disposed(by: rx_disposeBag)
        isEnabled.subscribeNext(weak: self, HomeSpeakerCell.updateForIsEnabled).disposed(by: rx_disposeBag)
        
        Observable.combineLatest(isEnabled,isLoading,isConnectable) { isEnabled, isLoading, isConnectable in
            return isConnectable && !isLoading && isEnabled
            }.map(!).bind(to:showSettingsButton.rx.isHidden).disposed(by: rx_disposeBag)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeSpeakerCell.handleNotification(notification:)), name: Notification.Name("UnlockedFound"), object: nil)

    }
    
    
    func handleNotification(notification: UIKit.Notification) {
        if let speakerUnlockDict = notification.userInfo {
            let speakerMac = speakerUnlockDict["speakerMac"] as! String
            if viewModel.value?.speaker.mac == speakerMac {
                self.updateSpeakerImage(speakerImg: UIImage(named: "list_item_prototype")!)
            }
        }
    }
    
    func updateSpeakerImage(speakerImg : UIImage) {
        self.speakerImage.image = speakerImg
        self.speakerImage.contentMode = .scaleAspectFit
    }
    
    func updateForIsEnabled(_ isEnabled: Bool) {
        
        UIView.animate(withDuration: 0.25, delay: 0, options: [.beginFromCurrentState], animations: { [weak self] in
        
            self?.speakerImage?.alpha = isEnabled ? 1.0 : 0.5
            self?.nameLabel?.alpha = isEnabled ? 1.0 : 0.5
        }, completion: nil)
        
        self.selectionStyle = isEnabled ? .default : .none
        
    }
    
    func updatePlayingIndicatorWithVisibility(visible: Bool) {
        
        UIView.setAlphaOfView(playIndicatorView, visible: visible, animated: true)
        if visible {
            statusLabelBottomConstraint.constant = -1
        } else {
            statusLabelBottomConstraint.constant = -15
        }
        if updatedWithNewInfo == true {
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
                
                self?.statusLabel.alpha = visible ? 1.0 : 0.0
                self?.playIndicatorView.alpha = visible ? 1.0 : 0.0
                self?.statusLabel.superview?.layoutIfNeeded()
                }, completion: nil)
        } else {
            
            self.statusLabel.alpha = visible ? 1.0 : 0.0
            self.playIndicatorView.alpha = visible ? 1.0 : 0.0
        }
        updatedWithNewInfo = true
    }
    
    func updateMultiIndicatorVisible(visible: Bool) {
        
        UIView.setAlphaOfView(multiLoadingIndicator, visible: visible, animated: true)
        
        if visible {
            self.multiLoadingIndicator.rotate(0.75)
        } else {
            self.multiLoadingIndicator.stopRotation()
        }
    }
    
    
    
    func updateDescriptionWithText(_ text: String?) {
        
        statusLabel.text = text
    }
    
    func updateForMultiState(_ multi: HomeSpeakerMultiState) {
        
        isSwitchingMulti.value = false
        soloMultiSwitch.removeTarget(self, action: #selector(onMultiChanged), for: .valueChanged)
        defer {
            soloMultiSwitch.addTarget(self, action: #selector(onMultiChanged), for: .valueChanged)
        }
        do {
            
            switch multi {
                
            case .loading, .notSupported, .unknown:
                
                soloMultiSwitch.isHidden = true
                try soloMultiSwitch.setIndex(MultiSwitchState.solo.rawValue, animated: false)
            case .solo:
                
                soloMultiSwitch.isHidden = false
                try soloMultiSwitch.setIndex(MultiSwitchState.solo.rawValue, animated: false)
                
                
            case .multi:
                
                soloMultiSwitch.isHidden = false
                try soloMultiSwitch.setIndex(MultiSwitchState.multi.rawValue, animated: false)
            }
        }
        catch {
            //not really a posiblity
        }
        
    }
    @IBAction func onMultiChanged(_ sender: Any) {
        
        let currentState = self.viewModel.value!.multi.value == .multi
        let newValue = soloMultiSwitch.index == MultiSwitchState.multi.rawValue
        if newValue != currentState {
            startMultiSwitchTimeout()
            isSwitchingMulti.value = newValue != currentState
        }
        
        self.delegate?.homeSpeakerCell(self, didRequestSwitchMultiToMulti: newValue)
    }
    
    func startMultiSwitchTimeout() {
        
        stopMultiSwitchTimeout()
        multiSwitchTimeout = Observable<Int>.timer(6.0, scheduler: MainScheduler.instance).subscribe(weak: self, onNext: HomeSpeakerCell.didTimeoutMultiSwitch)
        multiSwitchTimeout!.disposed(by: rx_disposeBag)
    }
    
    func didTimeoutMultiSwitch(_ : Int = 0) {
        stopMultiSwitchTimeout()
        self.viewModel.value?.multi.value = self.viewModel.value!.multi.value
    }
    
    func stopMultiSwitchTimeout(_ : Int = 0) {
        
        multiSwitchTimeout?.dispose()
        multiSwitchTimeout = nil
        isSwitchingMulti.value = false
    }
    
    @IBAction func onCastInfoButton(_ sender: AnyObject) {
        
        self.delegate?.homeSpeakerCellDidRequestCastInfo(self)
    }
    
    @IBAction func onShowSpeakerSettings(_ sender: AnyObject) {
        self.delegate?.homeSpeakerCellDidRequestSpeakerSettings(self)
    }
    
    func setActivityIndicatorVisiblity(_ spinnerImageViewVisibility: Bool) {
        
        spinnerImageView.isHidden = !spinnerImageViewVisibility
        if spinnerImageViewVisibility {
            spinnerImageView.rotate(0.75)
        } else  {
            spinnerImageView.stopRotation()
        }
    }
}
