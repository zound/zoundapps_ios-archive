//
//  HomeTableViewCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 22/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol HomeGroupCellDelegate: class {
    
    func homeGroupCellDidSelectSpeakerViewModel(_ speakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeGroupCellDidRequestSetupForSpeakerViewModel(_ speakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeGroupCellDidRequestFactoryResetForSpeakerViewModel(_ speakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeGroupCellDidRequestChangeMultiTo(_ multi: Bool, speakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeGroupCellDidRequestCastMultiroomInfo(_ speakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeGroupCellDidRequestSettings(_ speakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
}

class HomeGroupCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {

    
    weak var delegate: HomeGroupCellDelegate?
    @IBOutlet weak var tableView: UITableView!
    weak var viewModel: HomeGroupViewModel? {
       
        didSet {
            viewModel?.delegate = self
            self.tableViewHeight.constant = 5 * HomeSpeakerCell.cellHeight
            self.tableView.reloadData()
        }
    }
    
    override func prepareForReuse() {
        
        if let vm = viewModel {
        
            if let delegate = vm.delegate as? HomeGroupCell,
                delegate == self {
                vm.delegate = nil
            }
            
            self.viewModel = nil
            self.tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1.0, alpha: 0.3);
        self.selectedBackgroundView =  customColorView;
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        
        
        
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        self.tableView.addGestureRecognizer(longPressGesture)
        
        
        let twoFingerLongPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handle2FingerLongPress))
        twoFingerLongPressGesture.minimumPressDuration = 1.0 // 1 second press
        twoFingerLongPressGesture.numberOfTouchesRequired = 2
        self.tableView.addGestureRecognizer(twoFingerLongPressGesture)
    }
    
    func handle2FingerLongPress(_ longPressGesture: UIGestureRecognizer) {
        
        let p = longPressGesture.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: p)
        
        if indexPath == nil {
            NSLog("Long press on table view, not row.")
        }
        else if (longPressGesture.state == UIGestureRecognizerState.began) {
            NSLog("Long press on row, at \((indexPath! as NSIndexPath).row)")
            if let vm = viewModel {
                let speakerViewModel = vm.speakersViewModels.value[indexPath!.row]
                self.delegate?.homeGroupCellDidRequestFactoryResetForSpeakerViewModel(speakerViewModel, fromGroupViewModel: vm)
            }
        }
    }
    
    func handleLongPress(_ longPressGesture: UIGestureRecognizer) {
        
        let p = longPressGesture.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: p)
        
        if indexPath == nil {
            NSLog("Long press on table view, not row.")
        }
        else if (longPressGesture.state == UIGestureRecognizerState.began) {
            NSLog("Long press on row, at \((indexPath! as NSIndexPath).row)")
            if let vm = viewModel {
                let speakerViewModel = vm.speakersViewModels.value[indexPath!.row]
                self.delegate?.homeGroupCellDidRequestSetupForSpeakerViewModel(speakerViewModel, fromGroupViewModel: vm)
            }
        }
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let vm = viewModel {
            let speakerViewModel = vm.speakersViewModels.value[indexPath.row]
            
            self.delegate?.homeGroupCellDidSelectSpeakerViewModel(speakerViewModel, fromGroupViewModel: vm)
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let vm = viewModel {
           return vm.speakersViewModels.value.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "homeSpeakerItem") as? HomeSpeakerCell
            else {
                return UITableViewCell()
        }
        if let vm = viewModel {
            
            cell.viewModel.value = vm.speakersViewModels.value[indexPath.row]
            cell.delegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return HomeSpeakerCell.cellHeight
    }
   
}

extension HomeGroupCell: HomeGroupViewModelDelegate {
    
    func homeGroupViewModelDidAddSpeaker(atIndex: Int, fromMove: Bool, homeGroupViewModel: HomeGroupViewModel) {
        
        if let group = self.viewModel?.group, group == homeGroupViewModel.group {
            let indexPath = IndexPath(row: atIndex, section: 0)
            self.tableView.insertRows(at: [indexPath], with: fromMove ? .left : .fade)
        }
    }
    
    func homeGroupViewModelDidRemoveSpeaker(fromIndex: Int, fromMove: Bool, homeGroupViewModel: HomeGroupViewModel) {
        
        if let group = self.viewModel?.group, group == homeGroupViewModel.group {
            let indexPath = IndexPath(row: fromIndex, section: 0)
            self.tableView.deleteRows(at: [indexPath], with: fromMove ? .left : .fade)
        }
    }
    
    func homeGroupViewModelDidMoveSpeaker(fromIndex: Int, toIndex: Int, homeGroupViewModel: HomeGroupViewModel) {
        
        if let group = self.viewModel?.group, group == homeGroupViewModel.group {
            let fromIndexPath = IndexPath(row: fromIndex, section: 0)
            let toIndexPath = IndexPath(row: toIndex, section: 0)
            self.tableView.moveRow(at: fromIndexPath, to: toIndexPath)
        }
    }
}

extension HomeGroupCell: HomeSpeakerCellDelegate {
    
    func homeSpeakerCell(_ cell: HomeSpeakerCell, didRequestSwitchMultiToMulti multi: Bool) {
        
        if let vm = self.viewModel, let cellVM = cell.viewModel.value {
            self.delegate?.homeGroupCellDidRequestChangeMultiTo(multi, speakerViewModel: cellVM , fromGroupViewModel: vm)
        }
        
    }
    
    func homeSpeakerCellDidRequestCastInfo(_ cell: HomeSpeakerCell) {
        
        self.delegate?.homeGroupCellDidRequestCastMultiroomInfo(cell.viewModel.value!, fromGroupViewModel: self.viewModel!)
    }
    
    func homeSpeakerCellDidRequestSpeakerSettings(_ cell: HomeSpeakerCell) {
         self.delegate?.homeGroupCellDidRequestSettings(cell.viewModel.value!, fromGroupViewModel: self.viewModel!)
    }
}
