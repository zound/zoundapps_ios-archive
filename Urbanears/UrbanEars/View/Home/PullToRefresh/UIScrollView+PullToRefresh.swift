//
//  Created by Anastasiya Gorban on 4/14/15.
//  Copyright (c) 2015 Yalantis. All rights reserved.
//
//  Licensed under the MIT license: http://opensource.org/licenses/MIT
//  Latest version can be found at https://github.com/Yalantis/PullToRefresh
//

import Foundation
import UIKit
import ObjectiveC

private var topPullToRefreshKey: UInt8 = 0

public extension UIScrollView {
    
    fileprivate(set) var topPullToRefresh: PullToRefresh? {
        get {
            return objc_getAssociatedObject(self, &topPullToRefreshKey) as? PullToRefresh
        }
        set {
            objc_setAssociatedObject(self, &topPullToRefreshKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    public func addPullToRefresh(_ pullToRefresh: PullToRefresh, action: @escaping () -> ()) {
        pullToRefresh.scrollView = self
        pullToRefresh.action = action
        
        var originY: CGFloat
        let view = pullToRefresh.refreshView
        
        
        if let previousPullToRefresh = topPullToRefresh {
            removePullToRefresh(previousPullToRefresh)
        }
        
        topPullToRefresh = pullToRefresh
        originY = -view.frame.size.height - self.contentInset.top
        
        
        
        view.frame = CGRect(x: 0, y: originY, width: frame.width, height: view.frame.height)
        
        addSubview(view)
        sendSubview(toBack: view)
    }
    
    func removePullToRefresh(_ pullToRefresh: PullToRefresh) {
        
        topPullToRefresh?.refreshView.removeFromSuperview()
        topPullToRefresh = nil
    }
    
    func startRefreshing() {
        
        topPullToRefresh?.startRefreshing()
            
    }
    
    func endRefreshing() {
        
        topPullToRefresh?.endRefreshing()
    }
}
