//
//  UnlockedAlertViewController.swift
//  UrbanEars
//
//  Created by Claudiu Alin Luminosu on 10/10/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Zound

protocol UnlockedViewControllerDelegate : class {
    func didAcknowledgeUnlockedModuleWarning(_ alertViewController : UnlockedAlertViewController)
}

class UnlockedAlertViewController: UIViewController, BlurViewController {

    var dontShow: Bool = false
    weak var delegate: UnlockedViewControllerDelegate?
    var speakerViewModel: HomeSpeakerViewModel?
    var groupViewModel: HomeGroupViewModel?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleLabel.text = Localizations.UnlockedModule.Title
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        contentLabel.text = Localizations.UnlockedModule.Content((speakerViewModel?.speaker.friendlyName)!)
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        continueButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.Back), for: .normal)
        
        continueButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onClose(_ sender: AnyObject) {
        
        self.delegate?.didAcknowledgeUnlockedModuleWarning(self)
    }
}
