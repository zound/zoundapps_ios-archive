//
//  WelcomeViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import AVKit
import AVFoundation

protocol WelcomeViewControllerDelegate:class {
    
    func welcomeViewControllerDidAccept(_ welcomeViewController: WelcomeViewController)
    func welcomeViewControllerDidRequestTerms(_ welcomeViewController: WelcomeViewController)
}

class WelcomeViewController:UIViewController {
    
    weak var delegate: WelcomeViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var setupTextView: UITextView!
    @IBOutlet weak var setupTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var placeholderImage: UIImageView!
    var playerLayer: AVPlayerLayer?
    var player: AVPlayer?
    var openingLink = false
    var isLooping = false
    
    typealias ReplaceToken = (text: String, link: String)
    let termsText = Localizations.Welcome.Description.Text
    let tokenReplacements = ["[terms_link]": ReplaceToken(text:Localizations.Welcome.Description.TermsLinkText, link:"terms://"),
                             "[google_terms_link]": ReplaceToken(text:Localizations.Welcome.Description.GoogleTermsText, link:"terms://"),
                             "[google_privacy_link]": ReplaceToken(text:Localizations.Welcome.Description.GooglePrivacyText, link:"terms://")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.UrbanEars.ExtraLight(50).AttributedTextWithString(Localizations.Welcome.Title, color: titleLabel.textColor, letterSpacing: -1.0)
        
        UIView.setAnimationsEnabled(false)
        
        acceptButton.setAttributedTitle(Fonts.ButtonFont.AttributedTextWithString(Localizations.Welcome.Buttons.Accept, letterSpacing: 1.5), for: .normal)
        
        acceptButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        
        titleLabel.adjustsFontSizeToFitWidth = true
        
        
        let termsFont = Fonts.UrbanEars.Regular(10.5)
        let termsColor = UIColor("#CDCDCD")
        let linksFont = Fonts.UrbanEars.Bold(10.5)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let termsAttributes = [NSFontAttributeName:termsFont,
                               NSForegroundColorAttributeName:termsColor,
                               NSParagraphStyleAttributeName: paragraphStyle]
        let linkAttributes = [NSFontAttributeName:linksFont,
                              NSForegroundColorAttributeName:termsColor]
        
        
        let termsAttributedText = NSMutableAttributedString(string: termsText, attributes:  termsAttributes)
        for token in tokenReplacements.keys {
            let tokenRange = (termsAttributedText.string as NSString).range(of: token)
            if tokenRange.length != 0 {
                let replaceToken = tokenReplacements[token]!
                let linkAttributes = [
                    NSFontAttributeName:linksFont,
                    NSForegroundColorAttributeName:termsColor,
                    NSLinkAttributeName: replaceToken.link] as [String : Any]
                let link = NSAttributedString(string: replaceToken.text, attributes: linkAttributes)
                termsAttributedText.replaceCharacters(in: tokenRange, with: link)
            }
        }
        
        setupTextView.attributedText = termsAttributedText
        setupTextView.linkTextAttributes = linkAttributes
        
        setupTextView.rx.observe(CGSize.self, "contentSize").map{ $0 != nil ? $0!.height : 0}.bind(to:setupTextViewHeight.rx.constant).disposed(by: rx_disposeBag)
        
        setupTextView.disableTextSelection()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: NSNotification.Name(rawValue: "enterBackground"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: NSNotification.Name(rawValue: "exitBackground"), object: nil)
            
        initVideoPlayer()
        animateControls()
    }

    
    func willEnterForeground() -> Void {
        
        if self.navigationController?.viewControllers.last == self {
            delay(0.4, closure: { [weak self] in
                self?.startLooping()
            })
        }
        
    }
    
    func didEnterBackground() -> Void {
    
        self.stopLooping()
        self.placeholderImage.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {

        startLooping()
        openingLink = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        stopLooping()
    }
    
    override func viewDidLayoutSubviews() {
        
        playerLayer?.frame = view.frame
    }

    
    func animateControls() {
        
        titleLabel.alpha = 0.0
        acceptButton.alpha = 0.0
        setupTextView.alpha = 0.0
        
        UIView.animate(withDuration: 3.0, delay: 3.0, options: [.beginFromCurrentState], animations: { [weak self] in
        
            self?.titleLabel.alpha = 1.0
            self?.acceptButton.alpha = 1.0
            self?.setupTextView.alpha = 1.0
            
        }, completion: nil)
    }
    
    func initVideoPlayer() {
        
        if let videoURL = Bundle.main.url(forResource: "intro", withExtension: "mp4") {
            
            
            _ = try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
            
            player = AVPlayer(url: videoURL)
            player?.actionAtItemEnd = .none
            player?.isMuted = false
            
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            playerLayer?.zPosition = -1
            
            playerLayer?.frame = view.frame
            
            view.layer.addSublayer(playerLayer!)
            
            if let player = player {
                player.rx.observe(AVPlayerStatus.self, "status").filter({ $0 != nil && $0! == .readyToPlay}).subscribe( onNext: { [weak self] status in
                    if let status = status {
                        self?.didChangePlayerStatusTo(status: status)
                    }
                }).disposed(by: rx_disposeBag)
            }
        }
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        player = nil
    }
    
    func didChangePlayerStatusTo(status: AVPlayerStatus) {
        
        if case AVPlayerStatus.readyToPlay = status {
            self.placeholderImage.isHidden = true
        }
    }
    
    func startLooping() {
        if self.isLooping == false {

        NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        loopVideo()
            self.isLooping = true
            
        }
    }
    
    func stopLooping() {
        if self.isLooping == true {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
            
            DispatchQueue.main.async { [weak self] in
                self?.isLooping = false
                self?.player?.pause()
                NSLog("xxx stoping player loop")
            }
            
        }
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    func loopVideo() {
        
        DispatchQueue.main.async { [weak self] in
            let t1 = CMTimeMake(5, 100);
            self?.player?.seek(to: t1)
            self?.player?.play()
            self?.placeholderImage.isHidden = true
        }

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onAccept(_ sender: AnyObject) {
        
        self.delegate?.welcomeViewControllerDidAccept(self)
    }
    
}

extension WelcomeViewController: UITextViewDelegate {
    
    @available(iOS 10,*)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        if interaction == .invokeDefaultAction {
            if URL.absoluteString == "terms://" {
                
                self.delegate?.welcomeViewControllerDidRequestTerms(self)
                
            }
        }
        return false
    }
    
    @available(iOS 9,*)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        if URL.absoluteString == "terms://" {
            if !openingLink {
                openingLink = true
                self.delegate?.welcomeViewControllerDidRequestTerms(self)
            }
        }
        return false
    }
    
    
    
}
