//
//  AppCoordinator.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 11/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import WebKit
import STKWebKitViewController
import SafariServices
import Crashlytics
import Fabric
import MinuetSDK
import Zound

protocol Coordinator: class {
    
    func start()
}

enum AppStatus {
    
    case none
    case termsNotAccepted
    case loading
    case wifiNotAvailable
    case noSpeakersAvailable
    case speakersAvailable
}

enum WarningMessageType {
    case updateMessage
    case unlockedMessage
}

class AppCoordinator: RunAfterDelay {
 
    let mock = false
    
    fileprivate let unlockedMacPrefixForUserStore = "unlocked_"
    fileprivate let updateMacPrefixForUserStore = "updated_"
    
    let rootViewController: UIViewController
    
    let allowSpeakersToBeResetOrSetupForDebug = false
    
    let provider: SpeakerProvider
    
    var childCoordinators: [Coordinator] = []
    var menuViewModel: MenuViewModel
    var navController: UINavigationController?
    var disposeBag = DisposeBag()
    
    let appStatus: Variable<AppStatus> = Variable(.none)
    let initialScan: Variable<Bool> = Variable(false)
    var initialScanDisposable: Disposable?
    let discoveryService: DiscoveryServiceType
    let termsAcceptedKey = "TermsAccepted"
    var lastRefreshTriggerDate: Date?
    var isShowingVolumeViewController: Bool = false
    //let vTuner = VTunerProvider()
    
    let audioSystem: AudioSystem
    
    var openSpeaker: HomeSpeakerViewModel?
    var loadingSpeakerDisposable: Disposable?
    var wacService: WACServiceType?
    
    let blurTransitioningDelegate = BlurTransitioningDelegate()
    
    var homeViewModel: HomeViewModel?
    
    init(rootViewControllerController:UIViewController) {
        
        //NSUserDefaults.standardUserDefaults().setBool(false, forKey: self.termsAcceptedKey)
        
        provider = SpeakerProvider()
        
        self.rootViewController = rootViewControllerController
        
        self.discoveryService = (mock ? DiscoveryServiceMock(): DiscoveryService())
        
        
        self.audioSystem = AudioSystem(discoveryService: self.discoveryService, provider: self.provider)
        audioSystem.groupUpdatesActive = true
        
        self.menuViewModel = MenuViewModel(audioSystem: audioSystem)
        if let rootViewController = rootViewController as? RootViewController {
            
            let menuViewController = UIStoryboard.main.menuViewController
            menuViewController.viewModel = menuViewModel
            menuViewController.delegate = self
            rootViewController.menuViewController = menuViewController
        }
        
        let termsAcceptedKey = self.termsAcceptedKey
        let termsAccepted = NotificationCenter.default.rx.notification(UserDefaults.didChangeNotification).map{ _ in
            UserDefaults.standard.bool(forKey:termsAcceptedKey)
            }.startWith(UserDefaults.standard.bool(forKey:termsAcceptedKey)).distinctUntilChanged()
        
        let wifiReachable = NetworkInfoService.sharedInstance.wifiReachable
        
        let scanActive = Observable.combineLatest(wifiReachable, termsAccepted) { wifiReachable, termsAccepted in
            (wifiReachable && termsAccepted) }
        
        self.discoveryService.addDelegate(self)
        scanActive.distinctUntilChanged().subscribe(onNext: { [weak self] active in
            if active {
                self?.discoveryService.search()
            } else {
                self?.stopInitialScan()
                self?.discoveryService.stop()
            }
        })
        .disposed(by: disposeBag)
       
        let speakerCount = self.audioSystem.groups.asObservable().map{ $0.count }
        
        Observable.combineLatest(termsAccepted, wifiReachable, initialScan.asObservable().distinctUntilChanged(), speakerCount, UIApplication.appIsInForeground) { (termsAccepted, wifiReachable, initialScan, speakerCount, foreground) -> (termAccepted: Bool, wifiReachable: Bool, initialScan: Bool,foreground: Bool, speakerCount: Int) in
            
            return (termAccepted: termsAccepted, wifiReachable: wifiReachable, initialScan: initialScan,foreground: foreground, speakerCount: speakerCount)
            }.subscribe(onNext:{ [weak self] (termAccepted: Bool, wifiReachable: Bool, initialScan: Bool, foreground: Bool, speakerCount: Int) in
                self?.updateAppStatusFor(termsAccepted:termAccepted, wifiReachable: wifiReachable, initialScan: initialScan, foreground: foreground, speakerCount: speakerCount)
        }).disposed(by: disposeBag)
        
        self.appStatus.asObservable().distinctUntilChanged().subscribe(onNext:{ [weak self] appStatus in
            self?.updateForCurrentAppStatus()
        }).disposed(by: disposeBag)
        
        audioSystem.addDelegate(self)
        discoveryService.addDelegate(self)
    }
    
    func startInitialScan() {
        
        initialScan.value = true
        stopInitialScan()
        initialScanDisposable = Observable<Int>.interval(5.0, scheduler: MainScheduler.instance).take(1).subscribe(weak: self, onNext: AppCoordinator.initialScanDone)
        initialScanDisposable!.disposed(by: disposeBag)
        //NSLog("+ started initial scan")
        
    }
    
    func stopInitialScan() {
        
        initialScanDisposable?.dispose()
        initialScanDisposable = nil
        //NSLog("+ stopped initial scan")
    }
    
    func initialScanDone(done: Int) {
        
        //NSLog("+ finished initial scan")
        initialScan.value = false
    }
    
    func refreshDiscovery(clear: Bool) {
        
        Answers.logCustomEvent(withName: "ForceRefreshHomeScreen", customAttributes: nil)
        self.audioSystem.refresh(clear: clear)
        self.lastRefreshTriggerDate = Date()
    }
    
    func updateAppStatusFor(termsAccepted: Bool, wifiReachable: Bool, initialScan: Bool, foreground: Bool, speakerCount: Int) {
        
        
        if !termsAccepted {
            appStatus.value = AppStatus.termsNotAccepted
            return
        }
        //appStatus.value = AppStatus.speakersAvailable
        //return
        if !wifiReachable {
            
            appStatus.value = AppStatus.wifiNotAvailable
            
            return
        }
        
        if initialScan {
            
            if speakerCount < 1 {
                
                appStatus.value = AppStatus.loading
            } else {
                
                appStatus.value = AppStatus.speakersAvailable
            }
        } else {
            
            if speakerCount > 0 {
                
                appStatus.value = AppStatus.speakersAvailable
                return
            } else {
               
                appStatus.value = AppStatus.noSpeakersAvailable
                return
            }
        }
    }
    
    func updateForCurrentAppStatus() {
        
        
        DispatchQueue.main.async { [weak self] in
            
            if let status = self?.appStatus.value {
                switch status {
                case .none: break
                case .termsNotAccepted: self?.showWelcome()
                case .loading: self?.showLoading()
                case .noSpeakersAvailable: self?.showNoSpeakersAvailable()
                case .wifiNotAvailable: self?.showNoWifiAvailable()
                case .speakersAvailable: self?.showHome()
                }
            }
        }
        
    }
    
    func updateForCurrentGroups(_ groups: [SpeakerGroup]) {
        
        if let playerCoordinator = childCoordinators.find({ $0 is PlayerCoordinator }) as? PlayerCoordinator {
            
            let currentSpeaker = playerCoordinator.connectionManager.clientSpeaker
            let currentGroupsContainCurrentSpeaker = groups.find({$0.speakers.contains(currentSpeaker)}) != nil
            if !currentGroupsContainCurrentSpeaker {
                
                playerCoordinator.stop()
            }
        }
        
        if let settingsCoordinator = childCoordinators.find({ $0 is SettingsCoordinator }) as? SettingsCoordinator {
            
            let currentSpeaker = settingsCoordinator.currentSpeaker
            if let currentSpeaker = currentSpeaker {
                let currentGroupsContainCurrentSpeaker = groups.find({$0.speakers.contains(currentSpeaker)}) != nil
                if !currentGroupsContainCurrentSpeaker {
                    
                    _ = self.rootViewController.dismiss(animated: true, completion: {})
                }
            }
        }
    }
    
    func updateNowPlayingInfo(_ info: NowPlayingState, forSpeaker speaker: Speaker) {
        
        audioSystem.nowPlayingInfo.value[speaker] = info
    }
    
    func start() {
    
        updateForCurrentAppStatus()
    }
    
    func showHome() {
        
        if let rootVC:RootViewController = self.rootViewController as? RootViewController {
            
            if navController == nil {
                
                navController = UENavigationController()
                navController?.navigationBar.isHidden = true
                navController?.automaticallyAdjustsScrollViewInsets = false
            }
            
            if rootVC.displayedViewController != navController {
                rootVC.showChildViewController(navController, animated: false)
            }
            
            let topViewController = navController?.viewControllers.first
            if topViewController == nil || !(topViewController is HomeViewController){
                
                let homeViewController = UIStoryboard.main.homeViewController
                homeViewController.delegate = self;
                
                if homeViewModel == nil {
                    homeViewModel = HomeViewModel(audioSystem: audioSystem, speakerProvider: provider)
                }
                
                homeViewController.viewModel = homeViewModel;
                
                
                navController?.setViewControllers([homeViewController], animated: false)
            }
            
        }
    }
    
    func showWelcome() {
        
        if let rootVC:RootViewController = self.rootViewController as? RootViewController {
            
            let welcomeViewController = UIStoryboard.main.welcomeViewController
            welcomeViewController.delegate = self;
            if navController == nil {
                
                navController = UENavigationController()
                navController?.navigationBar.isHidden = true
            }
            rootVC.showChildViewController(navController, animated: false)
            navController?.setViewControllers([welcomeViewController], animated: false)
        }
    }
    
    func showTerms() {
        
        let termsViewController = UIStoryboard.main.termsViewController
        termsViewController.delegate = self;
        navController?.pushViewController(termsViewController, animated: true)
    }
    
    func showLoading() {
        
        if let rootVC:RootViewController = self.rootViewController as? RootViewController {
            
            let loadingViewController = UIStoryboard.main.loadingViewController
            rootVC.showChildViewController(loadingViewController, animated: false)
        }
    }
    
    func showNoSpeakersAvailable() {
        
        if let rootVC:RootViewController = self.rootViewController as? RootViewController {
            
            let noSpeakersViewController = UIStoryboard.main.noSpeakersViewController
            noSpeakersViewController.delegate = self
            rootVC.showChildViewController(noSpeakersViewController, animated: false)
        }
    }
    
    func showNoWifiAvailable() {
        
        if let rootVC:RootViewController = self.rootViewController as? RootViewController {
            
            let noWifiViewController = UIStoryboard.main.noWiFiViewController
            noWifiViewController.delegate = self
            rootVC.showChildViewController(noWifiViewController, animated: false)
            stopSettingsCoordinator()
            closeAnyOpenScreens {}
        }
    }
    
    func stopSettingsCoordinator() {
        
        for coordinator in self.childCoordinators {
            
            if let coordinator = coordinator as? SettingsCoordinator {
                coordinator.delegate?.settingsCoordinatorDidFinishCoordinating(coordinator)
            }
        }
    }
    
    func closeAnyOpenScreens(withCompletion completion:()->()) {
        if let playerCoordinator = childCoordinators.find({ $0 is PlayerCoordinator }) as? PlayerCoordinator {
            
            playerCoordinator.stop()
            removeChildCoordinator(playerCoordinator)
        }
        else
            if let settingsCoordinator = childCoordinators.find({ $0 is SettingsCoordinator }) as? SettingsCoordinator {
                
                _ = self.rootViewController.dismiss(animated: true, completion: {})
                removeChildCoordinator(settingsCoordinator)
                
            }
            else
                if let setupCoordinator = childCoordinators.find({ $0 is SetupCoordinator }) as? SetupCoordinator {
                    
                    _ = self.rootViewController.dismiss(animated: true, completion: {})
                    removeChildCoordinator(setupCoordinator)
                }
                else
                    if let helpCoordinator = childCoordinators.find({ $0 is HelpCoordinator }) as? HelpCoordinator {
                        
                        _ = self.rootViewController.dismiss(animated: true, completion: {})
                        removeChildCoordinator(helpCoordinator)
                    }
                    else if isShowingVolumeViewController {
                        
                        self.isShowingVolumeViewController = false
                        if let _ = self.rootViewController.presentedViewController as? VolumeViewController {
                            self.rootViewController.dismiss(animated: true, completion: {})
                        }
                    } else if let guestPlayerCoordinator = childCoordinators.find({ $0 is GuestPlayerCoordinator }) as? GuestPlayerCoordinator {
                        
                        guestPlayerCoordinator.stop()
                        removeChildCoordinator(guestPlayerCoordinator)
        }
        completion()
        
    }
    
    func showCastInfoAlert() {
        
        if let rootVC:RootViewController = self.rootViewController as? RootViewController {
            let castAlert = UIStoryboard.main.castInfoAlertViewController
            rootVC.definesPresentationContext = true
            
            
            castAlert.providesPresentationContextTransitionStyle = true
            castAlert.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            castAlert.transitioningDelegate = blurTransitioningDelegate
            castAlert.delegate = self
            
            
            rootVC.present(castAlert, animated: true, completion: nil)
        }
    }
    
    func showGroupFullInfoAlert() {
        
        if let rootVC:RootViewController = self.rootViewController as? RootViewController {
            let groupFullAlert = UIStoryboard.main.groupFullInfoAlertViewController
            rootVC.definesPresentationContext = true
            
            groupFullAlert.providesPresentationContextTransitionStyle = true
            groupFullAlert.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            groupFullAlert.transitioningDelegate = blurTransitioningDelegate
            groupFullAlert.delegate = self
            
            rootVC.present(groupFullAlert, animated: true, completion: nil)
        }
    }
    
    func showCastSwitchAlert(forSpeakerViewModel speakerViewModel:HomeSpeakerViewModel) {
        
        if let rootVC:RootViewController = self.rootViewController as? RootViewController {
            let castAlert = UIStoryboard.main.castSwitchAlertViewController
            rootVC.definesPresentationContext = true
            
            castAlert.providesPresentationContextTransitionStyle = true
            castAlert.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            castAlert.transitioningDelegate = blurTransitioningDelegate
            castAlert.delegate = self
            castAlert.speakerViewModel = speakerViewModel
            
            rootVC.present(castAlert, animated: true, completion: nil)
        }
    }
    
    func showUnlockedModuleAlert(forSpeakerViewModel speakerViewModel:HomeSpeakerViewModel, inGroupViewModel groupViewModel: HomeGroupViewModel) {
        
        if let rootVC:RootViewController = self.rootViewController as? RootViewController {
            let unlockedController = UIStoryboard.main.UnlockedAlertViewController
            rootVC.definesPresentationContext = true
            
            unlockedController.providesPresentationContextTransitionStyle = true
            unlockedController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            unlockedController.transitioningDelegate = blurTransitioningDelegate
            unlockedController.delegate = self
            unlockedController.speakerViewModel = speakerViewModel
            unlockedController.groupViewModel = groupViewModel
            
            rootVC.present(unlockedController, animated: true, completion: nil)
        }
        
        
    }
    
    func showSpeakerUpadateAvailable(forSpeakerViewModel speakerViewModel:HomeSpeakerViewModel, inGroupViewModel groupViewModel: HomeGroupViewModel) {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        let updateAvailableController = UIStoryboard.main.updateAvailableAlertViewController
        rootVC.definesPresentationContext = true
        
        updateAvailableController.providesPresentationContextTransitionStyle = true
        updateAvailableController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        updateAvailableController.delegate = self
        updateAvailableController.speakerViewModel = speakerViewModel
        updateAvailableController.groupViewModel = groupViewModel
        
        rootVC.present(updateAvailableController, animated: true, completion: nil)
    }
    
    
    func logSuccessfulConnectionWithDuration(_ duration: Double) {
        
        let eventNamesForDuration = [0.0: "Connected00Second",
                                     0.5: "Connected05Second",
                                     1.0: "Connected1Second",
                                     2.0: "Connected2Second",
                                     3.0: "Connected3Second",
                                     4.0: "Connected4Second",
                                     5.0: "Connected5Second",
                                     6.0: "Connected6Second",
                                     7.0: "Connected7Second",
                                     8.0: "Connected8Second",
                                     9.0: "Connected9Second",
                                     10.0: "Connected10Second"]
        var minDuration:Double = 0
        let allKeys = Array(eventNamesForDuration.keys).sorted()
        for i in 0..<(allKeys.count-1) {
            let first = allKeys[i]
            let second = allKeys[i+1]
            if duration < second && duration > first {
                minDuration = first
            }
            if duration > second {
                minDuration = second
            }
        }
        
        if let eventName = eventNamesForDuration[minDuration] {
            
            Answers.logCustomEvent(withName: "ConnectedFromHomeScreen",
                                   customAttributes:
                ["Status":eventName])
        }
    }

    
    func showSpeakerForHomeSpeakerViewModel(_ speakerViewModel: HomeSpeakerViewModel, inGroupViewModel groupViewModel:HomeGroupViewModel) {
        
        
        if let loadingDisposable = loadingSpeakerDisposable {
            
            if openSpeaker != nil && openSpeaker!.speaker.mac != speakerViewModel.speaker.mac {
                
                loadingDisposable.dispose()
                loadingSpeakerDisposable = nil
                openSpeaker?.loading.value = false
                openSpeaker = nil
            }
        }
        if loadingSpeakerDisposable == nil {
            
            if let masterSpeaker = groupViewModel.group.masterSpeaker {
                speakerViewModel.loading.value = true
                
                openSpeaker = speakerViewModel
                
                
                provider.setTOSAcceptedForGroup(groupViewModel.group).subscribe().disposed(by: disposeBag)
                
                
                let selectedSpeaker = speakerViewModel.speaker
                let getGroupState = Observable.combineLatest(provider.getClientStateForSpeaker(selectedSpeaker), provider.getMasterStateForSpeaker(masterSpeaker)) { clientState, masterState in
                    return (clientState: clientState, masterState: masterState)
                    }.timeout(10.0, scheduler: MainScheduler.instance)
                
                let start = CACurrentMediaTime()
                let getStateDisposable = getGroupState.subscribe(
                    onNext: { [weak self] state in
                        
                        let end = CACurrentMediaTime()
                        let duration = end-start
                        
                        self?.logSuccessfulConnectionWithDuration(duration)
                        
                        self?.startPlayerForSpeaker(selectedSpeaker, withClientState: state.clientState, masterSpeaker: masterSpeaker, masterState: state.masterState)
                    },
                    onError: { [weak self] error in
                        
                        Answers.logCustomEvent(withName: "ConnectedFromHomeScreen",
                                               customAttributes:
                            ["Status":"Failed"])
                        NSLog("get speaker state error \(error)")
                        speakerViewModel.loading.value = false
                        self?.openSpeaker = nil
                        self?.loadingSpeakerDisposable = nil
                    },
                    onCompleted: { [weak self] in
                        
                        speakerViewModel.loading.value = false
                        self?.loadingSpeakerDisposable = nil
                })
                
                loadingSpeakerDisposable = getStateDisposable
                loadingSpeakerDisposable?.disposed(by: disposeBag)
            }
            
        }
    }
    
    func startPlayerForSpeaker(_ speaker: Speaker, withClientState clientState: GroupClientState, masterSpeaker: Speaker,  masterState: GroupMasterState) {
        
        if let navController = self.navController {
                
            let clientNotifier = SpeakerNotifier(speaker: speaker, provider: provider)
            let speakerConnectionManager = SpeakerConnectionManager(clientSpeaker: speaker, clientNotifier: clientNotifier, clientState: clientState, masterSpeaker: masterSpeaker, masterState: masterState, audioSystem: audioSystem, speakerProvider: provider)
            
            let playerCoordinator : PlayerCoordinator
            if let connectable = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.connectable, connectable == true {
                playerCoordinator = PlayerCoordinator(connectionManager: speakerConnectionManager, navigationController: navController)
                playerCoordinator.delegate = self

                
            } else {
            
                playerCoordinator = GuestPlayerCoordinator(connectionManager: speakerConnectionManager, navigationController: navController)
                playerCoordinator.delegate = self
                
            }

            self.childCoordinators.append(playerCoordinator)
            
            playerCoordinator.start()
        }
    }
    
    func showVolumesFromViewCotroller(_ viewController: UIViewController) {

        
            provider.groupVolumesForGroups(self.audioSystem.groups.value, knownVolumes: audioSystem.volumeInfo.value).subscribe(onNext:{ [weak self] groupVolumes in
                
                guard let `self` = self else { return }
                let volume = UIStoryboard.main.volumeViewController
                viewController.definesPresentationContext = true
                
                if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone {
                    volume.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                } else {
                    volume.modalPresentationStyle = UIModalPresentationStyle.formSheet
                }
                volume.delegate = self
                volume.viewModel = VolumeViewModel(groupVolumes:groupVolumes,  audioSystem: self.audioSystem, speakerProvider: self.provider)
                volume.viewModel?.delegate = self
                self.isShowingVolumeViewController = true
                viewController.present(volume, animated: true, completion: nil)
                
            }).disposed(by: disposeBag)
    }
    
    func startSetup(_ speaker: Speaker?) {
        
        if wacService == nil {
            wacService = WACService()
        }
        let setupCoordinator = SetupCoordinator(displayViewController: rootViewController, moyaProvider: provider, discoveryService: discoveryService, wacService:  wacService!)
        setupCoordinator.delegate = self;
        self.childCoordinators.append(setupCoordinator)
        audioSystem.groupUpdatesActive = false
        
        setupCoordinator.start(speaker)
    }
    
    func startSettings() {
        
        let settingsCoordinator = SettingsCoordinator(viewController: rootViewController, audioSystem: audioSystem, provider: provider, discoveryService: discoveryService)
        settingsCoordinator.delegate = self
        self.childCoordinators.append(settingsCoordinator)
        settingsCoordinator.start()
    }
    
    func startSettings(forSpeaker speaker:Speaker) {
        let settingsCoordinator = SettingsCoordinator(viewController: rootViewController, audioSystem: audioSystem, provider: provider, discoveryService: discoveryService)
        settingsCoordinator.delegate = self
        self.childCoordinators.append(settingsCoordinator)
        settingsCoordinator.startForSpeaker(speaker)
    }
    
    func showAbout() {

        let aboutCoordinator = AboutCoordinator(parentViewController: rootViewController)
        aboutCoordinator.delegate = self
        self.childCoordinators.append(aboutCoordinator)
        aboutCoordinator.start()
    }
    
    func showHelp() {
        
        let helpCoordinator = HelpCoordinator(parentViewController: rootViewController)
        helpCoordinator.delegate = self
        self.childCoordinators.append(helpCoordinator)
        helpCoordinator.start()
        
    }
    
    func showBrowserForURL(_ url: URL) {
        
        if let rootVC:RootViewController = self.rootViewController as? RootViewController {
            if #available(iOS 9, *) {
                let safari = SFSafariViewController(url: url)
                safari.modalPresentationStyle = .overFullScreen
                safari.modalPresentationCapturesStatusBarAppearance = true
                rootVC.present(safari, animated: true, completion: nil)
            } else {
                let webViewController = STKWebKitModalViewController(url: url)
                rootVC.present(webViewController!, animated: true, completion: nil)
            }
        }
        
    }
    
    func toggleMenu() {
        
        if let rootVC:RootViewController = self.rootViewController as? RootViewController {
            if(rootVC.menuVisible) {
                rootVC.hideMenuViewController(withCompletion: {})
            } else {
                
                rootVC.showMenuViewController(withCompletion:  {
                    
                })
            }
        }
    }
    
    func removeChildCoordinator(_ element: Coordinator) {
        childCoordinators = childCoordinators.filter() { $0 !== element }
    }
}

extension AppCoordinator: DiscoveryServiceDelegate {
    
    
    func discoveryServiceDidStartSearching() {
        
        startInitialScan()
    }
    
    func discoveryServiceDidFinishConfirmingKnownSpeakers() {
        
        if let startTime = self.lastRefreshTriggerDate {
            let confirmDuration = abs(startTime.timeIntervalSince(Date()))
            if  confirmDuration > 2.0 {
                endRefreshing()
            } else {
                
                runAfterDelay(2.0 - confirmDuration, block: { [weak self] in
                    self?.endRefreshing()
                })
            }
            
        } else {
            
            endRefreshing()
        }
    }
    
    func endRefreshing() {
        
        if let homeViewController = navController?.viewControllers.first as? HomeViewController {
            
            if let tableView = homeViewController.tableView {
                tableView.endRefreshing()
            }
        }
    }
}

extension AppCoordinator: AudioSystemDelegate {
    
    func delegateIdentifier() -> String {
        
        return "app_coordinator"
    }
    
    func audioSystemDidMakeChanges() {
        
        updateForCurrentGroups(self.audioSystem.groups.value)
    }
}

extension AppCoordinator: NoSpeakersViewControllerDelegate {
    
    func noSpeakersViewControllerDidRequestRefresh(_ noSpeakersViewController: NoSpeakersViewController) {
        
        self.refreshDiscovery(clear: true)
    }
    func noSpeakersViewControllerDidRequestMenu(_ noSpeakersViewController: NoSpeakersViewController) {
        
        toggleMenu()
    }
    
    func noSpeakersViewControllerDidRequestSetup(_ noSpeakersViewController: NoSpeakersViewController) {
        
        startSetup(nil)
    }
}

extension AppCoordinator: NoWiFiViewControllerDelegate {
    
    func noWifiViewControllerDidRequestMenu(_ noWifiViewController: UIViewController) {
        
        toggleMenu()
    }
}

extension AppCoordinator: WelcomeViewControllerDelegate {
    
    func welcomeViewControllerDidAccept(_ welcomeViewController: WelcomeViewController) {
        
        UserDefaults.standard.set(true, forKey: termsAcceptedKey)
    }
    
    func welcomeViewControllerDidRequestTerms(_ welcomeViewController: WelcomeViewController) {
     
        showTerms()
    }
}

extension AppCoordinator: TermsViewControllerDelegate {
    
    func termsViewControllerDidRequestWebBrowserForURL(_ url: URL, termsViewController: TermsViewController) {
        
        showBrowserForURL(url)
    }
    
    func termsViewControllerDidAccept(_ termsViewController: TermsViewController) {
        
        UserDefaults.standard.set(true, forKey: termsAcceptedKey)
    }
    
    func termsViewControllerDidRequestBack(_ termsViewController: TermsViewController) {
        
        _ = navController?.popViewController(animated: true)
    }
}

extension AppCoordinator: HomeViewControllerDelegate {

    func homeViewController(_ homeViewController: HomeViewController, didRequestChangeMultiTo multi: Bool, inSpeakerViewModel speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel groupViewModel: HomeGroupViewModel) {
        
        var speakersInMulti = 0
        if let multiGroup = audioSystem.groups.value.find({$0.isMulti}) {
            speakersInMulti = multiGroup.speakers.count
        }
        
        if speakersInMulti >= 5 && multi == true {
            showGroupFullInfoAlert()
            speakerViewModel.multi.value = HomeSpeakerMultiState.solo
        } else if speakerViewModel.isPlayingCast.value == true && multi == true {
            showCastSwitchAlert(forSpeakerViewModel: speakerViewModel)
        } else {
            
            audioSystem.setMultiroomOn(multi, forSpeaker: speakerViewModel.speaker)
            //speakerViewModel.multi.value = multi ? .multi : .solo
        }
    }
    
    func homeViewControllerDidRequestFactoryReset(_ homeViewController: HomeViewController, didSelectSpeakerViewModel speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel) {
        
        if allowSpeakersToBeResetOrSetupForDebug {
            provider.setNode(ScalarNode.FactoryReset, value: "1", forSpeaker: speakerViewModel.speaker).subscribe().disposed(by: disposeBag)
            runAfterDelay(3.0, block: { [weak self] in
                self?.discoveryService.confirmDeviceAtIP(speakerViewModel.speaker.ipAddress, removeImmediatelyIfFail: true)
            })
        }
        
    }
    
    func homeViewControllerDidRequestSettings(_ homeViewController: HomeViewController, didSelectSpeakerViewModel: HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel) {
        startSettings(forSpeaker: didSelectSpeakerViewModel.speaker)
    }
    
    func homeViewControllerDidRequestCastInfo(_ homeViewController: HomeViewController, inSpeakerViewModel: HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel) {
        
        showCastInfoAlert()
    }
    
    func homeViewControllerDidRequestSetup(_ homeViewController: HomeViewController, didSelectSpeakerViewModel speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel) {
        if allowSpeakersToBeResetOrSetupForDebug {
            self.startSetup(speakerViewModel.speaker)
        }
    }
    
    func homeViewController(_ homeViewController: HomeViewController, didSelectSpeakerViewModel speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel groupViewModel: HomeGroupViewModel) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let audioSystem = appDelegate.appCoordinator.audioSystem
        let updateAvailable = audioSystem.doesSpeakerHasUpdateAvailable(speakerViewModel.speaker)
        
        if updateAvailable {
            checkAndShowWarningMessageAndShowSpeaker(forSpeakerViewModel: speakerViewModel, groupViewModel: groupViewModel, messageType: .updateMessage)
            return
        }
        
        if speakerViewModel.isUnlocked.value == true {
            
            checkAndShowWarningMessageAndShowSpeaker(forSpeakerViewModel: speakerViewModel, groupViewModel: groupViewModel, messageType: .unlockedMessage)
        } else {
            showSpeakerForHomeSpeakerViewModel(speakerViewModel, inGroupViewModel: groupViewModel)
        }
    }
    
    func checkAndShowWarningMessageAndShowSpeaker(forSpeakerViewModel speakerViewModel: HomeSpeakerViewModel, groupViewModel: HomeGroupViewModel, messageType : WarningMessageType) {
        
        let checkLastMessageShowDateFunction : (String) -> Date? = (messageType == WarningMessageType.unlockedMessage ? getLastCheckUpdateMessageDate : getLastCheckUpdateMessageDate)
        let storeCheckMessageDateFunction : (String, Date) -> Void = (messageType == WarningMessageType.unlockedMessage ? storeUnlockMessageLastShownDate : storeUpdateMessageLastShownDate)
        
        let showAlertFuncion : (HomeSpeakerViewModel,HomeGroupViewModel) -> Void = (messageType == WarningMessageType.unlockedMessage ? showUnlockedModuleAlert : showSpeakerUpadateAvailable)
        
        let lastAlertDisplayDate = checkLastMessageShowDateFunction(speakerViewModel.speaker.mac)
        
        if let lastCheckDate = lastAlertDisplayDate {
            
            let today = Date()
            let daysSince = today.since(lastCheckDate, in: .day)
            
            if daysSince >= 7 {
                
                // update the saved date
                storeCheckMessageDateFunction(speakerViewModel.speaker.mac, Date())
                showAlertFuncion(speakerViewModel, groupViewModel)
            } else {
                showSpeakerForHomeSpeakerViewModel(speakerViewModel, inGroupViewModel: groupViewModel)
            }
        } else {
            storeCheckMessageDateFunction(speakerViewModel.speaker.mac, Date())
            showAlertFuncion(speakerViewModel, groupViewModel)
        }
        
    }
    
    func storeUnlockMessageLastShownDate(forSpeakerMac speakerMac:String, dateChecked: Date)  {
        
        let storedSpeakerKey = unlockedMacPrefixForUserStore + speakerMac
        UserDefaults.standard.set(dateChecked, forKey: storedSpeakerKey)
    }
    
    func getLastCheckMessageDate(forSpreakerMac speakerMac:String) -> Date? {
        
        let storedSpeakerKey = unlockedMacPrefixForUserStore + speakerMac
        return UserDefaults.standard.object(forKey: storedSpeakerKey) as? Date
    }
    
    func storeUpdateMessageLastShownDate(forSpeakerMac speakerMac:String, dateChecked: Date)  {
        
        let storedSpeakerKey = updateMacPrefixForUserStore + speakerMac
        UserDefaults.standard.set(dateChecked, forKey: storedSpeakerKey)
    }
    
    func getLastCheckUpdateMessageDate(forSpreakerMac speakerMac:String) -> Date? {
        
        let storedSpeakerKey = updateMacPrefixForUserStore + speakerMac
        return UserDefaults.standard.object(forKey: storedSpeakerKey) as? Date
    }
    
    func homeViewControllerDidRequestVolumeControls(_ homeViewController: HomeViewController) {
        
        showVolumesFromViewCotroller(self.rootViewController)
    }
    
    func homeViewControllerDidRequestMenuToggle(_ homeViewController: HomeViewController) {
        
        toggleMenu()
    }
    
    func homeViewControllerDidRequestRefresh(_ homeViewController: HomeViewController) {
        
        self.refreshDiscovery(clear: false)
    }
}

extension AppCoordinator: VolumeViewControllerDelegate {
    
    func volumeViewControllerDidRequestClose(_ volumeViewController: VolumeViewController) {
        
        self.isShowingVolumeViewController = false
        if let vc = volumeViewController.presentingViewController {
            vc.dismiss(animated: true, completion: nil)
        }
    }
}

extension AppCoordinator: AboutCoordinatorDelegate {
    
    func aboutCoordinatorDidFinishCoordinating(_ coordinator: AboutCoordinator) {
        
        removeChildCoordinator(coordinator)
        rootViewController.dismiss(animated: true, completion: {})
    }
}

extension AppCoordinator: CastSwitchAlertViewControllerDelegate {
 
    func castSwitchAlertDidRequestContinue(_ alertViewController: CastSwitchAlertViewController) {
        
        if let vc = alertViewController.presentingViewController {
            let speakerViewModel = alertViewController.speakerViewModel
            vc.dismiss(animated: true, completion: { [weak self] in
                if let speaker = speakerViewModel?.speaker {
                    DispatchQueue.main.async {
                        self?.audioSystem.setMultiroomOn(true, forSpeaker: speaker)
                    }
                }
            })
        }
    }
    
    func castSwitchAlertDidRequestCancel(_ alertViewController: CastSwitchAlertViewController) {
        
        if let vc = alertViewController.presentingViewController {
            let speakerViewModel = alertViewController.speakerViewModel
            vc.dismiss(animated: true, completion: {
                speakerViewModel?.multi.value = .solo
            })
        }
    }
}

extension AppCoordinator: CastInfoAlertViewControllerDelegate {
    
    func castInfoAlertDidRequestContinue(_ alertViewController: CastInfoAlertViewController) {
        
        if let vc = alertViewController.presentingViewController {
            vc.dismiss(animated: true, completion: nil)
        }
    }
    
    func castInfoAlertDidRequestLearnMore(_ alertViewController: CastInfoAlertViewController) {
        
        if let vc = alertViewController.presentingViewController {
            vc.dismiss(animated: true, completion: nil)
        }
    }
    
    func castInfoAlertDidRequestClose(_ alertViewController: CastInfoAlertViewController) {
        
        if let vc = alertViewController.presentingViewController {
            vc.dismiss(animated: true, completion: nil)
        }
    }
}

extension AppCoordinator: GroupFullInfoAlertViewControllerDelegate {
    
    func groupFullInfoAlertDidRequestDismiss(_ alertViewController: GroupFullInfoAlertViewController) {
        
        if let vc = alertViewController.presentingViewController {
            vc.dismiss(animated: true, completion: nil)
        }
        
    }
    
}

extension AppCoordinator: MenuViewControllerDelegate {
    
    func didSelectMenuItem(_ menuItem: MenuItem, menuViewController: MenuViewController) {
        
        guard let rootVC:RootViewController = self.rootViewController as? RootViewController
            else {
                return;
        }
        
        if menuItem.menuItemType == MenuItemType.AddSpeaker {
            
            rootVC.hideMenuViewController(withCompletion:{
                
                self.startSetup(nil)
            })
        } else if menuItem.menuItemType == MenuItemType.Settings {
            
            rootVC.hideMenuViewController(withCompletion:{
                
                self.startSettings()
            })
        }  else if menuItem.menuItemType == MenuItemType.Help {
            
            rootVC.hideMenuViewController(withCompletion:{
                
                self.showHelp()
            })
        } else if menuItem.menuItemType == MenuItemType.Shop {
            
            rootVC.hideMenuViewController(withCompletion:{
                
                self.showBrowserForURL(URL(string: "https://www.urbanears.com/")!)
            })
        }
        else if menuItem.menuItemType == MenuItemType.About {
            
            rootVC.hideMenuViewController(withCompletion:{
                
                self.showAbout()
            })
        }
    }
    
    func didCloseMenu(menuViewController: MenuViewController) {
        self.toggleMenu()
    }
}

extension AppCoordinator: VolumeViewModelDelegate {
    
    //here we forward the state of the speaker volumes that change in the volume view for which the speaker does not notify
    func volumeViewModelDidSetVolumeForGroup(_ group: SpeakerGroup, volume: GroupVolume) {
        
        
        if let playerCoordinator = childCoordinators.find({ $0 is PlayerCoordinator }) as? PlayerCoordinator {
            
            if let volumeChangedGroupMaster = group.masterSpeaker, volumeChangedGroupMaster == playerCoordinator.connectionManager.masterSpeaker {
             
                playerCoordinator.connectionManager.masterState.value?.masterVolume = volume.masterVolume
            }
            
            if let speakerVolume = volume.clientsVolumes?[playerCoordinator.connectionManager.clientSpeaker] {
                
                playerCoordinator.connectionManager.clientState.value?.volume = speakerVolume.volume
            }
            
        }
    }
}

extension AppCoordinator: PlayerCoordinatorDelegate {
    
    func playerCoordinatorDidRequestVolumeFromViewController(_ viewController: UIViewController) {
        
        showVolumesFromViewCotroller(viewController)
    }
    
    func playerCoordinatorDidFinishCoordinating(_ coordinator: PlayerCoordinator) {
        openSpeaker = nil
        
        if let masterState = coordinator.connectionManager.masterState.value,
            let clientState = coordinator.connectionManager.clientState.value {
            
            let nowPlayingState = NowPlayingState(modeIndex: clientState.modeIndex,
                                                  presetIndex: clientState.presetIndex,
                                                  playStatus: masterState.playStatus,
                                                  modes: clientState.modes)
            updateNowPlayingInfo(nowPlayingState, forSpeaker: coordinator.connectionManager.clientSpeaker)
        }
        
        removeChildCoordinator(coordinator)
        
        if coordinator is GuestPlayerCoordinator {
            self.navController?.dismiss(animated: true, completion: nil)
            (self.rootViewController as? RootViewController)?.forcedHiddenStatusBar = false
        } else {
            _ = self.navController?.popToRootViewController(animated: true)
            (self.rootViewController as? RootViewController)?.forcedHiddenStatusBar = false
        }
        
    }
    
    func playerCoordinatorDidStartControllingVolume(_ coordinator: PlayerCoordinator) {
        
        (self.rootViewController as? RootViewController)?.forcedHiddenStatusBar = true
    }
    
    func playerCoordinatorDidFinishControllingVolume(_ coordinator: PlayerCoordinator) {
        
        (self.rootViewController as? RootViewController)?.forcedHiddenStatusBar = false
    }
}

extension AppCoordinator: SettingsCoordinatorDelegate {
    
    func settingsCoordinatorDidFinishCoordinating(_ coordinator: SettingsCoordinator) {
     
        removeChildCoordinator(coordinator)
        _ = self.rootViewController.dismiss(animated: true, completion: {})
    }
}

extension AppCoordinator: SetupCoordinatorDelegate {
 
    func setupCoordinatorDidFinishSetup(_ coordinator: SetupCoordinator) {
        removeChildCoordinator(coordinator)
        rootViewController.dismiss(animated: true, completion: {})
        audioSystem.groupUpdatesActive = true
    }
    
    func setupCoordinatorDidCancelSetup(_ coordinator: SetupCoordinator) {
        removeChildCoordinator(coordinator)
        rootViewController.dismiss(animated: true, completion: {})
        audioSystem.groupUpdatesActive = true
    }
}

extension AppCoordinator: HelpCoordinatorDelegate {
    
    func helpCoordinatorDidFinishCoordinating(_ coordinator: HelpCoordinator) {
        removeChildCoordinator(coordinator)
        rootViewController.dismiss(animated: true, completion: {})
    }
}

extension AppCoordinator : UnlockedViewControllerDelegate {
    
    func didAcknowledgeUnlockedModuleWarning(_ alertViewController: UnlockedAlertViewController) {
        if let vc = alertViewController.presentingViewController {
            let speakerViewModel = alertViewController.speakerViewModel
            let groupViewModel = alertViewController.groupViewModel
            vc.dismiss(animated: true, completion: { [weak self] in
                    DispatchQueue.main.async {
                        // continue connecting to speaker
                        self?.showSpeakerForHomeSpeakerViewModel(speakerViewModel!, inGroupViewModel: groupViewModel!)
                    }
            })
        }
    }
    
}

extension AppCoordinator: UpdateAvailableAlertViewControllerDelegate {
    func didAcknowledgeUpdateAvailabilityWarning(_ alertViewController : UpdateAvailableAlertViewController) {
        
        guard let vc = alertViewController.presentingViewController else { return }
        let speakerViewModel = alertViewController.speakerViewModel
        let groupViewModel = alertViewController.groupViewModel
        vc.dismiss(animated: true, completion: { [weak self] in
            DispatchQueue.main.async {
                // continue connecting to speaker
                self?.showSpeakerForHomeSpeakerViewModel(speakerViewModel!, inGroupViewModel: groupViewModel!)
            }
        })
    }
}
