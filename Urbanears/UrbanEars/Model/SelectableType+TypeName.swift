//
//  SelectableType+ModeName.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import MinuetSDK

extension SelectableType {
    
    public var typeName: String {
        
        switch self {
            
        case .preset: return Localizations.Player.Carousel.PlayableSource.Preset
        case .aux,.rca: return Localizations.Player.Carousel.PlayableSource.Aux
        case .bluetooth: return Localizations.Player.Carousel.PlayableSource.Bluetooth
        case .internetRadio: return Localizations.Player.Cloud.InternetRadioModeName
        case .cloud: return Localizations.Player.Carousel.PlayableSource.Cloud
        }
    }
}
