//
//  ExternalConfig.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper
import MinuetSDK

public class ExternalConfig {
    
    public static let sharedInstance  = ExternalConfig()
    public var speakerImages: SpeakerImages?
    public var presets: SpeakerInitialPresets?
    public init() {
        
        if let localSpeakerImagesJSONLocation = Bundle.main.url(forResource: "speaker_images", withExtension: "json") {
         
            if let speakerImagesJSONString = try? String(contentsOf: localSpeakerImagesJSONLocation) {
                
                speakerImages = Mapper<SpeakerImages>().map(JSONString:speakerImagesJSONString)
            }
        }
        
        if let localPresetsJSONLocation = Bundle.main.url(forResource: "radio_presets", withExtension: "json") {
            
            if let localPresetsJSONString = try? String(contentsOf: localPresetsJSONLocation) {
                
                presets = Mapper<SpeakerInitialPresets>().map(JSONString:localPresetsJSONString)
            }
        }

    }
    
    public func speakerImageForColor(_ color: String?) -> SpeakerImage? {
        
        if let `color` = color {
            return ExternalConfig.sharedInstance.speakerImages?.speakerImages?.filter({ speakerImage in color == speakerImage.color }).first
        } else {
            return nil
        }
    }
    
    
}
