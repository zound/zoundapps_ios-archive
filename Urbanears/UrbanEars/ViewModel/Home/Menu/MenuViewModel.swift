//
//  MenuViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

enum MenuItemType:String {
    
    case MySpeakers,AddSpeaker, Shop, Help, About, Settings
}

struct MenuItem {
    
    let title:String
    let icon:String
    let menuItemType:MenuItemType
}

class MenuViewModel {
    
    fileprivate var _defaultItems:[MenuItem] = [];
    
    var items : Variable<[MenuItem]>
    let audioSystem: AudioSystem
    
    let disposeBag: DisposeBag = DisposeBag()
    
    init(audioSystem: AudioSystem) {
        
        self.audioSystem = audioSystem
        
        items = Variable([]);
        
        self.audioSystem.groups.asObservable().subscribe(weak: self, onNext: MenuViewModel.updateForCurrentGroups).disposed(by: disposeBag)
    }
    
    func updateForCurrentGroups(_ groups: [SpeakerGroup]) {
        if groups.count == 0 {
            _defaultItems = [
                MenuItem(title:Localizations.Menu.AddSpeaker,   icon:"menu_add_speaker",menuItemType: MenuItemType.AddSpeaker),
                MenuItem(title:Localizations.Menu.Help,   icon:"menu_help",menuItemType: MenuItemType.Help),
                MenuItem(title:Localizations.Menu.ShopUrbanEars,   icon:"menu_shop",menuItemType: MenuItemType.Shop),
                MenuItem(title:Localizations.Menu.About,   icon:"menu_about",menuItemType: MenuItemType.About),
            ]
            items.value = _defaultItems
        } else {
            _defaultItems = [
                MenuItem(title:Localizations.Menu.AddSpeaker,   icon:"menu_add_speaker",menuItemType: MenuItemType.AddSpeaker),
                MenuItem(title:Localizations.Menu.SpeakerSettings,   icon:"menu_speakers",menuItemType: MenuItemType.Settings),
                MenuItem(title:Localizations.Menu.Help,   icon:"menu_help",menuItemType: MenuItemType.Help),
                MenuItem(title:Localizations.Menu.ShopUrbanEars,   icon:"menu_shop",menuItemType: MenuItemType.Shop),
                MenuItem(title:Localizations.Menu.About,   icon:"menu_about",menuItemType: MenuItemType.About),
            ]
            
            items.value = _defaultItems
        }
    }

}
