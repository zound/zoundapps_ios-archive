//
//  SetupUpdateViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 01/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import Interpolate
import MinuetSDK
import Zound

enum SetupUpdateState {
    
    case idle
    case checkingForUpdate
    case downloadingUpdate
    case installingUpdate
    case restartingSpeaker
    case finishedUpdate
    case failedUpdate
    case noUpdateAvailable
}

class SetupUpdateViewModel: RunAfterDelay {
    
    let speaker: Speaker
    let speakerProvider: SpeakerProvider
    let disposeBag: DisposeBag = DisposeBag()
    
    let updateStateVariable = Variable(SetupUpdateState.idle)
    let downloadProgress = Variable(0)
    let discoveryService: DiscoveryServiceType
    var progressDisposable: Disposable?
    
    var installTimeoutDisposable: Disposable?
    
    var updateState: SetupUpdateState {
        
        get {
            return updateStateVariable.value
        }
        set {
            updateStateVariable.value = newValue
        }
    }
    
    var progressChange: Interpolate! = nil
    var lastProgress: Int = 0
    
    init(speaker: Speaker, speakerProvider: SpeakerProvider, discoveryService: DiscoveryServiceType) {
        
        self.speaker = speaker
        self.speakerProvider = speakerProvider
        self.discoveryService = discoveryService
    }
    
    deinit {
        self.discoveryService.removeDelegate(self)
    }
    
    fileprivate func startUpdatingProgress() {
        
        let pollingInterval = 1.0
        let maxProgress:CGFloat = 100.0
        self.progressChange = Interpolate(from: 0,
                                          to: Int(maxProgress),
                                          function: BasicInterpolation.linear,
                                          apply: {[weak self] value in
            self?.downloadProgress.value = value
        })
        
        let progress = speakerProvider.pollNode(ScalarNode.UpdateProgress, atInterval: pollingInterval, onSpeaker: speaker).map { intFromValue($0) }
        progressDisposable = progress.subscribe(onNext: { [weak self] progress in
            
            guard let `self` = self else {return}
            let progressDiff = progress - self.lastProgress
            self.lastProgress = progress
            if progressDiff > 0 {
                
                let normalizedProgress = CGFloat(progress)/maxProgress
                let totalDownloadDuration = 100.0/(CGFloat(pollingInterval)*CGFloat(progressDiff))
                
                self.progressChange.animate(normalizedProgress, duration: totalDownloadDuration, completion: nil)
            }
        })
        progressDisposable?.disposed(by: disposeBag)
    }
    
    fileprivate func stopUpdatingProgress() {
        
        self.progressChange.invalidate()
        self.progressChange = nil
        progressDisposable?.dispose()
        progressDisposable = nil
    }
    
    fileprivate func isuState() -> Observable<UpdateState> {
        
        return speakerProvider.getNode(ScalarNode.UpdateState, forSpeaker: speaker).map{ state in
           return updateStateFromValue(state)
        }.timeout(5.0, scheduler: MainScheduler.instance).catchErrorJustReturn(UpdateState.checkFailed)
    }
    
    fileprivate func updateAvailable() -> Observable<Bool> {
        
        let isuStateNotifications = speakerProvider.pollNode(ScalarNode.UpdateState, atInterval: 1.0, onSpeaker: speaker).map{ node -> UpdateState? in
             updateStateFromValue(node)
        }.filter{ $0 != nil }.map{ $0! }.distinctUntilChanged()
        
        let waitForCheckFinish = isuStateNotifications.filter{ $0 != UpdateState.checkInProgress }.take(1).timeout(20.0, scheduler: MainScheduler.instance)
        let checkForUpdate = speakerProvider.setNode(ScalarNode.UpdateControl, value: String(UpdateControl.checkForUpdate.rawValue), forSpeaker: speaker).timeout(3.0, scheduler: MainScheduler.instance).catchErrorJustReturn(false)
        let checkForUpdateAndWait = checkForUpdate.flatMapLatest{ checking -> Observable<UpdateState> in
            NSLog("waiting for check to finish")
            if checking {
                return waitForCheckFinish.catchErrorJustReturn(UpdateState.checkFailed)
            } else {
                
                return Observable.just(UpdateState.checkFailed)
            }
        }
        
        let currentState = isuState()
        
        return currentState.flatMapLatest{ initialState -> Observable<UpdateState> in
            switch initialState {
            case .idle,.checkFailed, .updateNotAvailable: return checkForUpdateAndWait
            case .updateAvailable: return Observable.just(initialState)
            case .checkInProgress: return waitForCheckFinish
            }
            }.map { state in
                if state == UpdateState.updateAvailable {
                    return true
                } else {
                    return false
                }
        }
        
    }
    
    
    func checkForUpdates() {
     
        NSLog("checking for updates")
        updateState = .checkingForUpdate
        updateAvailable().subscribe(onNext: { [weak self] updateAvailable in
            
            guard let `self` = self else { return }
            if updateAvailable {
                
                self.downloadAvailableUpdate()
            } else {
                
                self.updateState = .noUpdateAvailable
            }
        }).disposed(by: disposeBag)
        
    }
    
    func downloadAvailableUpdate() {
        
        self.updateState = .downloadingUpdate
        startUpdatingProgress()
        
        let waitForProgressToStart = self.downloadProgress.asObservable()
            .distinctUntilChanged()
            .timeout(120.0, scheduler: MainScheduler.instance)
            .filter{ $0 != 0 }
            .take(1)
            .map { _ in return true }
            .catchErrorJustReturn(false)
        
        let waitForProgressToFinish = self.downloadProgress.asObservable()
            .distinctUntilChanged()
            .timeout(120.0, scheduler: MainScheduler.instance)
            .filter{ $0 == 100 }
            .take(1)
            .map { _ in return true }
            .catchErrorJustReturn(false)
        
        speakerProvider.setNode(ScalarNode.UpdateControl, value: String(UpdateControl.performUpdate.rawValue), forSpeaker: speaker).flatMapLatest{ done -> Observable<Bool> in
                NSLog("waiting for progress to start")
                return waitForProgressToStart
            }.flatMapLatest{ done -> Observable<Bool> in
                NSLog("waiting for progress to finish")
                return waitForProgressToFinish
            }.subscribe(onNext: { [weak self] installed in
            
            guard let `self` = self else { return }
            
            self.stopUpdatingProgress()
            if installed {
                NSLog("finished downloading")
                self.installAvailableUpdate()
                self.discoveryService.addDelegate(self)
            } else {
                NSLog("failed downloading")
                self.progressTimeoutDidTrigger()
            }
        }).disposed(by: disposeBag)
    }
    
    func installAvailableUpdate() {
        
        if self.discoveryService.speakers.find({$0.mac == speaker.mac}) == nil {
            
            NSLog("speaker already lost")
            self.updateState = .restartingSpeaker
        }
        else {
        
            NSLog("waiting for speaker to be removed from network")
            self.updateState = .installingUpdate
            startInstallTimeout()
            
            runAfterDelay(20.0, block: { [weak self] in
                guard let `self` = self else { return }
                self.discoveryService.confirmDeviceAtIP(self.speaker.ipAddress, removeImmediatelyIfFail: true)
            })
            
        }
        //wait for the speaker to dissapear from discovery
    }
    
    func startInstallTimeout() {
        
        installTimeoutDisposable?.dispose()
        installTimeoutDisposable = Observable<Int>.interval(90.0, scheduler: MainScheduler.instance).take(1).subscribe(weak: self, onNext: SetupUpdateViewModel.installTimeoutDidTrigger)
    }
    
    func installTimeoutDidTrigger(_ x: Int) {
        
        self.discoveryService.removeDelegate(self)
        self.updateState = .finishedUpdate
        stopInstallTimeout()
    }
    
    func progressTimeoutDidTrigger() {
        
        self.discoveryService.removeDelegate(self)
        self.updateState = .failedUpdate
    }
    
    func  stopInstallTimeout() {
     
        installTimeoutDisposable?.dispose()
        installTimeoutDisposable = nil
    }
}

extension SetupUpdateViewModel: DiscoveryServiceDelegate {
    
    func discoveryServiceDidUpdateInfoForSpeaker(_ speaker: Speaker, info: [ScalarNode:String]) {
        
        
    }

    func discoveryServiceDidFindSpeaker(_ discoveredSpeaker: Speaker, info: [ScalarNode : String]) {
        
        NSLog("+++ did find speaker \(speaker.mac) comparing with current speaker \(self.speaker.mac)")
        if speaker.mac == self.speaker.mac && updateState == .restartingSpeaker || updateState == .installingUpdate {
            
            NSLog("found speaker again")
            self.updateState = .finishedUpdate
            self.discoveryService.removeDelegate(self)
            stopInstallTimeout()
        }
    }
    
    func discoveryServiceDidLoseSpeaker(_ speaker: Speaker) {
        
        NSLog("+++ did lose speaker \(speaker.mac) comparing with current speaker \(self.speaker.mac)")
        if speaker.mac == self.speaker.mac && self.updateState == .installingUpdate {
            
            NSLog("restarting speaker")
            self.updateState = .restartingSpeaker
        }
    }
    
    func delegateIdentifier() -> String {
        
        return "setup_update_view_model"
    }
}

func intFromValue(_ value: String?) -> Int {
    
    if let intString = value {
        
        if let intValue = Int(intString) {
            
            return intValue
        }
    }
    
    return 0
}

func updateStateFromValue(_ value: String?) -> UpdateState {
        
        if let stateString = value {
            
            if let stateIntValue = Int(stateString) {
                
                if let stateEnum = UpdateState(rawValue: stateIntValue) {
                    
                    return stateEnum
                }
            }
        }
        
        return UpdateState.checkFailed
}

