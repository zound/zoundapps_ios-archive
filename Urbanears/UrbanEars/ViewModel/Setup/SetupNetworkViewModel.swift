//
//  SetupNetworkViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 01/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import Crashlytics
import MinuetSDK

protocol SetupNetworkViewModelDelegate: class {
    
    func setupNetworkViewModelDidChangeConfigurationState(_ setupViewModel: SetupNetworkViewModel, state: SetupConfigurationState)
    func setupNetworkViewModelDidChangeDiscoveryState(_ setupViewModel: SetupNetworkViewModel, state: SpeakerDiscoveryState)
    
    func setupViewModelDidFinishSpeakerConfiguration(_ setupViewModel: SetupNetworkViewModel, speaker: Speaker)
    func setupViewModelDidCancelSpeakerConfiguration(_ setupViewModel: SetupNetworkViewModel)
}

enum SetupNetworkError: Error {
    
    case didNotReconnectToSameWiFi
    case wifiDisconnected
    case wacCrash
    case cannotConnectToSpeaker
    case other
    
    var displayName: String {
 
        switch self {
        case .didNotReconnectToSameWiFi: return "DifferentWiFiError"
        case .cannotConnectToSpeaker: return "CannotConnectToSpeakerError"
        case .other: return "UnknownError"
        case .wacCrash: return "WACCrashError"
        case .wifiDisconnected: return "WiFiDisconnectedError"
        }
    }
}

enum SetupConfigurationState {
    
    case idle
    case loading
    case foundSpeakers
    case noSpeakersFound
    case configuring(speaker: UnconfiguredSpeaker)
    case configured(speaker: UnconfiguredSpeaker, configureError:SetupNetworkError?)
}

enum SpeakerDiscoveryState {
    
    case idle
    case discovering(count: Int)
    case discovered(speaker: Speaker)
    case discoveryFailed(count: Int)
}

class SetupNetworkViewModel: NSObject {
    
    weak var delegate: SetupNetworkViewModelDelegate?
    
    fileprivate let unconfiguredSpeakers : Variable<[UnconfiguredSpeaker]>
    let visibleUnconfiguredSpeakers : Variable<[UnconfiguredSpeaker]>
    fileprivate let configuredSpeakers : Variable<[Speaker]>
    let speakeProvider: SpeakerProvider
    let wacService: WACServiceType
    let discoveryService: DiscoveryServiceType
    let disposeBag = DisposeBag()
    
    var didDisconnectNetworkDuringWAC = false
    var didConnectToSpeakerSSIDDuringWAC = false
    var networkConnectedWhenStartedConfiguration: String? = nil
    var networkConnectedDuringConfiguration: String? = nil
    
    let setupNetworkStateVariable: Variable<SetupConfigurationState>  = Variable(.idle)
    var setupNetworkState: SetupConfigurationState {
        
        get{ return setupNetworkStateVariable.value }
        set{ setupNetworkStateVariable.value = newValue }
    }
    
    let speakerDiscoveryStateVariable: Variable<SpeakerDiscoveryState>  = Variable(.idle)
    var speakerDiscoveryState: SpeakerDiscoveryState {
        
        get{ return speakerDiscoveryStateVariable.value }
        set{ speakerDiscoveryStateVariable.value = newValue }
    }
    

    var noSpeakersTimeoutDisposable: Disposable? = nil
    var noDiscoveryTimeoutDisposable: Disposable? = nil
    
    
    init(speakerProvider: SpeakerProvider, wacService: WACServiceType, discoveryService: DiscoveryServiceType) {
        
        self.speakeProvider = speakerProvider
        self.wacService = wacService
        self.discoveryService = discoveryService
    
        unconfiguredSpeakers = Variable([]);
        configuredSpeakers = Variable([]);
        visibleUnconfiguredSpeakers = Variable([]);
        
        
        super.init()
        
        self.discoveryService.addDelegate(self)
        self.wacService.delegate = self
        NetworkInfoService.sharedInstance.addDelegate(self)
        
        Observable.combineLatest(self.unconfiguredSpeakers.asObservable(), self.configuredSpeakers.asObservable()) { unconfigured, configured in
            return unconfigured.filter({ unconfiguredSpeaker in
                let configuredmacs = configured.map{ $0.mac }
                return !configuredmacs.contains(unconfiguredSpeaker.mac) && !configuredmacs.contains(unconfiguredSpeaker.veniceMac)
            })
        }.bind(to: self.visibleUnconfiguredSpeakers).disposed(by: disposeBag)
        
        self.visibleUnconfiguredSpeakers.asObservable()
            .map{$0.count}
            .subscribe(weak: self,
                       onNext: SetupNetworkViewModel.updateForUnconfiguredSpeakerCount)
            .disposed(by: disposeBag)
        
        self.setupNetworkStateVariable.asObservable().subscribe(onNext:{ [weak self] newState in
            guard let `self` = self else  { return }
            NSLog("did change network state to \(newState)")
            self.delegate?.setupNetworkViewModelDidChangeConfigurationState(self, state: newState)
        }).disposed(by: disposeBag)
        
        self.speakerDiscoveryStateVariable.asObservable().subscribe(onNext:{ [weak self] newState in
            guard let `self` = self else  { return }
            NSLog("did change discovery state to \(newState)")
            self.delegate?.setupNetworkViewModelDidChangeDiscoveryState(self, state: newState)
        }).disposed(by: disposeBag)
    }
    
    deinit {
        
        discoveryService.removeDelegate(self)
        NetworkInfoService.sharedInstance.removeDelegate(self)
        wacService.active = false
    }
    
    func searchForSpeakers() {
        
        setupNetworkState = .loading
        unconfiguredSpeakers.value.removeAll()
        let sortedUnconfiguredSpeakers = wacService.unconfiguredSpeakers.sorted(by: {sp1, sp2 in sp1.completeName < sp2.completeName})
        self.unconfiguredSpeakers.value.append(contentsOf: sortedUnconfiguredSpeakers)
        
        startNoSpeakersFoundTimeout()
        if wacService.active {
            
            wacService.active = false
            runAfterDelay(1.0, block: { [weak self] in
            
                self?.wacService.active = true
            })
        } else {
            
            self.wacService.active = true
        }
    }
    
    func startNoSpeakersFoundTimeout() {
        
        noSpeakersTimeoutDisposable?.dispose()
        noSpeakersTimeoutDisposable = Observable<Int>.interval(5.0, scheduler: MainScheduler.instance).subscribe(weak: self, onNext: SetupNetworkViewModel.searchDidTimeout)
        noSpeakersTimeoutDisposable?.disposed(by: disposeBag)
    }
    
    func stopNoSpeakersFoundTimeout() {
        
        noSpeakersTimeoutDisposable?.dispose()
        noSpeakersTimeoutDisposable = nil
    }
    
    func searchDidTimeout(_ one: Int) {
        
        stopNoSpeakersFoundTimeout()
        if case .loading = setupNetworkState {
            setupNetworkState = .noSpeakersFound
        }
    }
    
    func updateForUnconfiguredSpeakerCount(_ count: Int) {
        
        switch setupNetworkState {
        case .loading, .noSpeakersFound:
            if count > 0 {
                setupNetworkState = .foundSpeakers
                stopNoSpeakersFoundTimeout()
            }
            
        case .foundSpeakers:
            if count == 0 {
                searchForSpeakers()
            }
        
        default: break
        }
    }
    
    func configureSpeaker(_ speaker:UnconfiguredSpeaker, viewController:UIViewController) {
        
        self.didConnectToSpeakerSSIDDuringWAC = false
        self.didDisconnectNetworkDuringWAC = false
        self.networkConnectedWhenStartedConfiguration = NetworkInfoService.sharedInstance.networkInfo.wifiName
        self.networkConnectedDuringConfiguration = nil
        
        setupNetworkState = .configuring(speaker: speaker)
        wacService.active = false
        self.discoveryService.stop()
        
        runAfterDelay(1.0, block: { [weak wacService] in
            wacService?.configureSpeaker(speaker, onViewController: viewController)
        })
    }
    
}

extension SetupNetworkViewModel: NetworkInfoServiceDelegate {
    
    func didConnectToNetworkWithNetworkInfo(_ networkInfo: NetworkInfo) {
     
        
        NSLog("did connect to \(networkInfo.wifiName!)")
        if case SetupConfigurationState.configuring(let unconfiguredSpeaker) = setupNetworkState {
            if let wifiName = networkInfo.wifiName {
                if !self.didConnectToSpeakerSSIDDuringWAC {
                    if wifiName == unconfiguredSpeaker.ssid {
                        self.didConnectToSpeakerSSIDDuringWAC = true
                    }
                }
                if didConnectToSpeakerSSIDDuringWAC && (networkConnectedDuringConfiguration == nil) && wifiName != unconfiguredSpeaker.ssid {
                    self.networkConnectedDuringConfiguration = wifiName
                }
            }
        }
        
        
    }
    
    func didDisconnectFromNetwork() {
        
        NSLog("did disconnect")
        if case SetupConfigurationState.configuring(_) = setupNetworkState {
            self.didDisconnectNetworkDuringWAC = true
        }
    }
}

extension SetupNetworkViewModel: WACServiceDelegate {
    
    func didCancelConfigurationForAccessory(_ speaker: UnconfiguredSpeaker) {
        
        //we keep this commented as it's easier to interpret the results without this event which is not relevant (basically means the user chose the wrong speaker)
        //Answers.logCustomEvent(withName: "WACFinished", customAttributes: ["result": "Cancelled"])
        self.searchForSpeakers()
        self.delegate?.setupViewModelDidCancelSpeakerConfiguration(self)
    }
    
    func didFailConfigurationForAccessory(_ speaker: UnconfiguredSpeaker) {
        

        var error = SetupNetworkError.other
        let currentWiFi = NetworkInfoService.sharedInstance.networkInfo.wifiName
        if !didConnectToSpeakerSSIDDuringWAC {
            if !didDisconnectNetworkDuringWAC {
                error = SetupNetworkError.wacCrash
            } else {
                error = SetupNetworkError.cannotConnectToSpeaker
            }
        } else if currentWiFi == nil {
            error = SetupNetworkError.wifiDisconnected
        } else if let wifi = currentWiFi, wifi != networkConnectedDuringConfiguration {
            error = SetupNetworkError.didNotReconnectToSameWiFi
        }
        
        Answers.logCustomEvent(withName: "WACFinished", customAttributes: ["result": error.displayName])
        
        setupNetworkState = .configured(speaker: speaker, configureError: error)
        if error != .wacCrash {
            self.discoveryService.refresh(clear: true)
            self.speakerDiscoveryState = .discovering(count: 0)
            startDiscoveryTimeout()
        }
    }
    
    func didFinishConfigurationForAccessory(_ speakerBeingConfigured: UnconfiguredSpeaker) {
        
        //self.didFailConfigurationForAccessory(speakerBeingConfigured)
        Answers.logCustomEvent(withName: "WACFinished", customAttributes: ["result": "Success"])
        var alreadyDiscoveredSpeaker: Speaker?
        if case .configuring(let speakerBeingConfigured) = setupNetworkState {
            
            for discoveredSpeaker in self.discoveryService.speakers {
                if  matchDiscoveredSpeaker(discoveredSpeaker, withSpeakerBeingConfigured: speakerBeingConfigured) {
                    alreadyDiscoveredSpeaker = discoveredSpeaker
                    break
                }
            }
        }
        
        setupNetworkState = .configured(speaker: speakerBeingConfigured, configureError: nil)
        if alreadyDiscoveredSpeaker != nil {
            
            handleDiscoveryOfConfiguredSpeaker(alreadyDiscoveredSpeaker!, afterConfiguring: speakerBeingConfigured)
        } else {
            
            self.speakerDiscoveryState = .discovering(count: 0)
            self.discoveryService.refresh(clear: true)
            self.startDiscoveryTimeout()
        }
    }
    
    
    func didAdd(unconfiguredSpeaker: UnconfiguredSpeaker) {
        
        let indexToInsert = alphabeticalIndexForUnconfiguredSpeaker(unconfiguredSpeaker, toInsertInto: self.unconfiguredSpeakers.value)
        self.unconfiguredSpeakers.value.insert(unconfiguredSpeaker, at: indexToInsert)
    }
    
    func didRemove(unconfiguredSpeaker: UnconfiguredSpeaker) {
        
        self.unconfiguredSpeakers.value.removeObject(unconfiguredSpeaker)
    }
}

extension SetupNetworkViewModel: DiscoveryServiceDelegate {
    

    func discoveryServiceDidFindSpeaker(_ discoveredSpeaker: Speaker, info: [ScalarNode : String]) {
        
        configuredSpeakers.value.append(discoveredSpeaker)
        if case .configured(let speakerBeingConfigured, _) = setupNetworkState {
            
            if  matchDiscoveredSpeaker(discoveredSpeaker, withSpeakerBeingConfigured: speakerBeingConfigured) {
                handleDiscoveryOfConfiguredSpeaker(discoveredSpeaker, afterConfiguring: speakerBeingConfigured)
            }
        }
    }
    
    func retryDiscovery() {
        
        if case .discoveryFailed(let count) = self.speakerDiscoveryState {
            self.speakerDiscoveryState = .discovering(count: count + 1)
            self.discoveryService.refresh(clear: true)
            self.startDiscoveryTimeout()
        }
    }

    func restartConfiguration() {
    
        self.speakerDiscoveryState = .idle
        self.searchForSpeakers()
    }
    
    func startDiscoveryTimeout() {
        
        noDiscoveryTimeoutDisposable?.dispose()
        noDiscoveryTimeoutDisposable = Observable<Int>.interval(5.0, scheduler: MainScheduler.instance).subscribe(weak: self, onNext: SetupNetworkViewModel.discoveryDidTimeout)
        noDiscoveryTimeoutDisposable?.disposed(by: disposeBag)
    }
    
    func stopDiscoveryTimeout() {
        
        noDiscoveryTimeoutDisposable?.dispose()
        noDiscoveryTimeoutDisposable = nil
    }
    
    func discoveryDidTimeout(_ one: Int) {
        
        stopDiscoveryTimeout()
        if case .discovering(let count) = speakerDiscoveryState {
            speakerDiscoveryState = .discoveryFailed(count: count)
            if case .configured(_, let error) = setupNetworkState {
                if error != nil {
                    Answers.logCustomEvent(withName: "SetupDiscoveryFinished", customAttributes: ["result": "DiscoveryFailed\(count)AfterWACError-\(error!.displayName)"])
                } else {
                    Answers.logCustomEvent(withName: "SetupDiscoveryFinished", customAttributes: ["result": "DiscoveryFailed\(count)AfterWACSuccess"])
                }
            }
        }
    }
    
    func matchDiscoveredSpeaker(_ discoveredSpeaker: Speaker, withSpeakerBeingConfigured speakerBeingConfigured: UnconfiguredSpeaker) -> Bool {
        
        //return false
        return (discoveredSpeaker.mac == speakerBeingConfigured.mac || discoveredSpeaker.mac == speakerBeingConfigured.veniceMac) && discoveredSpeaker.ipAddress != "172.24.0.1"
    }
    
    func handleDiscoveryOfConfiguredSpeaker(_ discoveredSpeaker: Speaker, afterConfiguring speakerBeingConfigured: UnconfiguredSpeaker) {
        
        var speaker = discoveredSpeaker
        let ssid = speakerBeingConfigured.ssid
        if let speakerImages = ExternalConfig.sharedInstance.speakerImages?.speakerImages {
            if let speakerImage = speakerImages.filter({ speakerImage in ssid.hasPrefix(speakerImage.ssid!)}).first {
                speaker.color = speakerImage.color
            }
        }
        if case .configured(_, let error) = setupNetworkState {
            
            if case SpeakerDiscoveryState.discovering(let count) = self.speakerDiscoveryState {
                if error != nil {
                    Answers.logCustomEvent(withName: "SetupDiscoveryFinished", customAttributes: ["result": "DiscoveredAfterWACError-Try\(count)-\(error!.displayName)"])
                } else {
                    Answers.logCustomEvent(withName: "SetupDiscoveryFinished", customAttributes: ["result": "DiscoveredAfterWACSuccess-Try\(count)"])
                }
            }
        }
        self.speakerDiscoveryState = .discovered(speaker: discoveredSpeaker)
        self.delegate?.setupViewModelDidFinishSpeakerConfiguration(self, speaker: speaker)
        self.wacService.active = false
        stopDiscoveryTimeout()
    }
    
    func discoveryServiceDidUpdateInfoForSpeaker(_ speaker: Speaker, info: [ScalarNode:String]) {
        
    }
    
    func discoveryServiceDidLoseSpeaker(_ speaker: Speaker) {
        
        configuredSpeakers.value.removeObject(speaker)
    }
    
    func delegateIdentifier() -> String {
        
        return "setup_view_model"
    }
}

func alphabeticalIndexForUnconfiguredSpeaker(_ speaker: UnconfiguredSpeaker, toInsertInto speakers: [UnconfiguredSpeaker]) -> Int {
    
    
    var index = 0
    
    for currentSpeaker in speakers {
        if currentSpeaker.completeName.lowercased() < speaker.completeName.lowercased() {
            index = index + 1
        } else {
            break
        }
    }
    
    return index
    
}
