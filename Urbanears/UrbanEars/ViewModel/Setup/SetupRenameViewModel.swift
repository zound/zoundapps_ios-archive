//
//  SetupRenameViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 01/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

enum ChangeFriendlyNameStatus {
    
    case idle
    case changing
    case changed
}

class SetupRenameViewModel {
    
    let speakerProvider: SpeakerProvider
    
    let friendlyName: Variable<String?>
    fileprivate var initialFriendlyName: String
    let currentRenameStatus: Variable<String?>
    let changeFriendlyNameStatus: Variable<ChangeFriendlyNameStatus>
    let disposeBag = DisposeBag()
    let speakerVariable: Variable<Speaker>
    
    var speaker: Speaker {
        get {
            return speakerVariable.value
        }
        set {
            speakerVariable.value = newValue
            initialFriendlyName = newValue.friendlyName
        }
    }
    
    init(speaker: Speaker, speakerProvider: SpeakerProvider) {
        
        self.speakerProvider = speakerProvider
        
        initialFriendlyName = ""
        friendlyName = Variable(initialFriendlyName)
        
        changeFriendlyNameStatus = Variable(.idle)
        currentRenameStatus = Variable(nil)
        speakerVariable = Variable(speaker)
        initialFriendlyName = speaker.friendlyName
        
        speakerVariable.asObservable().map{ (speaker) -> String in
            
            let speakerName : String = speaker.friendlyName.replacingOccurrences(of: " Gold Fish", with: " Goldfish")
            if speaker.friendlyName.range(of:" Gold Fish") != nil {
                print("chageing goldfish")
                let sanitezedFriendlyName = speakerName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                speakerProvider.setNode(ScalarNode.FriendlyName, value: sanitezedFriendlyName, forSpeaker: speaker)
                    .timeout(30.0, scheduler: MainScheduler.instance)
                    .catchErrorJustReturn(false)
                    .subscribe(onNext:{ [weak self] (response: Bool) in
                        
                        if response {
                            
                        } else {
                            self?.cancelSetFriendlyName()
                        }
                    }).disposed(by: self.disposeBag)
                
            }
            
            return speakerName
            }.bind(to:friendlyName).disposed(by: disposeBag)
        Observable.combineLatest(friendlyName.asObservable(), changeFriendlyNameStatus.asObservable()) { friendlyName, changeFriendlyNameStatus -> String? in
            
            switch changeFriendlyNameStatus {
                
            case .idle:
                return Localizations.Setup.Rename.SuccessfullyInstalledSpeaker(friendlyName ?? "")
            case .changing:
                return nil
            case .changed:
                return Localizations.Setup.Rename.SpeakerIsNowCalled(friendlyName ?? "")
            }
            }.bind(to:currentRenameStatus).disposed(by: disposeBag)
    }
    
    func saveFriendlyName() {
        
        changeFriendlyNameStatus.value = .changing
        
        if let sanitezedFriendlyName = friendlyName.value?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) {
        speakerProvider.setNode(ScalarNode.FriendlyName, value: sanitezedFriendlyName, forSpeaker: speaker)
            .timeout(30.0, scheduler: MainScheduler.instance)
            .catchErrorJustReturn(false)
            .subscribe(onNext:{ [weak self] (response: Bool) in
                
                if response {
                    self?.changeFriendlyNameStatus.value = .changed
                } else {
                    self?.cancelSetFriendlyName()
                }
            }).disposed(by: disposeBag)
        } else {
            
            self.changeFriendlyNameStatus.value = .changed
        }
        
    }
    
    func cancelSetFriendlyName() {
        
        changeFriendlyNameStatus.value = .idle
        friendlyName.value = initialFriendlyName
    }
}
