//
//  SettingsSpeakerListViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import MinuetSDK

class SettingsSpeakerListViewModel {
    
    let audioSystem: AudioSystem
    var speakers: Variable<[Speaker]> = Variable([])
    var disposeBag = DisposeBag()
    
    
    init(audioSystem: AudioSystem) {
    
        self.audioSystem = audioSystem
        self.audioSystem.speakers.asObservable().subscribe(onNext: { [weak self] speakers in
            self?.buildCells()
        }).disposed(by: disposeBag)
    }

    
    func buildCells() {
        
        speakers.value.removeAll()

        speakers.value = audioSystem.speakers.value.sorted(by: { speaker1, speaker2 in
            return speaker1.sortName.lowercased() < speaker2.sortName.lowercased()
        }).filter({ (speaker : Speaker) -> Bool in
            if let isConnectable = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.connectable {
                return isConnectable
            } else {
                return false
            }
        })
    }
}
