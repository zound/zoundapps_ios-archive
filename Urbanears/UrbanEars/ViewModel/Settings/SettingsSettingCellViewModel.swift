//
//  SettingsSettingCellViewModel.swift
//  UrbanEars
//
//  Created by Robert Sandru on 11/11/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

import RxSwift

enum SpeakerSetting {
    
    case about
    case equalizer
    case rename
    case timeZone
    case googlePrivacy
    
    var localizedDisplayName: String {
        
        switch self {
        case .about: return Localizations.Settings.Speaker.About
        case .equalizer: return Localizations.Settings.Speaker.Equalizer
        case .rename: return Localizations.Settings.Speaker.ChangeName
        case .timeZone: return Localizations.Settings.Speaker.TimeZone
        case .googlePrivacy: return Localizations.Settings.Speaker.GooglePrivacy
        }
    }
}

class SettingsSettingCellViewModel {
    
    var loading: Variable<Bool> = Variable(false)
    let setting: SpeakerSetting
    
    init(setting: SpeakerSetting) {
        self.setting = setting
    }
}
