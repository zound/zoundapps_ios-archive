//
//  SettingsSpeakerListViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import MinuetSDK

class SettingsTimeZoneViewModel {
    
    var disposeBag = DisposeBag()
    let timeZones:[MinuetSDK.TimeZone]
    var currentTimeZone: Variable<String>
    let speakerProvider:SpeakerProvider
    let speaker: Speaker
    
    let isSearching: Variable<Bool> = Variable(false)
    let searchText: Variable<String?> = Variable(nil)
    let filteredTimeZones: Variable<[MinuetSDK.TimeZone]> = Variable([])
    
    init(timeZoneState: TimeZoneState, speaker: Speaker, speakerProvider: SpeakerProvider) {
        
        self.speakerProvider = speakerProvider
        self.speaker = speaker
        self.currentTimeZone = Variable(timeZoneState.currentTimeZoneName)
        self.timeZones = timeZoneState.timeZones
        
        searchText.asObservable().map{ [weak self] searchText in
            guard let `self` = self else { return [] }
            if searchText != nil {
                return self.timeZones.filter({ $0.displayName.lowercased().contains(searchText!.lowercased())})
            } else {
                return self.timeZones
            }
        }.bind(to:filteredTimeZones).disposed(by: disposeBag)
    }
    
    func selectTimeZone(_ timeZone: MinuetSDK.TimeZone, done: @escaping (Void)->(Void)) {
        
        self.currentTimeZone.value = timeZone.name
        speakerProvider.setNode(.TimeZone, value: timeZone.name, forSpeaker: speaker).subscribe(onNext: { success in
            done()
        }, onError: { error in
            done()
        }).disposed(by: disposeBag)
    }
}
