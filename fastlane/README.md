fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

## Choose your installation method:

<table width="100%" >
<tr>
<th width="33%"><a href="http://brew.sh">Homebrew</a></td>
<th width="33%">Installer Script</td>
<th width="33%">Rubygems</td>
</tr>
<tr>
<td width="33%" align="center">macOS</td>
<td width="33%" align="center">macOS</td>
<td width="33%" align="center">macOS or Linux with Ruby 2.0.0 or above</td>
</tr>
<tr>
<td width="33%"><code>brew cask install fastlane</code></td>
<td width="33%"><a href="https://download.fastlane.tools">Download the zip file</a>. Then double click on the <code>install</code> script (or run it in a terminal window).</td>
<td width="33%"><code>sudo gem install fastlane -NV</code></td>
</tr>
</table>

# Available Actions
## iOS
### ios ue_frontier
```
fastlane ios ue_frontier
```
Deploy UrbanEars app to the Frontier Silicon TestFlight
### ios ue_zound
```
fastlane ios ue_zound
```
Deploy UrbanEars app to the Zound TestFlight
### ios marshall
```
fastlane ios marshall
```
Deploy Marshall app to the Zound TestFlight
### ios marshall_frontier
```
fastlane ios marshall_frontier
```
Deploy Marshall app to the Frontier Silicon TestFlight
### ios marshall_adhoc
```
fastlane ios marshall_adhoc
```
Build Marshall for AdHoc Deployment
### ios refresh_dsyms
```
fastlane ios refresh_dsyms
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
