//
//  SpotifyService.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 30/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya

public enum SpotifyService {
    
    case getPresets
    case getPlaylists
    case getPlaylistInfo(spotifyURI: String)
    case getPlaylistArtwork(spotifyURI: String)
    case getAlbumArtwork(spotifyURI: String)
    case getArtistArtwork(spotifyURI: String)
    case getTrackArtwork(spotifyURI: String)
    case getUserArtwork(spotifyURI: String)
    case getUserInfo
}

func playlistInfoURLFromSpotifyURI(_ spotifyURI: String) -> String {
 
    let spotifyURIComponents = spotifyURI.components(separatedBy: ":")
    let userId = spotifyURIComponents[2]
    let playlistId = spotifyURIComponents[4]
    return "/users/\(userId)/playlists/\(playlistId)"
}

func albumURLFromSpotifyURI(_ spotifyURI: String) -> String? {
    
    let spotifyURIComponents = spotifyURI.components(separatedBy: ":")
    if let albumId = spotifyURIComponents.last {
        return "/albums/\(albumId)"
    }
    return nil
}

func artistURLFromSpotifyURI(_ spotifyURI: String) -> String? {
    
    let spotifyURIComponents = spotifyURI.components(separatedBy: ":")
    if let artistId = spotifyURIComponents.last {
        return "/artists/\(artistId)"
    }
    return nil
}

func trackURLFromSpotifyURI(_ spotifyURI: String) -> String? {
    
    let spotifyURIComponents = spotifyURI.components(separatedBy: ":")
    if let trackId = spotifyURIComponents.last {
        return "/tracks/\(trackId)"
    }
    return nil
}

func userURLFromSpotifyURI(_ spotifyURI: String) -> String? {
    
    let spotifyURIComponents = spotifyURI.components(separatedBy: ":")
    if spotifyURI.contains("station") {
        if spotifyURIComponents.count > 3 {
            let userId = spotifyURIComponents[3]
            return "/users/\(userId)"
        }
    }
    return nil
}

extension SpotifyService:TargetType {
    
    public var headers: [String : String]? {
        return nil
    }
    
    
    public var task: Task {
        if let parameters = self.parameters {
            return Task.requestParameters(parameters: parameters, encoding: URLEncoding.default)
        } else {
            return Task.requestPlain
        }
    }

    public var baseURL: URL {
        
        switch self {
        case .getPresets: return URL(string: "https://exp.wg.spotify.com/v1")!
        default:return URL(string: "https://api.spotify.com/v1")!
        }
        
    }
    public var path: String {
        switch self {
        
        case .getPresets: return "/me/presets"
        case .getPlaylists: return "/me/playlists"
        case .getPlaylistInfo(let spotifyURI): return playlistInfoURLFromSpotifyURI(spotifyURI)
        case .getPlaylistArtwork(let spotifyURI): return playlistInfoURLFromSpotifyURI(spotifyURI)
        case .getAlbumArtwork(let spotifyURI): return albumURLFromSpotifyURI(spotifyURI) ?? ""
        case .getArtistArtwork(let spotifyURI): return artistURLFromSpotifyURI(spotifyURI) ??  ""
        case .getTrackArtwork(let spotifyURI): return trackURLFromSpotifyURI(spotifyURI) ??  ""
        case .getUserArtwork(let spotifyURI): return userURLFromSpotifyURI(spotifyURI) ??  ""
        case .getUserInfo: return "/me"
        }

    }
    public var method: Moya.Method {
        
        return .get
    }
    public var parameters: [String: Any]? {
     
        switch self {
        case .getPresets: return ["format":"blob" as AnyObject, "count":7 as AnyObject, "platform":"speaker" as AnyObject, "partner":"zound" as AnyObject]
        case .getPlaylists: return ["offset":0 as AnyObject, "limit":7 as AnyObject]
        case .getPlaylistArtwork: return ["fields":"images(url)" as AnyObject]
        case .getArtistArtwork: return nil
        case .getAlbumArtwork: return nil
        case .getTrackArtwork: return nil
        default: return nil
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
}
