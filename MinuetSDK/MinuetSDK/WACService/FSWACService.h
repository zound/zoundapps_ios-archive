//
//  FSWACService.h
//  UNDOK
//
//  Created by Raul Andrisan on 01/02/16.
//  Copyright © 2016 Frontier Silicon. All rights reserved.
//

#import <Foundation/Foundation.h>
@import ExternalAccessory;
typedef NS_ENUM(NSUInteger, FSWACServiceState) {
    FSWACServiceStateWiFiUnavailable = 0,
    FSWACServiceStateStopped,
    FSWACServiceStateSearching,
    FSWACServiceStateConfiguring
};

@protocol FSWACServiceDelegate;
@interface FSWACService : NSObject

- (instancetype)initWithSSIDWhitelist:(NSArray*)ssidWhitelist;
@property (readonly) NSArray* accessories;
@property (weak) id<FSWACServiceDelegate> delegate;
-(void)configureAccessory:(id)accessory withConfigurationUIOnViewController:(UIViewController*)viewContorller;
@property FSWACServiceState serviceState;
@property BOOL active;


@end

@protocol FSWACServiceDelegate <NSObject>

-(void)didFailConfigurationForAccessory:(EAWiFiUnconfiguredAccessory*)accessory;
-(void)didCancelConfigurationForAccessory:(EAWiFiUnconfiguredAccessory*)accessory;
-(void)didFinishConfigurationForAccessory:(EAWiFiUnconfiguredAccessory*)accessory;
-(void)didFindAccessory:(EAWiFiUnconfiguredAccessory*)accessory;
-(void)didLoseAccessory:(EAWiFiUnconfiguredAccessory*)accessory;
-(void)didUpdateState:(FSWACServiceState)state;

@end
