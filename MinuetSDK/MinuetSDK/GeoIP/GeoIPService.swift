//
//  GeoIPService.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 06/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya

enum GeoIPService {
    
    case getLocation
}

extension GeoIPService:TargetType {
    var headers: [String : String]? {
        return nil
    }
    
    var baseURL: URL {
        
        switch self {
        case .getLocation: return URL(string: "http://freegeoip.net/json/")!
        }
        
    }
    var path: String {
        switch self {
            
        case .getLocation: return ""
            
        }
        
    }
    var method: Moya.Method {
        
        return .get
    }
    
    public var task: Task {
        
            return Task.requestPlain
    }
    
    var sampleData: Data {
        return Data()
    }
}
