//
//  MultiroomDevice.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 03/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct MultiroomDevice {
    
    public let udn: String
    public let friendlyName: String
    public let ipAddress: String
    public let audioSyncVersion: String
    public let groupId: String?
    public let groupName: String?
    public let groupRole: GroupRole
    public let clientNumber: Int?
    public let key: Int
    public var mac: String {
        
        if udn.characters.count >= 12 {
            return self.udn.substring(from: self.udn.characters.index(self.udn.endIndex, offsetBy: -12))
        } else {
            return ""
        }
    }
}
