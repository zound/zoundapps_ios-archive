//
//  SpeakerUpdateStore.swift
//  MinuetSDK
//
//  Created by Claudiu Alin Luminosu on 08/01/2018.
//  Copyright © 2018 Zound Industries. All rights reserved.
//

import Foundation


class SpeakerUpdateStore {
    
    private  static let updateInfoKey = "updateInfo"
    private static let updateTimeStamp = "updateTimeStapmp"
    
    public static func storeUpdateInfoForSpeaker(_ speaker: Speaker, updateState: UpdateState) {
        
        let updateKey = "update_" + speaker.mac
        let updateStateString = String(updateState.rawValue)
        let currentDate  = String(Date().timeIntervalSinceReferenceDate)
        let updateDictionary = [
            updateTimeStamp : currentDate,
            updateInfoKey : updateStateString
        ]
        UserDefaults.standard.set(updateDictionary, forKey: updateKey)
        UserDefaults.standard.synchronize()
        print("stored update info for key \(updateKey) - update Info \(updateStateString) timestamp \(currentDate)")
    }
    
    /// returns the stored update state only if the check was done less that 24 hours ago
    public static func checkUpdateInfoForSpeaker(_ speaker: Speaker) -> UpdateState? {
        
        let updateKey = "update_" + speaker.mac
        
        let updateStateForSpeaker = UserDefaults.standard.object(forKey: updateKey)

        if let updateDictionary = updateStateForSpeaker as? [String : String] {
            let timestamp = Double(updateDictionary[updateTimeStamp]!)!
            let updateInfo =   UpdateState(rawValue: Int(updateDictionary[updateInfoKey]!)!)
            
            let currentTime = Date().timeIntervalSinceReferenceDate
            // if the time passed is less than one day (86.400 seconds) return stored value, else return nil
            if (currentTime - timestamp) < 86400 {
                print("returned update info from storage update key \(updateKey)")
                    return updateInfo
            } else {
                SpeakerUpdateStore.removeUpdateInfoForSpeaker(speaker)// remove old data because it's obsolete
                print("removed obsolete update data")

            }
        }
        
        return nil
    }
    
    public static func removeUpdateInfoForSpeaker(_ speaker : Speaker) {
        
        let updateKey = "update_" + speaker.mac
        UserDefaults.standard.removeObject(forKey: updateKey)
        UserDefaults.standard.synchronize()
    }
    
}
