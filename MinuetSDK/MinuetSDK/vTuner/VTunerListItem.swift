//
//  VTunerListItem.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import SWXMLHash

enum VTunerListItem: XMLIndexerDeserializable {
    
    case dir(info: VTunerDirInfo)
    case search(info: VTunerSearchInfo)
    case display(info: VTunerDisplayInfo)
    case message(info: VTunerMessageInfo)
    case previous(info: VTunerPreviousInfo)
    case showEpisode(info: VTunerShowEpisodeInfo)
    case showOnDemand(info: VTunerShowOnDemandInfo)
    case station(info: VTunerStationInfo)
    
    static func deserialize(_ node: XMLIndexer) throws -> VTunerListItem {
        
        let itemType: String = try node.byKey("ItemType").value()
        switch itemType {
        case "Dir":
            let dirInfo: VTunerDirInfo = try node.value()
            return .Dir(info: dirInfo)
        case "Display":
            let display: VTunerDisplayInfo = try node.value()
            return .Display(info: display)
        case "Search":
            let search: VTunerSearchInfo = try node.value()
            return .Search(info: search)
        case "Message":
            let message: VTunerMessageInfo = try node.value()
            return .Message(info: message)
        case "Previous":
            let previous: VTunerPreviousInfo = try node.value()
            return .Previous(info: previous)
        case "ShowEpisode":
            let showEpisode: VTunerShowEpisodeInfo = try node.value()
            return .ShowEpisode(info: showEpisode)
        case "ShowOnDemand":
            let showOnDemand: VTunerShowOnDemandInfo = try node.value()
            return .ShowOnDemand(info: showOnDemand)
        case "Station":
            let station: VTunerStationInfo = try node.value()
            return .Station(info: station)
        default: break
        }
        throw VTunerResponseError.vTunerErrorUnknown
    }
}

struct VTunerDirInfo: XMLIndexerDeserializable {
    
    let title: String
    let urlDir: String
    let urlDirBackUp: String
    
    static func deserialize(_ node: XMLIndexer) throws -> VTunerDirInfo {
        
        return VTunerDirInfo(title: try node.byKey("Title").value(),
                             urlDir: try node.byKey("UrlDir").value(),
                             urlDirBackUp: try node.byKey("UrlDirBackUp").value())
    }
    
    var folderName: String {
        
        if let urlComponents = URLComponents(string: urlDir) {
            
            if let goFile = urlComponents.queryItems?.filter({$0.name.lowercased() == "gofile"}).first {
                
                return goFile.value ?? ""
            }
        }
        return ""
    }
    
    
}

struct VTunerSearchInfo: XMLIndexerDeserializable {
    
    let searchURL: String
    let searchURLBackUp: String
    let searchCaption: String
    let searchTextBox: String
    let searchButtonGo: String
    let searchButtonCancel: String
    
    static func deserialize(_ node: XMLIndexer) throws -> VTunerSearchInfo {
        
        return VTunerSearchInfo(searchURL: try node.byKey("SearchURL").value(),
                                searchURLBackUp: try node.byKey("SearchURLBackUp").value(),
                                searchCaption: try node.byKey("SearchCaption").value(),
                                searchTextBox: try node.byKey("SearchTextbox").value(),
                                searchButtonGo: try node.byKey("SearchButtonGo").value(),
                                searchButtonCancel: try node.byKey("SearchButtonCancel").value())
    }
}

struct VTunerDisplayInfo: XMLIndexerDeserializable {
    
    let display: String
    
    static func deserialize(_ node: XMLIndexer) throws -> VTunerDisplayInfo {
        
        return VTunerDisplayInfo(display: try node.byKey("Display").value())
    }
}

struct VTunerMessageInfo: XMLIndexerDeserializable {
    
    let message: String
    
    static func deserialize(_ node: XMLIndexer) throws -> VTunerMessageInfo {
        
        return VTunerMessageInfo(message: try node.byKey("Message").value())
    }
}

struct VTunerPreviousInfo: XMLIndexerDeserializable {
    
    let urlPrevious: String
    let urlPreviousBackUp: String
    
    static func deserialize(_ node: XMLIndexer) throws -> VTunerPreviousInfo {
        
        return VTunerPreviousInfo(urlPrevious: try node.byKey("UrlPrevious").value(),
                                  urlPreviousBackUp: try node.byKey("UrlPreviousBackUp").value())
    }
}

struct VTunerShowEpisodeInfo: XMLIndexerDeserializable {
    
    let showEpisodeId: String
    let showEpisodeName: String
    let showEpisodeURL: String
    let bookmarkShow: String
    let showDesc: String
    let showFormat: String
    let lang: String
    let country: String
    let showMime: String
    
    static func deserialize(_ node: XMLIndexer) throws -> VTunerShowEpisodeInfo {
        
        return VTunerShowEpisodeInfo(showEpisodeId: try node.byKey("ShowEpisodeId").value(),
                                     showEpisodeName: try node.byKey("ShowEpisodeName").value(),
                                     showEpisodeURL: try node.byKey("ShowEpisodeURL").value(),
                                     bookmarkShow: try node.byKey("BookmarkShow").value(),
                                     showDesc: try node.byKey("ShowDesc").value(),
                                     showFormat: try node.byKey("ShowFormat").value(),
                                     lang: try node.byKey("Lang").value(),
                                     country: try node.byKey("Country").value(),
                                     showMime: try node.byKey("ShowMime").value())
    }
}

struct VTunerShowOnDemandInfo: XMLIndexerDeserializable {
    
    let showOnDemandId: String
    let showOnDemandName: String
    let logo: String
    let showOnDemandURL: String
    let showOnDemandURLBackup: String
    let bookmarkShow: String
    
    static func deserialize(_ node: XMLIndexer) throws -> VTunerShowOnDemandInfo {
        
        return VTunerShowOnDemandInfo(showOnDemandId: try node.byKey("ShowOnDemandId").value(),
                                      showOnDemandName: try node.byKey("ShowOnDemandName").value(),
                                      logo: try node.byKey("Logo").value(),
                                      showOnDemandURL: try node.byKey("ShowOnDemandURL").value(),
                                      showOnDemandURLBackup: try node.byKey("ShowOnDemandURLBackup").value(),
                                      bookmarkShow: try node.byKey("BookmarkShow").value())
    }
}

struct VTunerStationInfo: XMLIndexerDeserializable {
    
    let stationId: String
    let stationName: String
    let stationURL: String
    let stationDesc: String?
    let stationFormat: String?
    let stationLocation: String?
    let stationBandwidth: String?
    let stationMime: String?
    let relia: String?
    let bookmark: String?
    let logo: String?
    
    static func deserialize(_ node: XMLIndexer) throws -> VTunerStationInfo {
        
        return VTunerStationInfo(stationId: try node.byKey("StationId").value(),
                                 stationName: try node.byKey("StationName").value(),
                                 stationURL: try node.byKey("StationUrl").value(),
                                 stationDesc: try? node.byKey("StationDesc").value(),
                                 stationFormat: try? node.byKey("StationFormat").value(),
                                 stationLocation: try? node.byKey("StationLocation").value(),
                                 stationBandwidth: try? node.byKey("StationBandwidth").value(),
                                 stationMime: try? node.byKey("StationMime").value(),
                                 relia: try? node.byKey("Relia").value(),
                                 bookmark: try? node.byKey("Bookmark").value(),
                                 logo: try? node.byKey("Logo").value())
    }
}

